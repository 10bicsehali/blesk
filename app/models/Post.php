<?php

class Post extends Eloquent
{	
	protected $fillable = array('*');

	public function user()
	{
		return $this->belongsTo('User', 'user_id');
	}

	public function problem()
	{
		return $this->belongsTo('Problem', 'problem_id');
	}
}

?>