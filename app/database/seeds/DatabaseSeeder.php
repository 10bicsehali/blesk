<?php

class DatabaseSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();
		DB::table('problem_types')->delete();
		DB::table('services')->delete();
		DB::table('states')->delete();

		$this->call('GeneralSeeder');
	}

}

class GeneralSeeder extends Seeder {
	public function run()
	{
		DB::table('users')->insert(array(
				array('username' => 'usr1',
					  'nickname' => 'example user 1',
					  'email'	 => 'aaaaaa',
					  'password' => 'usr1',
					  'role_id'  => '0'),
				array('username' => 'usr2',
					  'nickname' => 'example user 2',
					  'email'	 => 'aaaaaa',
			   		  'password' => 'bbbbbb',
					  'role_id'  => '0'),
				array('username' => 'usr3',
					  'nickname' => 'example user 3',
					  'email'	 => 'aaaaaa',
					  'password' => 'bbbbbb',
					  'role_id'  => '0')
		));

		DB::table('problem_types')->insert(array(
				array('problem_type_name' => 'example type 1'),
				array('problem_type_name' => 'example type 2'),
				array('problem_type_name' => 'example type 3')
		));

		
		DB::table('services')->insert(
			array(
				array('service_name' => 'example service 1'),
				array('service_name' => 'example service 2'),
				array('service_name' => 'example service 3')
		));

		DB::table('states')->insert(
			array(
				array('state_name' => 'todo'),
				array('state_name' => 'in progress'),
				array('state_name' => 'done'),
				array('state_name' => 'duplicity'),

		));
	}
}
