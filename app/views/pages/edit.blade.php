@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'update/'.$problem->id, 'method' => 'POST')) }}
 <!-- title field -->
 <p>{{ Form::label('title', 'Subject') }}</p>
 {{ $errors->first('title', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('title', $problem->title) }}</p>

  <p>{{ Form::label('author', 'Author') }}</p>
 {{ $errors->first('author', '<p class="error">:message</p>') }}
 <p>{{ Form::select('author', $users ,$problem->author_id) }}</p>

  <p>{{ Form::label('problem_type', 'Type of problem') }}</p>
 {{ $errors->first('problem_type', '<p class="error">:message</p>') }}
 <p>{{ Form::select('problem_type', $problem_types ,$problem->problem_type_id) }}</p>

 <p>{{ Form::label('service', 'Service') }}</p>
 {{ $errors->first('service', '<p class="error">:message</p>') }}
 <p>{{ Form::select('service',  $services ,$problem->service_id) }}</p>



 <p>{{ Form::label('state', 'State') }}</p>
 {{ $errors->first('state', '<p class="error">:message</p>') }}
<p>{{ Form::select('state',  $states ,$problem->state_id) }}</p>

   <p>{{ Form::label('solver', 'Solver') }}</p>
 {{ $errors->first('solver', '<p class="error">:message</p>') }}
 <p>{{ Form::select('solver', $users ,$problem->solver_id) }}</p>

 <!-- body field -->
 <p>{{ Form::label('content', 'Problem description') }}</p>
 {{ $errors->first('content', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::textarea('content', $problem->content) }}</p>
 <!-- submit button -->
 <button type="submit" class="btn btn-large btn-primary"> Update </button>
 <a href="/" class = "btn btn-large btn-primary" > Back </a>
 {{ Form::close() }}

@stop
