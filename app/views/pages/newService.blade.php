@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'newService', 'method' => 'POST')) }}
 <!-- title field -->
 
 <p>{{ Form::label('service_name', 'Service name') }}</p>
 {{ $errors->first('service_name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('service_name', Input::old('service_name')) }}</p>

 <!-- submit button -->

<button type="submit" class="btn btn-primary"> Create </button>
<a href="/" class = "btn btn-default" > Back </a>

 {{ Form::close() }}
@stop
