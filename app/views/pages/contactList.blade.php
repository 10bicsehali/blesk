@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	$('#datatable').dataTable( {
		"sAjaxSource": "/index.php/contactList",
		"aoColumnDefs": [
          { 'bSortable': false,  }
       	],
       	"fnCreatedRow": function( nRow, aData, iDataIndex ) {
    	}
	} );

  var field1 = $("#datatable_length")
  $(".movedFieldLenght").append(field1);

  var field2 = $("#datatable_filter")
  $(".movedFieldFilter").append(field2);

  var field3 = $("#datatable_paginate")
  $(".movedFieldPaginate").append(field3);

  var field4 = $("#datatable_info")
  $(".movedFieldInfo").append(field4);

} );

</script>
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">



<div class="row">
    <div class="span3"><div class='movedFieldInfo'></div></div>
    <div class="span2 offset7"><div class='movedFieldPaginate'></div></div>
</div>
<div class="row">
    <div class='movedFieldLenght'></div>
</div > 

@stop