@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'answer/update/'.$post->id, 'method' => 'POST')) }}

 <p>{{ Form::label('body', 'Body') }}</p>
 {{ $errors->first('body', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::textarea('body', $post->body) }}</p>
 
 <button type="submit" class="btn btn-large btn-primary"> Update </button>
 {{ Form::close() }}

@stop