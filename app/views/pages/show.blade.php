@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	var oTable = $('#datatable').dataTable( {
		"sAjaxSource": "/index.php/log-data/{{$problem->id}}",
		"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [ 0 ] }],
		
	} );
	oTable.fnSort( [ [2,'asc'] ] );

	var field1 = $("#datatable_length")
	$(".movedFields").append(field1);

	var field3 = $("#datatable_paginate")
  	$(".movedFieldPaginate").append(field3);

  	var field4 = $("#datatable_info")
  	$(".movedFieldInfo").append(field4);

} );
</script>

<div class="span7"> <h2>{{$problem->title}}</h2> </div>

<div class="row">

	<div2 class="span7 well" > <h5 style="text-align:justify;">  Description: <br/>  {{$problem->content}} </h5></div2>
		<div class="span2" > 
	 		<div class="label label-info">  State: {{$problem->state->state_name}} </div>
			<div> <p>Author: {{$problem->author->username}}</p>  </div>
			<div> <p>Solver: {{$problem->solver->username}}</p>  </div>
			<div> <p>Created at: {{$problem->created_at}}</p>  </div>
		</div>
 	</div>
</div>


<div class="span7"> <h2>Comunication</h2> </div>



<div class="span9"> 
	<table class="table" id="datatable">
	 <thead><tr><th width="40%">Message</th><th width="10%">User</th><th width="12%">Date</th><th width="10%">Action</th></thead>
	</table>
	<div class="row">
      <div class="span3"><div class='movedFieldInfo'></div></div>
      <div class="span2 offset7"><div class='movedFieldPaginate'></div></div>
  </div>
	<div class='movedFields'></div>
  {{ Form::open(array('url' => '/show/'.$problem->id, 'method' => 'POST')) }}
</div>
{{-- --}}
<div class="span7"> <h2>Answer</h2> </div>

<div class="span9">
<p>{{ Form::label('user', 'User') }}</p>
 {{ $errors->first('user', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::select('user',  $users ,Input::old('user')) }}</p>


 <p>{{ Form::label('body', 'Content') }}</p>
 {{ $errors->first('body', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::textarea('body', Input::old('body')) }}</p>
 <!-- submit button -->

 <button type="submit" class="btn btn-primary" onclick="this.disabled=true;forms[0].submit();"> Post answer </button>
 <a href="/" class = "btn btn-default" > Back </a>
 {{ Form::close() }}


</div>
@stop
