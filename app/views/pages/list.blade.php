@extends('templates.layout')
@section('content')
<script type="text/javascript">
$(document).ready(function() {
	$('#datatable').dataTable( {
		"sAjaxSource": "/index.php/problems-data",
		"aoColumnDefs": [
    { 'bSortable': false, 'aTargets': [ 7 ] }
    ],
    "fnCreatedRow": function( nRow, aData, iDataIndex ) {
     cell = $('td:eq(6)', nRow).html();
     if ( cell == "done" )	{		
      nRow.setAttribute('class','success');
    }	else if(cell == "in progress")	{
     nRow.setAttribute('class','warning');
   }	else if (cell == "todo")	{
     nRow.setAttribute('class','error');
   }
   else {
    nRow.setAttribute('class','info');
  }
          //cell = $('td:eq(0)', nRow).html();
          //alert($('.acceptLink').html());//.attr("href", 'asdsad');
        }
      } );

  var field1 = $("#datatable_length")
  $(".movedFieldLenght").append(field1);

  var field2 = $("#datatable_filter")
  $(".movedFieldFilter").append(field2);

  var field3 = $("#datatable_paginate")
  $(".movedFieldPaginate").append(field3);

  var field4 = $("#datatable_info")
  $(".movedFieldInfo").append(field4);

  var field5 = $("#contactlist")
  $(".movedFieldContactlist").append(field5);

} );

</script>
<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
<div class="row-fluid">
  <div class="span10" style="float: left;">
    <table class="table table-bordered" id="datatable">
      <thead><tr><th>ID</th><th>Problem</th><th>Service</th><th>Type</th><th>Date</th><th>Solver</th><th>State</th><th>Action</th></tr></thead>
    </table>
    <div class="row">
      <div class="span3"><div class='movedFieldInfo'></div></div>
      <div class="span2 "><div class='movedFieldPaginate'></div></div>
      <!--<div class="span2 offset14"><div class='movedFieldConatactlist'></div></div> -->
    </div>
    <div class="row">
      <div class='movedFieldLenght'></div>
    </div > 
  </div>
  <div class="span2" style="float: left;">  
    
    <div class="heading">
      <h4 class="form-heading"> Contact List</h4>
    </div>

    @foreach($contactlist as $contact)
    <!-- <div class="span8 offset20"><div class='movedFieldConatactlist'></div> -->
    <div class="control-group">
      <br>
      <label class="control-label"> <b> Name: </b> {{$contact->name}} </label>
      <label class="control-label"> <b> Position: </b> {{$contact->position}} </label>
      <label class="control-label"> <b> Web: </b> {{$contact->web}} </label>
      <label class="control-label"> <b> Phone: </b> {{$contact->phone}} </label>
      <label class="control-label"> <b> Email: </b> {{$contact->mail}} </label>
    </div>
    <input type="button" class="btn" onClick="location.href='/index.php/editContact/{{$contact->id}}'" value="Edit">
    <input type="button" class="btn" onClick="location.href='/index.php/deleteContact/{{$contact->id}}'" value="Delete">

    
    
    @endforeach


  </div>


</div> 
@stop