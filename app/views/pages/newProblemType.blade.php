@extends('templates.layout')
@section('content')
 {{ Form::open(array('url' => 'newProblemType', 'method' => 'POST')) }}
 <!-- title field -->
 
 <p>{{ Form::label('problem_type_name', 'Problem Type name') }}</p>
 {{ $errors->first('problem_type_name', '<p class="alert alert-danger">:message</p>') }}
 <p>{{ Form::text('problem_type_name', Input::old('problem_type_name')) }}</p>

 <!-- submit button -->

<button type="submit" class="btn btn-primary"> Create </button>
<a href="/" class = "btn btn-default" > Back </a>

 {{ Form::close() }}
@stop
