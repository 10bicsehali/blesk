--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: exch; Type: SCHEMA; Schema: -; Owner: exchange
--

CREATE SCHEMA exch;


ALTER SCHEMA exch OWNER TO exchange;

--
-- Name: SCHEMA exch; Type: COMMENT; Schema: -; Owner: exchange
--

COMMENT ON SCHEMA exch IS 'databaze pro exchange system';


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: intranet
--

COMMENT ON SCHEMA public IS 'schema pro intranet';


--
-- Name: regsys; Type: SCHEMA; Schema: -; Owner: regsys
--

CREATE SCHEMA regsys;


ALTER SCHEMA regsys OWNER TO regsys;

--
-- Name: SCHEMA regsys; Type: COMMENT; Schema: -; Owner: regsys
--

COMMENT ON SCHEMA regsys IS 'Registration System';


--
-- Name: udb; Type: SCHEMA; Schema: -; Owner: udbmaster
--

CREATE SCHEMA udb;


ALTER SCHEMA udb OWNER TO udbmaster;

--
-- Name: SCHEMA udb; Type: COMMENT; Schema: -; Owner: udbmaster
--

COMMENT ON SCHEMA udb IS 'Standard udb schema';


--
-- Name: plperlu; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: 
--

CREATE PROCEDURAL LANGUAGE plperlu;


--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: 
--

CREATE PROCEDURAL LANGUAGE plpgsql;


SET search_path = exch, pg_catalog;

--
-- Name: F_update_company_after_training(); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION "F_update_company_after_training"() RETURNS "trigger"
    AS $$DECLARE
  changed boolean;
BEGIN

  IF tg_op = 'DELETE' THEN
     UPDATE exch.exch_company set available=true where id = old.exch_company_id;
  ELSE
     UPDATE exch.exch_company set available=(SELECT available FROM exch.exch_training_state WHERE id = new.exch_training_state_id) WHERE id = new.exch_company_id; 
  END IF;

  RETURN old;
END
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch."F_update_company_after_training"() OWNER TO exchange;

--
-- Name: er_int_fin_por(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_fin_por(id_student bigint, id_company integer, "year" integer) RETURNS integer
    AS $$DECLARE finpor integer;
DECLARE student RECORD;
BEGIN
finpor:=0;
FOR student IN SELECT exch_exam_student_id FROM er_int_temp 
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id
WHERE exch_company_id=id_company AND ec.year=exch_year
ORDER BY temprio,date_created LOOP
finpor:=finpor+1;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;

RETURN finpor;
END; 
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_fin_por(id_student bigint, id_company integer, "year" integer) OWNER TO exchange;

--
-- Name: FUNCTION er_int_fin_por(id_student bigint, id_company integer, "year" integer); Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON FUNCTION er_int_fin_por(id_student bigint, id_company integer, "year" integer) IS 'Vraci finalni poradi studenta pro view exch_request_prio
FIXME: Udelat pomoci window function az to pude';


--
-- Name: er_int_fin_por2(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_fin_por2(id_student bigint, id_company integer, exch_year integer) RETURNS integer
    AS $$
DECLARE finpor integer;
DECLARE student RECORD;
BEGIN
finpor:=0;
FOR student IN SELECT exch_exam_student_id FROM er_int_temp_speed
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id
WHERE exch_company_id=id_company AND ec.year=exch_year
ORDER BY temprio,date_created LOOP
finpor:=finpor+1;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;

RETURN finpor;
END; 
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_fin_por2(id_student bigint, id_company integer, exch_year integer) OWNER TO exchange;

--
-- Name: er_int_lcpor(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_lcpor(id_student bigint, id_lc integer, exch_year integer) RETURNS integer
    AS $$DECLARE lcpor integer;
DECLARE student RECORD;
DECLARE firsts bigint[];
BEGIN
lcpor:=0;
SELECT into firsts array(SELECT min(exch_exam_student_id) FROM exch_request AS er
	LEFT JOIN exch_company AS ec ON exch_company_id=ec.id
	WHERE ec.year=exch_year 
	AND (exch_request_state_id<=2 OR exch_request_state_id=3)
	GROUP BY exch_request_state_id,exch_company_id,date_created
	ORDER BY exch_request_state_id DESC,er.date_created);
SET LOCAL random_page_cost TO 0.5;
FOR student IN SELECT exch_exam_student_id,exch_company_id FROM exch_request AS er
LEFT JOIN exch_exam_student AS es ON exch_exam_student_id=es.id
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id AND ec.year=exch_year
WHERE es.exch_local_centre_id=id_lc 
AND (exch_request_state_id<=2 OR exch_request_state_id=3)
ORDER BY date_created LOOP
IF student.exch_exam_student_id = ANY(firsts) THEN  lcpor:=lcpor+1; END IF;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;
RETURN lcpor;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_lcpor(id_student bigint, id_lc integer, exch_year integer) OWNER TO exchange;

--
-- Name: FUNCTION er_int_lcpor(id_student bigint, id_lc integer, exch_year integer); Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON FUNCTION er_int_lcpor(id_student bigint, id_lc integer, exch_year integer) IS 'Vraci poradi studenta v ramci LC pro view exch_request_prio. 
FIXME: Udelat pomoci window function az to pude';


--
-- Name: er_int_lcpor2(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_lcpor2(id_student bigint, id_lc integer, exch_year integer) RETURNS integer
    AS $$
DECLARE student RECORD;
DECLARE lcpor integer;
DECLARE firsts bigint[];
BEGIN
lcpor:=0;
SELECT into firsts array(SELECT err.exch_exam_student_id FROM (SELECT min(er.id) AS esmin,exch_company_id FROM exch_request AS er
WHERE (exch_request_state_id<=3) AND extract(year from er.date_created)=exch_year
GROUP BY exch_company_id) AS aaa
LEFT JOIN exch_request err ON esmin=err.id);
SET LOCAL random_page_cost TO 0.5;
FOR student IN SELECT exch_exam_student_id,exch_company_id FROM exch_request AS er
LEFT JOIN exch_exam_student AS es ON exch_exam_student_id=es.id
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id AND ec.year=exch_year
WHERE es.exch_local_centre_id=id_lc
AND (exch_request_state_id<=2 OR exch_request_state_id=3)
ORDER BY date_created LOOP
IF student.exch_exam_student_id = ANY(firsts) THEN  lcpor:=lcpor+1; END IF;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;
RETURN lcpor;
END; 
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_lcpor2(id_student bigint, id_lc integer, exch_year integer) OWNER TO exchange;

--
-- Name: er_int_lcpor3(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_lcpor3(id_student bigint, id_lc integer, exch_year integer) RETURNS integer
    AS $$
DECLARE student RECORD;
DECLARE lcpor integer;
DECLARE firsts bigint[];
BEGIN
lcpor:=0;
SELECT into firsts array(SELECT err.exch_exam_student_id FROM (SELECT min(er.id) AS esmin,exch_company_id FROM exch_request AS er
WHERE (exch_request_state_id<=3) AND extract(year from er.date_created)=2013
GROUP BY exch_company_id) AS aaa
LEFT JOIN exch_request err ON esmin=err.id);
SET LOCAL random_page_cost TO 0.5;
FOR student IN SELECT exch_exam_student_id,exch_company_id FROM exch_request AS er
LEFT JOIN exch_exam_student AS es ON exch_exam_student_id=es.id
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id AND ec.year=exch_year
WHERE es.exch_local_centre_id=id_lc
AND (exch_request_state_id<=2 OR exch_request_state_id=3)
ORDER BY date_created LOOP
IF student.exch_exam_student_id = ANY(firsts) THEN  lcpor:=lcpor+1; END IF;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;
RETURN lcpor;
END; 
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_lcpor3(id_student bigint, id_lc integer, exch_year integer) OWNER TO exchange;

--
-- Name: er_int_lokfirst(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_lokfirst(id_student bigint, id_company integer, exch_year integer) RETURNS boolean
    AS $$DECLARE student RECORD;
BEGIN
	SELECT INTO student exch_exam_student_id FROM exch_request AS er
	LEFT JOIN exch_company AS ec ON exch_company_id=ec.id
	WHERE exch_company_id=id_company AND ec.year=exch_year 
	AND (exch_request_state_id<=2 OR exch_request_state_id=3)
	ORDER BY exch_request_state_id DESC,er.date_created LIMIT 1;
RETURN student.exch_exam_student_id=id_student;
END;
$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.er_int_lokfirst(id_student bigint, id_company integer, exch_year integer) OWNER TO exchange;

--
-- Name: er_int_lokpor(bigint, integer, integer); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION er_int_lokpor(id_student bigint, id_company integer, exch_year integer) RETURNS integer
    AS $$DECLARE lokpor integer;
DECLARE student RECORD;
BEGIN
lokpor:=0;
FOR student IN SELECT exch_exam_student_id FROM exch_request AS er
LEFT JOIN exch_company AS ec ON exch_company_id=ec.id
WHERE exch_company_id=id_company AND ec.year=exch_year 
AND (exch_request_state_id<=2 OR exch_request_state_id=3)
ORDER BY exch_request_state_id DESC,er.date_created
LOOP
lokpor:=lokpor+1;
EXIT WHEN student.exch_exam_student_id=id_student;
END LOOP;
RETURN lokpor;
END;
$$
    LANGUAGE plpgsql STABLE;


ALTER FUNCTION exch.er_int_lokpor(id_student bigint, id_company integer, exch_year integer) OWNER TO exchange;

--
-- Name: FUNCTION er_int_lokpor(id_student bigint, id_company integer, exch_year integer); Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON FUNCTION er_int_lokpor(id_student bigint, id_company integer, exch_year integer) IS 'Vraci lokalni poradi studenta v ramci praxe podle casu
FIXME: Udelat pomoci window function az to pude';


--
-- Name: f_training_acceptation_date(); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION f_training_acceptation_date() RETURNS "trigger"
    AS $$BEGIN

        IF (new.exch_training_state_id=12 OR new.exch_training_state_id=13) THEN
           IF (old.exch_training_state_id!=12 AND old.exch_training_state_id!=13) THEN
              new.acceptation_date=now();
           END IF;
        ELSE
            new.acceptation_date=null;
        END IF;

RETURN new;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.f_training_acceptation_date() OWNER TO exchange;

--
-- Name: f_update_company_request(); Type: FUNCTION; Schema: exch; Owner: exchange
--

CREATE FUNCTION f_update_company_request() RETURNS "trigger"
    AS $$BEGIN 

IF new.exch_request_state_id = '3' and old.exch_request_state_id<>new.exch_request_state_id THEN 

-- update all other requests to "used by another student", except own and previous unsuccessfull requests
   update exch_request set exch_request_state_id = '5', time_send_email = null, time_changed = now(), who_changed=new.who_changed
     where exch_company_id=new.exch_company_id and id<>new.id and date_created>old.date_created;
END IF;

IF new.exch_request_state_id <> '5' THEN 
-- always update training availability based on target request state. only for manually assigned states (<>5) to avoid cascading
   update exch_company set available=(select oform_available from exch_request_state where id=new.exch_request_state_id)
      where id=new.exch_company_id;
END IF;

return new;
END

$$
    LANGUAGE plpgsql;


ALTER FUNCTION exch.f_update_company_request() OWNER TO exchange;

SET search_path = public, pg_catalog;

--
-- Name: exch_ci_kontakt_trainee(); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION exch_ci_kontakt_trainee() RETURNS "trigger"
    AS $$BEGIN

        IF (new.exch_training_state_id=12 AND old.exch_training_state_id!=12) THEN
 insert into public.ci_kontakt(firma,id, ico,jmeno_prijmeni,tel,funkce, 
poznamka, email,datum, lc,mtime,muser,jmeno,aktivni, typ, ulice, mesto)

select ct.ci_firma, nextval('public.s_ci_kontakt'), t.id, s.family_name, s.phone, c.ref_no, substring ('praktikant v '||c.company_name || '
od '|| public.my_short_date(t.training_period_from)|| ' do ' || public.my_short_date(training_period_to) for 255), s.email, CURRENT_DATE, lc.username, now(), u.username, coalesce(s.first_name, '') || ' ' || coalesce(s.other_names, ''), true, 4, s.home_address_street, s.home_address_city
from exch.exch_training t
left join exch.exch_student s on t.exch_student_id=s.id 
left join exch.exch_company c on t.exch_company_id=c.id
left join public.users u on t.exch_user_id=u.id
left join public.lc on c.exch_local_centre_id=lc.id
join public.exch_ci_trainees ct on c.exch_local_centre_id=ct.lc and ct.rok=2008
where t.id=new.id and t.id not in (select ico from public.ci_kontakt where typ=4) and c.ref_no like 'CZ-%';

 update public.ci_kontakt set aktivni=true,mtime=now(),muser=(select username from public.users where id=new.exch_user_id) where typ=4 and ico=new.id;

END IF;

IF (new.exch_training_state_id!=12 AND old.exch_training_state_id=12) THEN
 update public.ci_kontakt set aktivni=false,mtime=now(),muser=(select username from public.users where id=new.exch_user_id) where typ=4 and ico=new.id;
END IF;

RETURN new;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.exch_ci_kontakt_trainee() OWNER TO intranet;

--
-- Name: FUNCTION exch_ci_kontakt_trainee(); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION exch_ci_kontakt_trainee() IS 'tmp. pridani kontaktu na praktikanty do company indexu pri jejich akceptaci';


--
-- Name: ldap_modify(text, text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION ldap_modify(server text, base text, username text, "password" text, filter text, attribute text, newvalues text) RETURNS character varying
    AS $_$use strict;
use Net::LDAP;

my ($server, $base, $username, $password, $filter, $attribute, $newvalues) = @_;

my @new = split /,/,$newvalues;

my $ldap = Net::LDAP->new($server, version=>3) || elog ERROR, "Could not connect to server: $@";

my $msg = $ldap->bind($username, password=>$password) || elog ERROR, "Could not bind: $@";

$msg->code && elog ERROR, "Could not bind: $@";

$msg = $ldap->search(
   base=>$base,
   filter=>$filter,
);

$msg->code && elog ERROR, "Could not search: $@";

$msg->count != 1 && elog ERROR, "Too much results for $filter! $@";

my $dn = $msg->entry(0)->dn();

$msg = $ldap->modify ($dn, replace => { $attribute => [ @new ] } );

$msg->code && elog ERROR, "Error ldap modify! $@";

return "$attribute:$newvalues";
$_$
    LANGUAGE plperlu;


ALTER FUNCTION public.ldap_modify(server text, base text, username text, "password" text, filter text, attribute text, newvalues text) OWNER TO alfi;

--
-- Name: FUNCTION ldap_modify(server text, base text, username text, "password" text, filter text, attribute text, newvalues text); Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON FUNCTION ldap_modify(server text, base text, username text, "password" text, filter text, attribute text, newvalues text) IS 'ulozi obsah newvalues jako pole do atributu newvalues pro uzivatele podle filter. 
server, username, password = login do ldapu';


--
-- Name: ldap_modify_share(text, text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) RETURNS character varying
    AS $_$use strict;
use Net::LDAP;

my ($server, $base, $username, $password, $filter, $share_ro, $share_rw) = @_;

my @new_ro = split /,/,$share_ro;
my @new_rw = split /,/,$share_rw;

my $ldap = Net::LDAP->new($server, version=>3) || elog ERROR, "Could not connect to server: $@";

my $msg = $ldap->bind($username, password=>$password) || elog ERROR, "Could not bind: $@";

$msg->code && elog ERROR, "Could not bind: $@";

$msg = $ldap->search(
   base=>$base,
   filter=>$filter,
);

$msg->code && elog ERROR, "Could not search: $@";

$msg->count == 0 && return "No result for $filter, missing user in ldap?!";
$msg->count != 1 && elog ERROR, "Too much results for $filter! $@";

my $dn = $msg->entry(0)->dn();

$msg = $ldap->modify ($dn, replace => { 'share-read' => [ @new_ro ],
                                        'share-write' => [ @new_rw ]
                      } );

$msg->code && elog ERROR, "Error ldap modify! ".$msg->error.", $@";

return "read: $share_ro; write: $share_rw";$_$
    LANGUAGE plperlu;


ALTER FUNCTION public.ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) OWNER TO intranet;

--
-- Name: FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) IS 'ulozi obsah share_rw a share_ro do atributu v ldapu (share-read, share-write)';


--
-- Name: ldap_search(text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION ldap_search(server text, base text, username text, "password" text, filter text, attributes text) RETURNS SETOF record
    AS $_$use strict;
use Net::LDAP;

my ($server, $base, $username, $password, $filter, $attributes) = @_;

my @attr = split /,/,$attributes;

my $ldap = Net::LDAP->new($server, version=>3) || elog ERROR, "Could not connect to server: $@";

my $msg = $ldap->bind($username, password=>$password) || elog ERROR, "Could not bind: $@";

$msg->code && elog ERROR, "Could not bind: $@";

$msg = $ldap->search(
   base=>$base,
   filter=>$filter,
   attrs=>@attr,
);

$msg->code && elog ERROR, "Could not search: $@";

my $ret;
foreach my $entry ($msg->entries)
{
   my $h = {};
   foreach my $a (@attr)
   {
      my @xx = {};
      @xx = $entry->get_value($a);
      $h->{$a} = join (', ', @xx);
   }
   push @$ret,$h
}

return $ret;
$_$
    LANGUAGE plperlu;


ALTER FUNCTION public.ldap_search(server text, base text, username text, "password" text, filter text, attributes text) OWNER TO alfi;

--
-- Name: FUNCTION ldap_search(server text, base text, username text, "password" text, filter text, attributes text); Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON FUNCTION ldap_search(server text, base text, username text, "password" text, filter text, attributes text) IS 'čtení ldapu přes perl';


--
-- Name: ldap_search_test(text, text, text, text, text, text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION ldap_search_test(server text, base text, username text, "password" text, filter text, attributes text) RETURNS SETOF record
    AS $_$use strict;
use Net::LDAP;

my ($server, $base, $username, $password, $filter, $attributes) = @_;

my @attr = split /,/,$attributes;

my $ldap = Net::LDAP->new($server, version=>3) || elog ERROR, "Could not connect to server: $@";

my $msg = $ldap->bind($username, password=>$password) || elog ERROR, "Could not bind: $@";

$msg->code && elog ERROR, "Could not bind: $@";

$msg = $ldap->search(
   base=>$base,
   filter=>$filter,
   attrs=>@attr,
);

$msg->code && elog ERROR, "Could not search: $@";

my $ret;
foreach my $entry ($msg->entries)
{
   my $h = {};
   foreach my $a (@attr)
   {
      my @xx = {};
      @xx = $entry->get_value($a);
      $h->{$a} = join (', ', @xx);
   }
   push @$ret,$h
}

return $ret;
$_$
    LANGUAGE plperlu;


ALTER FUNCTION public.ldap_search_test(server text, base text, username text, "password" text, filter text, attributes text) OWNER TO alfi;

--
-- Name: FUNCTION ldap_search_test(server text, base text, username text, "password" text, filter text, attributes text); Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON FUNCTION ldap_search_test(server text, base text, username text, "password" text, filter text, attributes text) IS 'testování ldapu';


--
-- Name: like_regexp_part2(text, text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION like_regexp_part2(text, text) RETURNS boolean
    AS $_$select ($1 ~* trim(substring ($2, '(?:.*:|^)([^:()]*)'))
-- workaround pro bug v postgresu - neumi ilike pro utf8 s diakritikou
or $1 ~* trim(substring (translate($2, 'ěščřžýáíéú', 'ĚŠČŘŽÝÁÍÉÚ'), '(?:.*:|^)([^:()]*)'))
) and length(trim(substring ($2, '(?:.*:|^)([^:()]*)')))>0
as result;$_$
    LANGUAGE sql;


ALTER FUNCTION public.like_regexp_part2(text, text) OWNER TO alfi;

--
-- Name: like_regexp_part3(text, text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION like_regexp_part3(text, text) RETURNS boolean
    AS $_$select CASE WHEN length(substring ($2, '[^(]* \\((.*)')) >0
THEN
$1 ~* trim(substring ($2, '[^(]* \\((.*)'))
-- workaround pro bug v postgresu - neumi ilike pro utf8 s diakritikou
or $1 ~* trim(substring (translate($2, 'ěščřžýáíéú', 'ĚŠČŘŽÝÁÍÉÚ'), '[^(]* \\((.*)'))

WHEN length(trim(substring ('$q', '(?:.*:|^)([^:()]*)'))) >0 
THEN
$1 ~* trim(substring ($2, '(?:.*:|^)([^:()]*)'))
-- workaround pro bug v postgresu - neumi ilike pro utf8 s diakritikou
or $1 ~* trim(substring (translate($2, 'ěščřžýáíéú', 'ĚŠČŘŽÝÁÍÉÚ'), '(?:.*:|^)([^:()]*)'))
ELSE
false
END 

as result;$_$
    LANGUAGE sql;


ALTER FUNCTION public.like_regexp_part3(text, text) OWNER TO alfi;

--
-- Name: my_date(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION my_date(timestamp with time zone) RETURNS text
    AS $_$select to_char($1, 'dd.mm.yyyy hh24:mi') as result;$_$
    LANGUAGE sql;


ALTER FUNCTION public.my_date(timestamp with time zone) OWNER TO intranet;

--
-- Name: my_short_date(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION my_short_date(timestamp with time zone) RETURNS text
    AS $_$select to_char($1, 'FMdd.FMmm.yyyy')  as result;$_$
    LANGUAGE sql;


ALTER FUNCTION public.my_short_date(timestamp with time zone) OWNER TO intranet;

--
-- Name: rights_get(character, integer); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION rights_get(rights character, "level" integer) RETURNS boolean
    AS $$begin
IF (substring(rights from level for 1) = (select id from _groups where level=_groups.level)) THEN
return true;
  ELSE 
return false;
END IF;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.rights_get(rights character, "level" integer) OWNER TO intranet;

--
-- Name: rights_set(character, integer, boolean); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION rights_set(rights character, "level" integer, "set" boolean) RETURNS character
    AS $_$declare
fullrights character(51);
begin
-- retezcove operace orezavaji mezeru na konci?!
fullrights = rpad(rights, 50, ' ')||'_';
IF (set = true) THEN 
  fullrights=overlay(fullrights  placing (select g.id from public._groups g where g.level=$2) from (level) for 1);
ELSE 
  fullrights=overlay(fullrights placing ' ' from (level) for 1);
END IF;
return substring(fullrights for 50);
END;
$_$
    LANGUAGE plpgsql;


ALTER FUNCTION public.rights_set(rights character, "level" integer, "set" boolean) OWNER TO intranet;

--
-- Name: FUNCTION rights_set(rights character, "level" integer, "set" boolean); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) IS 'vlozeni nebo smazani konkretniho prava pro intranet';


--
-- Name: share2ldap(text); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION share2ldap(username text) RETURNS character varying
    AS $_$declare
  result_all text;
  result text;
  dir_ro text;
  dir_rw text;
  username_ldap character varying;
  share_row RECORD;
  user_row RECORD;

begin
  result_all='';

  -- seznam vsech aktivnich uzivatelu nebo
  -- pokud je zadano jmeno, nastavi se pouze vybrany uzivatel
  FOR user_row in SELECT udb_username from public.users where password_hash is not null and (length($1)=0 or udb_username=$1) group by 1 order by 1  LOOP
    dir_ro='';
    dir_rw='';
    username_ldap='(uid='||user_row.udb_username||')';
    result_all=result_all||'\n'||user_row.udb_username;

    -- ulozi sloupec s pravama do retezce
    FOR share_row IN SELECT sr.access, sr.directory FROM public.share_rights_users2ldap sr where sr.username=user_row.udb_username group by sr.directory,sr.access order by 1 LOOP
      IF (share_row.access='RO') THEN 
         IF (length(dir_ro)>0) THEN
            dir_ro=dir_ro||',';
         END IF;
         dir_ro=dir_ro||coalesce(share_row.directory,'');
      END IF;
      IF (share_row.access='RW') THEN
         IF (length(dir_rw)>0) THEN
            dir_rw=dir_rw||',';
         END IF;
         dir_rw=dir_rw||coalesce(share_row.directory,'');
      END IF;
    END LOOP;

    -- a ulozi obsah do ldapu (pl/perl untrusted)
    result=public.ldap_modify_share ('localhost', 'ou=members,dc=iaeste,dc=cz', 'cn=shareadmin,ou=system,dc=iaeste,dc=cz', 'unstonsin31', username_ldap, dir_ro, dir_rw);
    result_all=result_all||' - '||coalesce(result,'');

  END LOOP;
-- vrati vsechny vystupy volani ldapu
return result_all;
end;$_$
    LANGUAGE plpgsql;


ALTER FUNCTION public.share2ldap(username text) OWNER TO intranet;

--
-- Name: FUNCTION share2ldap(username text); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION share2ldap(username text) IS 'precte nastaveni prav pro pristup k sharu a ulozi jejich jmena do ldapu
pokud neni zadano username, provede se pro vsechny aktivni uzivatele';


--
-- Name: share2ldap_tmp(); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION share2ldap_tmp() RETURNS character varying
    AS $$declare
result_all text;
result text;
dir_ro text;
dir_rw text;
username_ldap character varying;
share_row RECORD;
user_row RECORD;

begin
  result_all='';
  -- precteni vsech ovlivnenych uzivatelu.. melo by cist z tabulky users kvuli smazanym pravum??!!
--  FOR user_row in SELECT udb_username from users where password_hash is not null group by 1 order by 1 LOOP
  FOR user_row in SELECT udb_username from users where password_hash is not null and (length('')=0 or udb_username='') group by 1 order by 1 LOOP
    dir_ro='';
    dir_rw='';
    username_ldap='(uid=test)';
    result_all=result_all||'\n'||user_row.udb_username;

    -- ulozi sloupec s pravama do retezce
    FOR share_row IN SELECT access, directory FROM share_rights_users2ldap where username=user_row.udb_username group by directory,access order by 1 LOOP
      IF (share_row.access='RO') THEN 
         IF (length(dir_ro)>0) THEN
            dir_ro=dir_ro||',';
         END IF;
         dir_ro=dir_ro||coalesce(share_row.directory,'');
      END IF;
      IF (share_row.access='RW') THEN
         IF (length(dir_rw)>0) THEN
            dir_rw=dir_rw||',';
         END IF;
         dir_rw=dir_rw||coalesce(share_row.directory,'');
      END IF;
    END LOOP;

    -- a ulozi obsah do ldapu (pl/perl untrusted)
    result=ldap_modify_share ('localhost', 'ou=members,dc=devel,dc=iaeste,dc=cz', 'cn=devel,ou=system,dc=devel,dc=iaeste,dc=cz', 'heslo', username_ldap, dir_ro, dir_rw);
    result_all=result_all||' - '||coalesce(result,'');

  END LOOP;
-- vrati vsechny vystupy volani ldapu
return result_all;
end;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.share2ldap_tmp() OWNER TO alfi;

--
-- Name: FUNCTION share2ldap_tmp(); Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON FUNCTION share2ldap_tmp() IS 'tmp. precte nastaveni prav pro pristup k sharu a ulozi jejich jmena do ldapu';


--
-- Name: share2ldap_update_user(); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION share2ldap_update_user() RETURNS "trigger"
    AS $$declare
result text; 
begin

-- kdyby se menilo username, v ldapu jeste nove nebude
result=public.share2ldap(old.udb_username);
IF (length(result)>0) THEN
return new;
ELSE
return old;
END IF;

end;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.share2ldap_update_user() OWNER TO intranet;

--
-- Name: FUNCTION share2ldap_update_user(); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION share2ldap_update_user() IS 'update prav pro share v ldapu pro zmeneneho uzivatele';


--
-- Name: share_read(text); Type: FUNCTION; Schema: public; Owner: alfi
--

CREATE FUNCTION share_read(username text) RETURNS character varying
    AS $$declare
  res text;

begin

res = (select 'read: ' || "share-read" || '
write: ' || "share-write"  from ldap_search_test ('localhost', 'ou=members,dc=iaeste,dc=cz', 'cn=shareadmin,ou=system,dc=iaeste,dc=cz', 'unstonsin31', '(uid='||username||')', 'share-read,share-write') 
as foo("share-read" text, "share-write" text));

-- vrati vsechny vystupy volani ldapu
return res;
end;

$$
    LANGUAGE plpgsql STRICT;


ALTER FUNCTION public.share_read(username text) OWNER TO alfi;

--
-- Name: FUNCTION share_read(username text); Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON FUNCTION share_read(username text) IS 'čtení stavu práv pro share v ldapu';


--
-- Name: to_bit(integer); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION to_bit(integer) RETURNS text
    AS $_$select ($1&1024)/1024 || ($1&512)/512 || ($1&256)/256 || ($1&128)/128 || ($1&64)/64 ||
      ($1&32)/32 || ($1&16)/16 || ($1&8)/8 || ($1&4)/4 || ($1&2)/2 || ($1&1) as result;$_$
    LANGUAGE sql;


ALTER FUNCTION public.to_bit(integer) OWNER TO intranet;

--
-- Name: udb2intranet_insert(); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION udb2intranet_insert() RETURNS "trigger"
    AS $$BEGIN

IF (new.id>0) THEN
insert into public.users (username, 	rights, 	id, 	groupname, 	lastlogin, 	fullname, 	email, 	password_hash, 	m_time, 	m_uid, 	ci_kontakt, 	udb_username, 	udb_id) values

(new.username, (select rights from public.users where username='default'), nextval('public.s_users'), (select username from public.lc where id=new.lcs_id), null, new.first_name||' '||new.last_name, 
CASE WHEN (select mail from services where users_id=new.id) THEN new.iaeste_mail ELSE new.personal_mail END, 
'enabled', now(), 0, new.ci_contact, new.username, new.id);

END IF;

RETURN new;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.udb2intranet_insert() OWNER TO intranet;

--
-- Name: FUNCTION udb2intranet_insert(); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION udb2intranet_insert() IS 'vlozeni uzivatele z udb do intranetu';


--
-- Name: udb2intranet_services(); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION udb2intranet_services() RETURNS "trigger"
    AS $$BEGIN

IF (new.users_id>0) THEN
update public.users set 
-- ulozi personal mail pro neaktivni mail uzivatele
email = CASE WHEN new.mail THEN (select iaeste_mail from udb.users where udb.users.id=new.users_id) ELSE (select personal_mail from udb.users where udb.users.id=new.users_id)  END,
-- nastavi pravo pro share
rights=public.rights_set(rights, 26, new.share_read)
where udb_id=new.users_id;

END IF;

RETURN new;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.udb2intranet_services() OWNER TO intranet;

--
-- Name: FUNCTION udb2intranet_services(); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION udb2intranet_services() IS 'aktualizace email adresy v intranetu podle stavu email uctu v udb';


--
-- Name: udb2intranet_update(); Type: FUNCTION; Schema: public; Owner: intranet
--

CREATE FUNCTION udb2intranet_update() RETURNS "trigger"
    AS $$BEGIN

IF (new.id>0) THEN
update public.users set 
-- default fulname slozene ze jmena a prijmeni
fullname=CASE WHEN fullname=old.first_name||' '||old.last_name THEN new.first_name||' '||new.last_name ELSE fullname END, 
-- email private nebo iaeste podle aktivity
email=CASE WHEN (select mail from services where users_id=new.id) THEN new.iaeste_mail ELSE new.personal_mail END, 
-- povoleny ucet (udb_rights.id<>10)
password_hash=CASE when new.rights_id=10 THEN null else coalesce(password_hash, 'enabled') END, 
-- kopie prav pro HR (3), admin (1,2)
rights=public.rights_set(public.rights_set(rights, 21, (new.rights_id=1 or new.rights_id=2)), 20, (new.rights_id=3)),
-- parovani s udb
udb_username=new.username, m_time=now(), m_uid=0, ci_kontakt=new.ci_contact 
where udb_id=new.id;

IF (old.lcs_id<>new.lcs_id) THEN
-- zmena LC
  update public.users set groupname=(select username from public.lc where id=new.lcs_id) where udb_id=new.id;
END IF;

END IF;


RETURN new;
END;$$
    LANGUAGE plpgsql;


ALTER FUNCTION public.udb2intranet_update() OWNER TO intranet;

--
-- Name: FUNCTION udb2intranet_update(); Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON FUNCTION udb2intranet_update() IS 'aktualizace uzivatele z udb do intranetu';


--
-- Name: array_agg(anyelement); Type: AGGREGATE; Schema: public; Owner: alfi
--

CREATE AGGREGATE array_agg (
    BASETYPE = anyelement,
    SFUNC = array_append,
    STYPE = anyarray,
    INITCOND = '{}'
);


ALTER AGGREGATE public.array_agg(anyelement) OWNER TO alfi;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ci_firma; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_firma (
    id integer NOT NULL,
    ico integer,
    nazev character varying,
    lc_reg character varying(50),
    ulice character varying(50),
    mesto character varying(50),
    psc character varying(10),
    datum_registrace timestamp with time zone,
    typ_spoluprace character varying,
    poznamky text,
    zamereni character varying,
    dic character varying(15),
    tel character varying(50),
    fax character varying(50),
    telefax character varying(50),
    web character varying(255),
    email character varying(50),
    lc_katalog character varying(50),
    lc_vymena character varying(50),
    dat_registrace_vymena timestamp with time zone,
    dat_registrace_katalog timestamp with time zone,
    mtime timestamp with time zone,
    muser character varying(20),
    alias character varying,
    aktivni boolean DEFAULT true
);


ALTER TABLE public.ci_firma OWNER TO intranet;

--
-- Name: COLUMN ci_firma.aktivni; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_firma.aktivni IS 'pozor, drive pouzivano jako id<-20!!';


SET search_path = exch, pg_catalog;

--
-- Name: ci_company; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW ci_company AS
    SELECT ci_firma.id, ci_firma.ico, ci_firma.nazev, ci_firma.lc_reg, ci_firma.ulice, ci_firma.mesto, ci_firma.psc, ci_firma.datum_registrace, ci_firma.typ_spoluprace, ci_firma.poznamky, ci_firma.zamereni, ci_firma.dic, ci_firma.tel, ci_firma.fax, ci_firma.telefax, ci_firma.web, ci_firma.email, ci_firma.mtime, ci_firma.muser, ci_firma.alias FROM public.ci_firma WHERE (ci_firma.aktivni = true);


ALTER TABLE exch.ci_company OWNER TO exchange;

--
-- Name: VIEW ci_company; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON VIEW ci_company IS 'seznam firem, pouze aktivni';


SET search_path = public, pg_catalog;

--
-- Name: ci_kontakt; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_kontakt (
    firma integer NOT NULL,
    id integer NOT NULL,
    ico integer,
    osloveni character varying(30),
    jmeno_prijmeni character varying(50),
    tel character varying(100),
    fax character varying(100),
    funkce character varying(50),
    popis_osoby character varying(100),
    poznamka character varying,
    email character varying(100),
    mobil character varying(100),
    telefax character varying(100),
    datum date,
    lc character varying(10),
    mtime timestamp with time zone,
    muser character varying(20),
    jmeno character varying(50),
    aktivni boolean DEFAULT true,
    typ smallint DEFAULT 1,
    ulice character varying(255),
    mesto character varying(255),
    psc character varying(10)
);


ALTER TABLE public.ci_kontakt OWNER TO intranet;

--
-- Name: COLUMN ci_kontakt.jmeno_prijmeni; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_kontakt.jmeno_prijmeni IS 'prejmenovat na ''prijmeni''';


SET search_path = exch, pg_catalog;

--
-- Name: ci_person; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW ci_person AS
    SELECT ci_kontakt.firma AS ci_company, ci_kontakt.id, ci_kontakt.osloveni, ci_kontakt.jmeno_prijmeni AS prijmeni, ci_kontakt.jmeno, ci_kontakt.tel, ci_kontakt.fax, ci_kontakt.funkce, ci_kontakt.popis_osoby, ci_kontakt.poznamka, ci_kontakt.email, ci_kontakt.mobil, ci_kontakt.lc, ci_kontakt.mtime AS modified, ci_kontakt.muser AS modified_by FROM public.ci_kontakt WHERE (ci_kontakt.aktivni = true);


ALTER TABLE exch.ci_person OWNER TO exchange;

--
-- Name: VIEW ci_person; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON VIEW ci_person IS 'kontaktni osoby';


--
-- Name: exch_company; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company (
    ref_no character varying(50) NOT NULL,
    company_name character varying(255),
    business character varying(255),
    responsible character varying(255),
    phone character varying(50),
    fax character varying(50),
    email character varying(100),
    working_place character varying(50),
    airport character varying(50) DEFAULT 'Prague'::character varying,
    transport character varying(50),
    employees character varying(25),
    specialization character varying(255),
    study_level character varying(50),
    previous_training boolean,
    other_requirements text,
    kind_of_work text,
    number_weeks_min integer DEFAULT 6,
    number_weeks_max integer DEFAULT 12,
    work_closed character varying(50) DEFAULT '-'::character varying,
    gross_pay character varying(50),
    max_deduction character varying(50),
    lodging_arranged character varying(50),
    canteen_facilities boolean,
    estimated_lodging character varying(50) DEFAULT '3000'::character varying,
    estimated_living character varying(50) DEFAULT '6000'::character varying,
    deadline_for_nomination date,
    pay_by_iaeste character varying(50),
    pay_by_employer character varying(50),
    "year" integer,
    exch_user character varying,
    english_and_or character varying,
    german_and_or character varying,
    french_and_or character varying,
    exch_work_category_abbr character varying,
    english smallint,
    german smallint,
    french smallint,
    other_language character varying(50),
    other smallint,
    pay_per_gross smallint,
    pay_per_employer smallint,
    pay_per_iaeste smallint,
    pay_per_lodging smallint,
    pay_per_living smallint,
    business_or_product character varying(255),
    lock_public_edit smallint,
    hash_public character(40),
    ci_company_id integer,
    time_changed timestamp without time zone,
    exch_local_centre_id integer,
    exch_user_id integer,
    sent_to_email character varying(70),
    sent_to_company timestamp without time zone,
    id integer DEFAULT nextval(('exch.exch_company_id_seq'::text)::regclass) NOT NULL,
    faculty_text character varying(255),
    who_changed integer,
    training_available boolean,
    within_from date,
    within_to date,
    within_from2 date,
    within_to2 date,
    working_hours_per_week numeric(4,1) DEFAULT 40,
    daily_working_hours numeric(4,1) DEFAULT 8,
    street character varying(255),
    city character varying(255),
    currency character varying(10),
    available boolean DEFAULT true,
    postpraxe boolean DEFAULT false,
    reserved boolean DEFAULT false
);


ALTER TABLE exch.exch_company OWNER TO exchange;

--
-- Name: exch_exam_student; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam_student (
    id bigint DEFAULT nextval(('exch.exch_exam_student_id_seq'::text)::regclass) NOT NULL,
    exch_exam_id integer,
    exch_faculty_id integer,
    exch_user_id integer,
    first_name character varying(100),
    family_name character varying(100),
    email character varying(100),
    phone character varying(50),
    exch_study_program_id integer,
    exch_year_class_id integer,
    specialization character varying(255),
    specialization_info text,
    language_other_1 character varying(50),
    language_other_2 character varying(50),
    exch_language_level_id_4 integer,
    exch_language_level_id_5 integer,
    exch_special_activity_id integer,
    manual_work_id integer,
    exch_prefered_residency_length_id integer,
    motivation text,
    exch_title_id integer,
    points_writing numeric(5,2) DEFAULT 0.0,
    points_listening numeric(5,2) DEFAULT 0.0,
    points_speaking numeric(5,2) DEFAULT 0.0,
    points_motivation numeric(5,2) DEFAULT 0.0,
    points_extra numeric(5,2) DEFAULT 0.0,
    points_iaeste numeric(5,2) DEFAULT 0.0,
    points_sum numeric(5,2),
    points_index numeric(5,2),
    average_year_1 numeric(5,2),
    average_year_2 numeric(5,2),
    average_year_3 numeric(5,2),
    average_year_4 numeric(5,2),
    average_year_5 numeric(5,2),
    average_year_6 numeric(5,2),
    average_pgs numeric(5,2),
    time_changed timestamp without time zone,
    exam_language integer,
    who_changed_points integer,
    time_changed_points timestamp without time zone,
    time_create timestamp without time zone,
    time_send_email timestamp without time zone,
    hash character(40),
    time_changed_preference timestamp without time zone,
    who_changed_preference integer,
    citizenship_country_id integer,
    registration_validity date,
    want_lateoffers boolean DEFAULT false,
    exch_local_centre_id integer,
    sex_id smallint
);


ALTER TABLE exch.exch_exam_student OWNER TO exchange;

--
-- Name: COLUMN exch_exam_student.exch_exam_id; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_student.exch_exam_id IS 'od 2013 nepoužíváno..';


--
-- Name: COLUMN exch_exam_student.exch_local_centre_id; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_student.exch_local_centre_id IS 'LC, kterému student patří. automatickou hodnotu je možné ručně změnit';


--
-- Name: exch_request; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_request (
    exch_company_id integer,
    exch_exam_student_id integer,
    priority integer,
    date_created timestamp without time zone DEFAULT now() NOT NULL,
    exch_request_state_id smallint NOT NULL,
    id serial NOT NULL,
    time_changed timestamp without time zone,
    who_changed integer,
    time_send_email timestamp without time zone,
    "comment" text
);


ALTER TABLE exch.exch_request OWNER TO exchange;

--
-- Name: COLUMN exch_request.date_created; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request.date_created IS 'datum požadavku o praxi - kvůli pořadí v čase';


--
-- Name: COLUMN exch_request.exch_request_state_id; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request.exch_request_state_id IS 'stav požadavku (pohovor, zamítnuto...)';


--
-- Name: COLUMN exch_request.time_changed; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request.time_changed IS 'čas poslední změny pro mailové notifikace';


--
-- Name: COLUMN exch_request.who_changed; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request.who_changed IS 'user id poslední změny';


--
-- Name: COLUMN exch_request.time_send_email; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request.time_send_email IS 'čas poslední emailové notifikace';


--
-- Name: COLUMN exch_request."comment"; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_request."comment" IS 'komentář LC k pohovoru';


--
-- Name: exch_request_limits; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_request_limits (
    exch_local_centre_id integer NOT NULL,
    request_limit integer,
    liftdate date,
    who_changed integer,
    time_changed timestamp with time zone
);


ALTER TABLE exch.exch_request_limits OWNER TO exchange;

--
-- Name: er_int_temp; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW er_int_temp AS
    SELECT aaa.id, aaa.exch_request_state_id, aaa.poradi_lc, aaa.lok_por, aaa.lc_limit, aaa.liftdate, aaa.date_created, aaa.exch_company_id, aaa.exch_exam_student_id, aaa.exch_year, CASE WHEN (aaa.exch_request_state_id >= 4) THEN 4 WHEN (aaa.exch_request_state_id = 2) THEN 1 WHEN ((now() >= aaa.liftdate) AND (aaa.lok_por = 1)) THEN 1 WHEN ((now() >= aaa.liftdate) AND (aaa.lok_por > 1)) THEN 2 WHEN (((aaa.lc_limit - aaa.poradi_lc) >= 0) AND (aaa.lok_por = 1)) THEN 1 WHEN (((aaa.lc_limit - aaa.poradi_lc) > 0) AND (aaa.lok_por > 1)) THEN 2 WHEN (((aaa.lc_limit - aaa.poradi_lc) = 0) AND (aaa.lok_por > 1)) THEN 3 WHEN (((aaa.lc_limit - aaa.poradi_lc) IS NULL) AND (aaa.lok_por = 1)) THEN 1 WHEN (((aaa.lc_limit - aaa.poradi_lc) IS NULL) AND (aaa.lok_por > 1)) THEN 2 ELSE 3 END AS temprio FROM (SELECT "out".id, "out".exch_request_state_id, er_int_lcpor(es.id, es.exch_local_centre_id, ec."year") AS poradi_lc, er_int_lokpor(es.id, "out".exch_company_id, ec."year") AS lok_por, (SELECT rl.request_limit FROM exch_request_limits rl WHERE (es.exch_local_centre_id = rl.exch_local_centre_id)) AS lc_limit, (SELECT rl.liftdate FROM exch_request_limits rl WHERE (es.exch_local_centre_id = rl.exch_local_centre_id)) AS liftdate, "out".date_created, "out".exch_company_id, "out".exch_exam_student_id, ec."year" AS exch_year FROM ((exch_request "out" LEFT JOIN exch_exam_student es ON (("out".exch_exam_student_id = es.id))) LEFT JOIN exch_company ec ON (("out".exch_company_id = ec.id))) WHERE (("out".exch_request_state_id <= 2) OR ("out".exch_request_state_id = 3))) aaa;


ALTER TABLE exch.er_int_temp OWNER TO exchange;

--
-- Name: er_int_temp2; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW er_int_temp2 AS
    SELECT aaa.id, aaa.exch_request_state_id, aaa.poradi_lc, aaa.lok_por, aaa.lc_limit, aaa.liftdate, aaa.date_created, aaa.exch_company_id, aaa.exch_exam_student_id, aaa.exch_year, CASE WHEN (aaa.exch_request_state_id >= 4) THEN 4 WHEN (aaa.exch_request_state_id = 2) THEN 1 WHEN ((now() >= aaa.liftdate) AND (aaa.lok_por = 1)) THEN 1 WHEN ((now() >= aaa.liftdate) AND (aaa.lok_por > 1)) THEN 2 WHEN (((aaa.lc_limit - aaa.poradi_lc) >= 0) AND (aaa.lok_por = 1)) THEN 1 WHEN (((aaa.lc_limit - aaa.poradi_lc) > 0) AND (aaa.lok_por > 1)) THEN 2 WHEN (((aaa.lc_limit - aaa.poradi_lc) = 0) AND (aaa.lok_por > 1)) THEN 3 WHEN (((aaa.lc_limit - aaa.poradi_lc) IS NULL) AND (aaa.lok_por = 1)) THEN 1 WHEN (((aaa.lc_limit - aaa.poradi_lc) IS NULL) AND (aaa.lok_por > 1)) THEN 2 ELSE 3 END AS temprio FROM (SELECT "out".id, "out".exch_request_state_id, er_int_lcpor2(es.id, es.exch_local_centre_id, ec."year") AS poradi_lc, er_int_lokpor(es.id, "out".exch_company_id, ec."year") AS lok_por, (SELECT rl.request_limit FROM exch_request_limits rl WHERE (es.exch_local_centre_id = rl.exch_local_centre_id)) AS lc_limit, (SELECT rl.liftdate FROM exch_request_limits rl WHERE (es.exch_local_centre_id = rl.exch_local_centre_id)) AS liftdate, "out".date_created, "out".exch_company_id, "out".exch_exam_student_id, ec."year" AS exch_year FROM ((exch_request "out" LEFT JOIN exch_exam_student es ON (("out".exch_exam_student_id = es.id))) LEFT JOIN exch_company ec ON (("out".exch_company_id = ec.id))) WHERE (("out".exch_request_state_id <= 2) OR ("out".exch_request_state_id = 3))) aaa;


ALTER TABLE exch.er_int_temp2 OWNER TO exchange;

--
-- Name: er_int_temp_speed; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW er_int_temp_speed AS
    SELECT aaa.id, aaa.exch_request_state_id, aaa.poradi_lc, aaa.lok_por, aaa.lc_limit, aaa.liftdate, aaa.date_created, aaa.exch_company_id, aaa.exch_exam_student_id, aaa.exch_year, CASE WHEN (aaa.exch_request_state_id >= 4) THEN 4 WHEN (aaa.exch_request_state_id = 2) THEN 1 WHEN (aaa.lok_por = 1) THEN CASE WHEN (now() >= aaa.liftdate) THEN 1 WHEN ((aaa.lc_limit - aaa.poradi_lc) >= 0) THEN 1 WHEN ((aaa.lc_limit - aaa.poradi_lc) IS NULL) THEN 1 ELSE 3 END WHEN (aaa.lok_por > 1) THEN CASE WHEN (now() >= aaa.liftdate) THEN 2 WHEN ((aaa.lc_limit - aaa.poradi_lc) > 0) THEN 2 WHEN ((aaa.lc_limit - aaa.poradi_lc) = 0) THEN 3 WHEN ((aaa.lc_limit - aaa.poradi_lc) IS NULL) THEN 2 ELSE 3 END ELSE 3 END AS temprio FROM (SELECT "out".id, "out".exch_request_state_id, er_int_lcpor2(es.id, es.exch_local_centre_id, ec."year") AS poradi_lc, er_int_lokpor(es.id, "out".exch_company_id, ec."year") AS lok_por, rl.request_limit AS lc_limit, rl.liftdate, "out".date_created, "out".exch_company_id, "out".exch_exam_student_id, ec."year" AS exch_year FROM (((exch_request "out" LEFT JOIN exch_exam_student es ON (("out".exch_exam_student_id = es.id))) LEFT JOIN exch_request_limits rl ON ((es.exch_local_centre_id = rl.exch_local_centre_id))) LEFT JOIN exch_company ec ON (("out".exch_company_id = ec.id))) WHERE ("out".exch_request_state_id <= 3)) aaa;


ALTER TABLE exch.er_int_temp_speed OWNER TO exchange;

--
-- Name: exch_and_or; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_and_or (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE exch.exch_and_or OWNER TO exchange;

--
-- Name: exch_catalog; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_catalog (
    id integer NOT NULL,
    code character varying(20),
    name character varying(50),
    flag character(2)
);


ALTER TABLE exch.exch_catalog OWNER TO exchange;

--
-- Name: exch_company_count_year; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company_count_year (
    id integer DEFAULT nextval(('exch_count_company_year_id_seq'::text)::regclass) NOT NULL,
    "year" integer,
    count_oform integer,
    exch_local_centre_id integer,
    reservation character varying(5)
);


ALTER TABLE exch.exch_company_count_year OWNER TO exchange;

--
-- Name: exch_company_faculty; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company_faculty (
    exch_company_id integer,
    exch_faculty_id integer
);


ALTER TABLE exch.exch_company_faculty OWNER TO exchange;

--
-- Name: exch_company_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_company_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_company_id_seq OWNER TO exchange;

--
-- Name: exch_company_intraweb; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company_intraweb (
    ref_no character varying(50) NOT NULL,
    company_name character varying(255),
    business character varying(255),
    responsible character varying(255),
    phone character varying(50),
    fax character varying(50),
    email character varying(100),
    working_place character varying(50),
    airport character varying(50) DEFAULT 'Prague'::character varying,
    transport character varying(50),
    employees character varying(25),
    specialization character varying(255),
    study_level character varying(50),
    previous_training boolean,
    other_requirements text,
    kind_of_work text,
    number_weeks_min integer DEFAULT 6,
    number_weeks_max integer DEFAULT 12,
    work_closed character varying(50) DEFAULT '-'::character varying,
    gross_pay character varying(50),
    max_deduction character varying(50),
    lodging_arranged character varying(50),
    canteen_facilities boolean,
    estimated_lodging character varying(50) DEFAULT '3000'::character varying,
    estimated_living character varying(50) DEFAULT '6000'::character varying,
    deadline_for_nomination date,
    pay_by_iaeste character varying(50),
    pay_by_employer character varying(50),
    "year" integer,
    exch_user character varying,
    english_and_or character varying,
    german_and_or character varying,
    french_and_or character varying,
    exch_work_category_abbr character varying,
    english smallint,
    german smallint,
    french smallint,
    other_language character varying(50),
    other smallint,
    pay_per_gross smallint,
    pay_per_employer smallint,
    pay_per_iaeste smallint,
    pay_per_lodging smallint,
    pay_per_living smallint,
    business_or_product character varying(255),
    lock_public_edit smallint,
    hash_public character(40),
    ci_company_id integer,
    time_changed timestamp without time zone,
    exch_local_centre_id integer,
    exch_user_id integer,
    sent_to_email character varying(70),
    sent_to_company timestamp without time zone,
    id integer DEFAULT nextval(('exch.exch_company_id_seq'::text)::regclass) NOT NULL,
    faculty_text character varying(255),
    who_changed integer,
    training_available boolean,
    within_from date,
    within_to date,
    within_from2 date,
    within_to2 date,
    working_hours_per_week numeric(4,1) DEFAULT 40,
    daily_working_hours numeric(4,1) DEFAULT 8,
    street character varying(255),
    city character varying(255),
    currency character varying(10),
    exch_company_intraweb_status_id smallint
);


ALTER TABLE exch.exch_company_intraweb OWNER TO exchange;

--
-- Name: exch_company_intraweb_mask; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company_intraweb_mask (
    ref_no character varying NOT NULL,
    who_masked integer,
    time_masked timestamp with time zone DEFAULT now()
);


ALTER TABLE exch.exch_company_intraweb_mask OWNER TO exchange;

--
-- Name: exch_company_intraweb_status_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_company_intraweb_status_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_company_intraweb_status_id_seq OWNER TO exchange;

--
-- Name: exch_company_intraweb_status; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_company_intraweb_status (
    id smallint DEFAULT nextval('exch_company_intraweb_status_id_seq'::regclass) NOT NULL,
    name character varying(30),
    abbr character varying(5)
);


ALTER TABLE exch.exch_company_intraweb_status OWNER TO exchange;

--
-- Name: exch_count_company_year_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_count_company_year_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_count_company_year_id_seq OWNER TO exchange;

--
-- Name: exch_country; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_country (
    name_en character varying,
    name_cz character varying,
    abbr character varying(10) NOT NULL,
    id serial
);


ALTER TABLE exch.exch_country OWNER TO exchange;

--
-- Name: exch_employers_report_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_employers_report_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_employers_report_id_seq OWNER TO exchange;

--
-- Name: exch_employers_report; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_employers_report (
    id integer DEFAULT nextval('exch_employers_report_id_seq'::regclass) NOT NULL,
    exch_training_id integer,
    time_created timestamp without time zone,
    time_changed timestamp without time zone,
    performance integer,
    relations integer,
    attitude integer,
    ability_to_learn integer,
    dependability integer,
    quality_of_work integer,
    language_fluency integer,
    flexibility integer,
    other integer,
    kind_of_work text,
    comments text,
    imperssion_of_programme integer,
    imperssion_of_programme_elaborate text,
    imperssion_of_organisation integer,
    imperssion_of_organisation_elaborate text,
    next_year integer,
    signature character varying(100),
    employer_report_received timestamp without time zone
);


ALTER TABLE exch.exch_employers_report OWNER TO exchange;

--
-- Name: exch_exam; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam (
    id bigint DEFAULT nextval(('exch.exch_exam_id_seq'::text)::regclass) NOT NULL,
    exch_local_centre_id integer NOT NULL,
    date_of_exam date,
    "year" integer,
    place character varying(100),
    time_of_exam time without time zone,
    notice character varying(255),
    expired date,
    time_changed timestamp without time zone
);


ALTER TABLE exch.exch_exam OWNER TO exchange;

--
-- Name: exch_exam_country; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam_country (
    id integer NOT NULL,
    name character varying(100),
    spec character varying(100)
);


ALTER TABLE exch.exch_exam_country OWNER TO exchange;

--
-- Name: COLUMN exch_exam_country.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_country.name IS 'name';


--
-- Name: COLUMN exch_exam_country.spec; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_country.spec IS 'spec';


--
-- Name: exch_exam_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_exam_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_exam_id_seq OWNER TO exchange;

--
-- Name: exch_exam_student_file_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_exam_student_file_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_exam_student_file_seq OWNER TO exchange;

--
-- Name: exch_exam_student_file; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam_student_file (
    id integer DEFAULT nextval('exch_exam_student_file_seq'::regclass) NOT NULL,
    orig_name character varying(50),
    filename character varying(50),
    mimetype character varying(30),
    exchtype integer,
    student_id integer,
    date_uploaded timestamp without time zone
);


ALTER TABLE exch.exch_exam_student_file OWNER TO exchange;

--
-- Name: exch_exam_student_file_type; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam_student_file_type (
    id integer NOT NULL,
    name character varying(30),
    longname character varying(30)
);


ALTER TABLE exch.exch_exam_student_file_type OWNER TO exchange;

--
-- Name: exch_exam_student_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_exam_student_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_exam_student_id_seq OWNER TO exchange;

--
-- Name: exch_exam_university; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_exam_university (
    id integer DEFAULT nextval(('exch.exch_exam_university_id_seq'::text)::regclass) NOT NULL,
    name character varying(100),
    exch_local_centre_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE exch.exch_exam_university OWNER TO exchange;

--
-- Name: COLUMN exch_exam_university.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_university.name IS 'name';


--
-- Name: COLUMN exch_exam_university.exch_local_centre_id; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_exam_university.exch_local_centre_id IS 'automatické přidělení LC pro registraci studenta';


--
-- Name: exch_exam_university_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_exam_university_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_exam_university_id_seq OWNER TO exchange;

--
-- Name: exch_faculty; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_faculty (
    id integer DEFAULT nextval(('exch.exch_faculty_id_seq'::text)::regclass) NOT NULL,
    name character varying(100),
    active boolean DEFAULT true NOT NULL
);


ALTER TABLE exch.exch_faculty OWNER TO exchange;

--
-- Name: COLUMN exch_faculty.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_faculty.name IS 'name';


--
-- Name: COLUMN exch_faculty.active; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_faculty.active IS 'non active faculties are not displayed in new forms';


--
-- Name: exch_faculty_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_faculty_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    MINVALUE 50
    CACHE 1;


ALTER TABLE exch.exch_faculty_id_seq OWNER TO exchange;

--
-- Name: exch_language; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_language (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE exch.exch_language OWNER TO exchange;

--
-- Name: COLUMN exch_language.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_language.name IS 'name';


--
-- Name: exch_language_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_language_id_seq OWNER TO exchange;

--
-- Name: exch_language_knowledge; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_language_knowledge (
    id integer DEFAULT nextval(('exch.exch_language_knowledge_id_seq'::text)::regclass) NOT NULL,
    exch_exam_student_id integer,
    exch_language_id integer,
    priority integer,
    exch_language_level_id integer
);


ALTER TABLE exch.exch_language_knowledge OWNER TO exchange;

--
-- Name: exch_language_knowledge_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_language_knowledge_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_language_knowledge_id_seq OWNER TO exchange;

--
-- Name: exch_language_level; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_language_level (
    id integer DEFAULT nextval(('exch.exch_language_level_id_seq'::text)::regclass) NOT NULL,
    name character varying(100)
);


ALTER TABLE exch.exch_language_level OWNER TO exchange;

--
-- Name: COLUMN exch_language_level.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_language_level.name IS 'name';


--
-- Name: exch_marital_status; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_marital_status (
    id integer NOT NULL,
    value character varying(10)
);


ALTER TABLE exch.exch_marital_status OWNER TO exchange;

--
-- Name: exch_month; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_month (
    id integer NOT NULL,
    name character varying(20)
);


ALTER TABLE exch.exch_month OWNER TO exchange;

--
-- Name: COLUMN exch_month.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_month.name IS 'name';


--
-- Name: exch_month_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_month_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_month_id_seq OWNER TO exchange;

--
-- Name: exch_month_or_week; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_month_or_week (
    id smallint,
    mw character varying
);


ALTER TABLE exch.exch_month_or_week OWNER TO exchange;

--
-- Name: exch_not_prefered_country; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_not_prefered_country (
    id integer DEFAULT nextval(('exch.exch_not_prefered_country_id_seq'::text)::regclass) NOT NULL,
    exch_exam_student_id integer,
    exch_country_id integer,
    priority integer
);


ALTER TABLE exch.exch_not_prefered_country OWNER TO exchange;

--
-- Name: exch_not_prefered_country_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_not_prefered_country_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_not_prefered_country_id_seq OWNER TO exchange;

--
-- Name: postpraxe_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE postpraxe_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.postpraxe_id_seq OWNER TO exchange;

--
-- Name: exch_postpraxe; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_postpraxe (
    id integer DEFAULT nextval('postpraxe_id_seq'::regclass) NOT NULL,
    ref_no character varying(50),
    faculty character varying,
    specialization character varying,
    date_created date,
    filename character varying(100),
    abb boolean,
    deadline date,
    time_changed time without time zone
);


ALTER TABLE exch.exch_postpraxe OWNER TO exchange;

--
-- Name: exch_prefered_country; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_prefered_country (
    id integer DEFAULT nextval(('exch.exch_prefered_country_id_seq'::text)::regclass) NOT NULL,
    exch_exam_student_id integer,
    exch_country_id integer,
    priority integer
);


ALTER TABLE exch.exch_prefered_country OWNER TO exchange;

--
-- Name: exch_prefered_country_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_prefered_country_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_prefered_country_id_seq OWNER TO exchange;

--
-- Name: exch_prefered_faculty; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_prefered_faculty (
    id integer DEFAULT nextval(('exch.exch_prefered_study_section_id_seq'::text)::regclass) NOT NULL,
    exch_exam_student_id integer,
    exch_faculty_id integer,
    priority integer
);


ALTER TABLE exch.exch_prefered_faculty OWNER TO exchange;

--
-- Name: exch_prefered_month; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_prefered_month (
    id integer DEFAULT nextval(('exch.exch_prefered_month_id_seq'::text)::regclass) NOT NULL,
    exch_month_id integer,
    exch_exam_student_id integer
);


ALTER TABLE exch.exch_prefered_month OWNER TO exchange;

--
-- Name: exch_prefered_month_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_prefered_month_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_prefered_month_id_seq OWNER TO exchange;

--
-- Name: exch_prefered_study_section_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_prefered_study_section_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_prefered_study_section_id_seq OWNER TO exchange;

--
-- Name: exch_public_text; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_public_text (
    id integer DEFAULT nextval(('exch.exch_public_text_id_seq'::text)::regclass) NOT NULL,
    "type" character varying(10),
    text_value text,
    "key" character varying(50)
);


ALTER TABLE exch.exch_public_text OWNER TO exchange;

--
-- Name: exch_public_text_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_public_text_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_public_text_id_seq OWNER TO exchange;

--
-- Name: exch_request_backup_2012; Type: TABLE; Schema: exch; Owner: alfi; Tablespace: 
--

CREATE TABLE exch_request_backup_2012 (
    exch_company_id integer,
    exch_exam_student_id integer,
    priority integer,
    date_created timestamp without time zone,
    exch_request_state_id smallint,
    id integer,
    time_changed timestamp without time zone,
    who_changed integer,
    time_send_email timestamp without time zone,
    "comment" text
);


ALTER TABLE exch.exch_request_backup_2012 OWNER TO alfi;

--
-- Name: TABLE exch_request_backup_2012; Type: COMMENT; Schema: exch; Owner: alfi
--

COMMENT ON TABLE exch_request_backup_2012 IS 'záloha před promazáním requestů minulých let';


--
-- Name: exch_request_prio; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW exch_request_prio AS
    SELECT erq.id, erq.exch_exam_student_id, erq.exch_company_id, ecc."year" AS exch_year, CASE WHEN ((ccc.poradi = 1) AND (ccc.temprio = 2)) THEN 1 WHEN (ccc.temprio IS NULL) THEN 4 ELSE ccc.temprio END AS priority, CASE WHEN (erq.exch_request_state_id = 3) THEN 1 ELSE ccc.poradi END AS poradi FROM (((SELECT bbb.id, bbb.exch_request_state_id, bbb.poradi_lc, bbb.lok_por, bbb.lc_limit, bbb.liftdate, bbb.date_created, bbb.exch_company_id, bbb.exch_exam_student_id, bbb.exch_year, bbb.temprio, er_int_fin_por2((bbb.exch_exam_student_id)::bigint, bbb.exch_company_id, bbb.exch_year) AS poradi FROM (SELECT er_int_temp.id, er_int_temp.exch_request_state_id, er_int_temp.poradi_lc, er_int_temp.lok_por, er_int_temp.lc_limit, er_int_temp.liftdate, er_int_temp.date_created, er_int_temp.exch_company_id, er_int_temp.exch_exam_student_id, er_int_temp.exch_year, er_int_temp.temprio FROM er_int_temp_speed er_int_temp) bbb) ccc RIGHT JOIN exch_request erq ON ((ccc.id = erq.id))) LEFT JOIN exch_company ecc ON ((erq.exch_company_id = ecc.id))) ORDER BY erq.id;


ALTER TABLE exch.exch_request_prio OWNER TO exchange;

--
-- Name: exch_request_state_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_request_state_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_request_state_id_seq OWNER TO exchange;

--
-- Name: exch_request_state; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_request_state (
    id integer DEFAULT nextval('exch_request_state_id_seq'::regclass) NOT NULL,
    name character varying NOT NULL,
    short_name character varying(3) NOT NULL,
    oform_available boolean NOT NULL,
    lc_editable boolean DEFAULT true
);


ALTER TABLE exch.exch_request_state OWNER TO exchange;

--
-- Name: TABLE exch_request_state; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON TABLE exch_request_state IS 'stav požadavků o praxi';


--
-- Name: exch_residency_length; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_residency_length (
    id integer NOT NULL,
    name character varying(20)
);


ALTER TABLE exch.exch_residency_length OWNER TO exchange;

--
-- Name: COLUMN exch_residency_length.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_residency_length.name IS 'name';


--
-- Name: exch_residency_length_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_residency_length_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_residency_length_id_seq OWNER TO exchange;

--
-- Name: exch_sex; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_sex (
    id integer NOT NULL,
    value character varying(10)
);


ALTER TABLE exch.exch_sex OWNER TO exchange;

--
-- Name: exch_student; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_student (
    id serial NOT NULL,
    exch_exam_student_id integer,
    first_name character varying(100),
    family_name character varying(100),
    phone character varying(50),
    language1_id integer,
    language2_id integer,
    language3_id integer,
    language1_level_id integer,
    language2_level_id integer,
    language3_level_id integer,
    language4_other_level_id integer,
    language5_other_level_id integer,
    email character varying(100),
    language4_other character varying(50),
    language5_other character varying(50),
    exch_local_centre_id integer,
    "year" integer,
    other_names character varying(100),
    home_address_street character varying(255),
    home_address_city character varying(255),
    home_address_code character varying(30),
    during_address_street character varying(255),
    during_address_city character varying(255),
    during_address_code character varying(30),
    during_phone character varying(50),
    date_of_birth date,
    place_of_birth character varying(50),
    nationality character varying(50),
    passport_no character varying(50),
    passport_issued_at character varying(50),
    passport_valid date,
    sex_id smallint DEFAULT 1,
    marital_status_id smallint DEFAULT 1,
    medically_fit boolean DEFAULT true,
    university character varying(50),
    faculty character varying(255),
    specialization character varying(255),
    completed_years_of_study numeric(2,1),
    total_years_required numeric(2,1),
    fees_amount character varying(50),
    fees_return timestamp without time zone,
    fees_comment text,
    date_created timestamp without time zone,
    date_changed timestamp without time zone,
    exch_user_id integer,
    nominating_country_abbr character varying(10)
);


ALTER TABLE exch.exch_student OWNER TO exchange;

--
-- Name: COLUMN exch_student.nominating_country_abbr; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_student.nominating_country_abbr IS 'pozor, foreign key přes textové ID!';


--
-- Name: exch_study_level; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_study_level (
    id serial NOT NULL,
    name character varying(50)
);


ALTER TABLE exch.exch_study_level OWNER TO exchange;

--
-- Name: exch_study_program; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_study_program (
    id integer NOT NULL,
    name character varying(50)
);


ALTER TABLE exch.exch_study_program OWNER TO exchange;

--
-- Name: COLUMN exch_study_program.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_study_program.name IS 'name';


--
-- Name: exch_study_program_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_study_program_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_study_program_id_seq OWNER TO exchange;

--
-- Name: exch_title; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_title (
    id integer NOT NULL,
    name character varying(20)
);


ALTER TABLE exch.exch_title OWNER TO exchange;

--
-- Name: COLUMN exch_title.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_title.name IS 'name';


--
-- Name: exch_title_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_title_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_title_id_seq OWNER TO exchange;

--
-- Name: exch_training; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_training (
    id serial NOT NULL,
    exch_company_id integer,
    exch_student_id integer,
    exch_training_report_id integer,
    exch_user_id integer,
    exch_training_state_id integer,
    time_changed timestamp without time zone,
    time_created timestamp without time zone,
    date_acceptation_requested date,
    exch_training_state_cancel_id integer,
    nomination_sent timestamp without time zone,
    acceptation_date timestamp without time zone,
    n5abc_printed timestamp without time zone,
    n5b_received timestamp without time zone,
    employer_report_requested timestamp without time zone,
    student_report_requested timestamp without time zone,
    student_arrival_changed timestamp without time zone,
    arrival_visa_no character varying(255),
    arrival_date date,
    arrival_time time without time zone,
    arrival_by character varying(10),
    arrival_flight_train_no character varying(30),
    arrival_place character varying(50),
    living_allowance character varying(50),
    living_allowance_pay_per smallint,
    technical_report boolean DEFAULT true,
    lodging boolean DEFAULT true,
    lodging_address character varying(255),
    lodging_period_from date,
    lodging_period_to date,
    training_period_from date,
    training_period_to date,
    note text,
    arrival_coming_from character varying(80),
    living_allowance_currency character varying(10),
    code character varying(50),
    cancel_note text,
    employer_report_received timestamp without time zone
);


ALTER TABLE exch.exch_training OWNER TO exchange;

--
-- Name: COLUMN exch_training.employer_report_received; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_training.employer_report_received IS 'tmp do opraveni u drs';


--
-- Name: exch_training_state; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_training_state (
    id serial NOT NULL,
    available boolean,
    name character varying,
    short_name character varying(3)
);


ALTER TABLE exch.exch_training_state OWNER TO exchange;

--
-- Name: TABLE exch_training_state; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON TABLE exch_training_state IS 'stavy praxe';


--
-- Name: exch_training_state_cancel; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_training_state_cancel (
    id serial NOT NULL,
    "type" character(1),
    reason character varying
);


ALTER TABLE exch.exch_training_state_cancel OWNER TO exchange;

--
-- Name: TABLE exch_training_state_cancel; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON TABLE exch_training_state_cancel IS 'duvody zruseni praxe';


--
-- Name: exch_translate; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_translate (
    id serial NOT NULL,
    lang character varying(5),
    table_name character varying(50),
    t_text text,
    table_id character varying(11)
);


ALTER TABLE exch.exch_translate OWNER TO exchange;

--
-- Name: exch_translate_tables; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_translate_tables (
    id serial NOT NULL,
    name character varying(80)
);


ALTER TABLE exch.exch_translate_tables OWNER TO exchange;

--
-- Name: exch_work_category; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_work_category (
    category_name character varying,
    abbr character varying,
    id integer NOT NULL
);


ALTER TABLE exch.exch_work_category OWNER TO exchange;

--
-- Name: exch_year_class; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_year_class (
    id integer NOT NULL,
    name character varying(10)
);


ALTER TABLE exch.exch_year_class OWNER TO exchange;

--
-- Name: COLUMN exch_year_class.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_year_class.name IS 'name';


--
-- Name: exch_year_class_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_year_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_year_class_id_seq OWNER TO exchange;

--
-- Name: exch_yes_no; Type: TABLE; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE TABLE exch_yes_no (
    id integer NOT NULL,
    name character varying(20)
);


ALTER TABLE exch.exch_yes_no OWNER TO exchange;

--
-- Name: COLUMN exch_yes_no.name; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON COLUMN exch_yes_no.name IS 'name';


--
-- Name: exch_yes_no_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE exch_yes_no_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.exch_yes_no_id_seq OWNER TO exchange;

SET search_path = public, pg_catalog;

--
-- Name: lc; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE lc (
    id integer NOT NULL,
    jmeno character varying(50),
    zkratka character varying(10),
    username character varying(10),
    www character varying(100),
    email_exchange character varying(100)
);


ALTER TABLE public.lc OWNER TO intranet;

SET search_path = exch, pg_catalog;

--
-- Name: local_centre; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW local_centre AS
    SELECT lc.id, 'CZ'::text AS country, lc.jmeno AS centre_name, lc.zkratka AS abbr, lc.username AS short_name, lc.www, lc.email_exchange AS email FROM public.lc WHERE (((lc.id < 20) AND (lc.id <> 8)) AND (lc.id <> 11));


ALTER TABLE exch.local_centre OWNER TO exchange;

--
-- Name: VIEW local_centre; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON VIEW local_centre IS 'lokalni centra, zatim pouze CZ';


--
-- Name: trainees_now; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW trainees_now AS
    SELECT exch_training.id, s.first_name, s.family_name, c.ref_no, c.company_name, lc.abbr AS lc, co.name_cz AS country, c.working_place, COALESCE(c.specialization, c.faculty_text) AS faculty_text, exch_training.training_period_from, exch_training.training_period_to FROM ((((exch_training LEFT JOIN exch_company c ON ((exch_training.exch_company_id = c.id))) LEFT JOIN exch_student s ON ((exch_training.exch_student_id = s.id))) LEFT JOIN exch_country co ON (((s.nominating_country_abbr)::text = (co.abbr)::text))) LEFT JOIN local_centre lc ON ((c.exch_local_centre_id = lc.id))) WHERE (((exch_training.exch_training_state_id = 12) AND (exch_training.training_period_from <= '2012-05-29'::date)) AND (exch_training.training_period_to >= ('now'::text)::date)) ORDER BY exch_training.training_period_from;


ALTER TABLE exch.trainees_now OWNER TO exchange;

--
-- Name: training_state_cancel_id_seq; Type: SEQUENCE; Schema: exch; Owner: exchange
--

CREATE SEQUENCE training_state_cancel_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE exch.training_state_cancel_id_seq OWNER TO exchange;

SET search_path = public, pg_catalog;

--
-- Name: users; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE users (
    username character varying(50) NOT NULL,
    "password" character varying(30),
    rights character(50),
    id integer NOT NULL,
    groupname character varying(30) NOT NULL,
    lastlogin timestamp with time zone,
    fullname character varying(50),
    email character varying(50),
    password_hash character varying(32),
    m_time timestamp without time zone,
    m_uid integer,
    ci_kontakt integer,
    udb_username character varying(20) DEFAULT 'uknown'::character varying NOT NULL,
    udb_id integer
);


ALTER TABLE public.users OWNER TO intranet;

--
-- Name: COLUMN users.password_hash; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN users.password_hash IS 'po integraci s udb pouze null=neaktivni ucet, heslo se overuje z udb';


--
-- Name: COLUMN users.udb_username; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN users.udb_username IS 'username z UDB';


--
-- Name: COLUMN users.udb_id; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN users.udb_id IS 'id z UDB, přidat references?';


SET search_path = exch, pg_catalog;

--
-- Name: user; Type: VIEW; Schema: exch; Owner: exchange
--

CREATE VIEW "user" AS
    SELECT users.id, users.udb_username AS user_name, users.fullname AS full_name, users.email, users.groupname AS local_centre, ("substring"((users.rights)::text, 6, 2) || "substring"((users.rights)::text, 18, 1)) AS rights FROM public.users;


ALTER TABLE exch."user" OWNER TO exchange;

--
-- Name: VIEW "user"; Type: COMMENT; Schema: exch; Owner: exchange
--

COMMENT ON VIEW "user" IS 'uzivatele z intranetu';


SET search_path = public, pg_catalog;

--
-- Name: _groups; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE _groups (
    id character(1) NOT NULL,
    "level" integer,
    descr character varying(50),
    "grant" character varying
);


ALTER TABLE public._groups OWNER TO intranet;

--
-- Name: COLUMN _groups."grant"; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN _groups."grant" IS 'co může uživatel s právem přidělit někomu dalšímu';


--
-- Name: alumni_users; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE alumni_users (
    id integer,
    username character varying(10),
    fullname character varying(50),
    email character varying(50),
    "password" text,
    lc character varying(10)
);


ALTER TABLE public.alumni_users OWNER TO intranet;

--
-- Name: badgets; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE badgets (
    jmeno character varying,
    person1 character varying
);


ALTER TABLE public.badgets OWNER TO intranet;

--
-- Name: TABLE badgets; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE badgets IS 'old-not used';


--
-- Name: board; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE board (
    id integer NOT NULL,
    lc character varying(10) NOT NULL,
    autor character varying(20),
    nazev character varying(50),
    skupina integer,
    datumcas timestamp with time zone,
    text character varying,
    replyto integer,
    strom character varying,
    poradi smallint
);


ALTER TABLE public.board OWNER TO intranet;

--
-- Name: board_name; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE board_name (
    id integer NOT NULL,
    vlastnik character varying(50) NOT NULL,
    nazev character varying(100),
    popis character varying
);


ALTER TABLE public.board_name OWNER TO intranet;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE categories (
    name character varying,
    abbr character varying
);


ALTER TABLE public.categories OWNER TO intranet;

--
-- Name: ci_adresy; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW ci_adresy AS
    SELECT f.lc_reg, f.nazev, k.jmeno_prijmeni, f.ulice, f.psc, f.mesto FROM (ci_kontakt k RIGHT JOIN ci_firma f ON ((k.firma = f.id))) ORDER BY f.nazev;


ALTER TABLE public.ci_adresy OWNER TO intranet;

--
-- Name: ci_cvut_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_cvut_tmp (
    id integer,
    nazev character varying,
    osloveni1 character varying,
    osloveni5 character varying,
    osoba character varying,
    uilce character varying,
    mesto character varying,
    psc character varying,
    kpp2003 character varying
);


ALTER TABLE public.ci_cvut_tmp OWNER TO intranet;

--
-- Name: ci_faktura; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura (
    id integer NOT NULL,
    cislo integer NOT NULL,
    rok integer NOT NULL,
    typ smallint NOT NULL,
    jazyk character varying(10),
    nazev character varying,
    firma integer NOT NULL,
    firma_prijemce character varying,
    firma_ico character varying(100),
    lc character varying(50) NOT NULL,
    lc_kontakt1 character varying,
    lc_kontakt2 character varying,
    lc_ico character varying(100),
    lc_banka_ucet character varying(100),
    lc_banka_ks character varying(50),
    lc_banka_vs character varying(50),
    lc_text character varying,
    uhrada character varying(50),
    castka integer,
    mena character varying(10),
    datum_vystaveni date,
    datum_splatnosti date,
    datum_plneni date,
    datum_tisku date,
    datum_uhrazeni date,
    stav_uhrazeni smallint,
    poznamka character varying,
    vystavil character varying(100),
    mtime timestamp without time zone,
    muser character varying(50),
    prijata_platba integer,
    firma_odberatel character varying
);


ALTER TABLE public.ci_faktura OWNER TO intranet;

--
-- Name: ci_faktura_lang; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_lang (
    id integer NOT NULL,
    "key" character varying(255),
    value text,
    locale character varying(10)
);


ALTER TABLE public.ci_faktura_lang OWNER TO intranet;

--
-- Name: ci_faktura_polozka; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_polozka (
    id integer NOT NULL,
    faktura integer NOT NULL,
    popis character varying NOT NULL,
    pocet integer NOT NULL,
    cena integer NOT NULL,
    dph integer,
    projekt integer,
    poplatek_nc boolean DEFAULT true
);


ALTER TABLE public.ci_faktura_polozka OWNER TO intranet;

--
-- Name: COLUMN ci_faktura_polozka.poplatek_nc; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_faktura_polozka.poplatek_nc IS 'plati ze z polozky poplatky NC?';


--
-- Name: ci_faktura_seznam; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_seznam (
    id integer NOT NULL,
    lc character varying(50) NOT NULL,
    popis character varying NOT NULL,
    cena integer,
    dph integer,
    mtime timestamp with time zone,
    muser character varying(50),
    projekt integer,
    poplatek_nc boolean DEFAULT true,
    verejna boolean DEFAULT true
);


ALTER TABLE public.ci_faktura_seznam OWNER TO intranet;

--
-- Name: COLUMN ci_faktura_seznam.poplatek_nc; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_faktura_seznam.poplatek_nc IS 'plati ze z polozky poplatky NC?';


--
-- Name: COLUMN ci_faktura_seznam.verejna; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_faktura_seznam.verejna IS 'položku vidí ostatní LC? zatím nepoužíváno..';


--
-- Name: ci_faktura_stav; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_stav (
    id smallint NOT NULL,
    stav character varying(50),
    barva character varying(20)
);


ALTER TABLE public.ci_faktura_stav OWNER TO intranet;

--
-- Name: ci_faktura_text; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_text (
    id integer NOT NULL,
    lc character varying(50) NOT NULL,
    nazev character varying(100),
    text character varying,
    mtime timestamp without time zone,
    muser character varying(50)
);


ALTER TABLE public.ci_faktura_text OWNER TO intranet;

--
-- Name: ci_faktura_typ; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_typ (
    id smallint NOT NULL,
    nazev character varying(100),
    zkratka character varying(10),
    vs character varying(2),
    doklad character varying(100),
    popis character varying(100)
);


ALTER TABLE public.ci_faktura_typ OWNER TO intranet;

--
-- Name: ci_faktura_typ_text; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_typ_text (
    id integer NOT NULL,
    typ integer NOT NULL,
    locale character varying(10) NOT NULL,
    text character varying
);


ALTER TABLE public.ci_faktura_typ_text OWNER TO intranet;

--
-- Name: TABLE ci_faktura_typ_text; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE ci_faktura_typ_text IS 'preklady ci_faktura_typ';


--
-- Name: ci_faktura_ucet; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_faktura_ucet (
    id integer NOT NULL,
    lc character varying(50) NOT NULL,
    banka character varying(100),
    cislo character varying(100),
    popis character varying,
    mtime timestamp without time zone,
    muser character varying(50)
);


ALTER TABLE public.ci_faktura_ucet OWNER TO intranet;

--
-- Name: ci_firma_dic_bak; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_firma_dic_bak (
    id integer,
    dic character varying(15),
    nazev character varying
);


ALTER TABLE public.ci_firma_dic_bak OWNER TO intranet;

--
-- Name: ci_firma_historie; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_firma_historie (
    id integer DEFAULT nextval(('s_ci_firma_historie'::text)::regclass) NOT NULL,
    firma integer NOT NULL,
    puvodni_lc character varying(50),
    puvodni_nazev character varying,
    mtime timestamp with time zone,
    muser character varying(20),
    poznamka character varying
);


ALTER TABLE public.ci_firma_historie OWNER TO intranet;

--
-- Name: ci_firma_odkaz; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_firma_odkaz (
    id serial NOT NULL,
    firma integer NOT NULL,
    typ integer NOT NULL,
    nazev character varying,
    text text,
    mtime timestamp without time zone NOT NULL,
    muid integer NOT NULL,
    lc character varying(50) NOT NULL
);


ALTER TABLE public.ci_firma_odkaz OWNER TO intranet;

--
-- Name: ci_firma_odkaz_typ; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_firma_odkaz_typ (
    id serial NOT NULL,
    nazev character varying NOT NULL,
    barva character varying,
    poradi integer DEFAULT 1000 NOT NULL,
    url_prefix character varying
);


ALTER TABLE public.ci_firma_odkaz_typ OWNER TO intranet;

--
-- Name: TABLE ci_firma_odkaz_typ; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE ci_firma_odkaz_typ IS 'číselník typů odkazů';


--
-- Name: COLUMN ci_firma_odkaz_typ.url_prefix; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_firma_odkaz_typ.url_prefix IS 'začátek adresy';


--
-- Name: ci_hledani; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_hledani (
    id integer NOT NULL,
    "user" integer,
    mtime timestamp with time zone,
    typ integer NOT NULL,
    nazev character varying(50),
    lc character varying(50),
    text1 character varying(100),
    text2 character varying(100),
    text3 character varying(100),
    text4 character varying(100),
    text5 character varying(100),
    text6 character varying(100),
    cislo1 integer,
    cislo2 integer,
    datum1 date,
    datum2 date
);


ALTER TABLE public.ci_hledani OWNER TO intranet;

--
-- Name: ci_hledani_bak; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_hledani_bak (
    id integer,
    firma character varying(50),
    ico character varying(50),
    mesto character varying(50),
    lc_reg character varying(50),
    kontakt_jmeno character varying(50),
    poznamka character varying(50),
    "owner" character varying(50),
    mtime timestamp with time zone,
    nazev character varying(50)
);


ALTER TABLE public.ci_hledani_bak OWNER TO intranet;

--
-- Name: ci_hledani_bak2; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_hledani_bak2 (
    id integer NOT NULL,
    firma character varying(50),
    ico character varying(50),
    mesto character varying(50),
    lc_reg character varying(50),
    typ_spoluprace character varying(50),
    poznamka character varying(50),
    "owner" integer,
    mtime timestamp with time zone,
    nazev character varying(50),
    "next" integer
);


ALTER TABLE public.ci_hledani_bak2 OWNER TO intranet;

--
-- Name: ci_hledani_typ; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_hledani_typ (
    id integer NOT NULL,
    nazev character varying(100),
    zkratka character varying(10),
    next_state integer,
    lc character varying(100),
    text1 character varying(100),
    text2 character varying(100),
    text3 character varying(100),
    text4 character varying(100),
    text5 character varying(100),
    text6 character varying(100),
    cislo1 character varying(100),
    cislo2 character varying(100),
    datum1 character varying(100),
    datum2 character varying(100)
);


ALTER TABLE public.ci_hledani_typ OWNER TO intranet;

--
-- Name: ci_kontakt_jmeno_prijmeni; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_kontakt_jmeno_prijmeni (
    firma integer,
    id integer,
    ico integer,
    osloveni character varying(30),
    jmeno_prijmeni character varying(50),
    tel character varying(100),
    fax character varying(100),
    funkce character varying(50),
    popis_osoby character varying(100),
    poznamka character varying,
    email character varying(100),
    mobil character varying(100),
    telefax character varying(100),
    datum date,
    lc character varying(10),
    mtime timestamp with time zone,
    muser character varying(20),
    jmeno character varying(50),
    aktivni boolean,
    typ smallint,
    ulice character varying(50),
    mesto character varying(50),
    psc character(6)
);


ALTER TABLE public.ci_kontakt_jmeno_prijmeni OWNER TO intranet;

--
-- Name: ci_kontakt_oldid; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_kontakt_oldid (
    id integer
);


ALTER TABLE public.ci_kontakt_oldid OWNER TO intranet;

--
-- Name: ci_kontakt_typ; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_kontakt_typ (
    id integer NOT NULL,
    typ character varying(50),
    popis character varying(255),
    poradi integer,
    barva character varying(10)
);


ALTER TABLE public.ci_kontakt_typ OWNER TO intranet;

--
-- Name: TABLE ci_kontakt_typ; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE ci_kontakt_typ IS 'typy kontaktnich osob';


--
-- Name: ci_projekt; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_projekt (
    id integer NOT NULL,
    nazev character varying(100) NOT NULL,
    lc character varying(10) NOT NULL,
    rok smallint NOT NULL,
    vedouci character varying,
    uzaverka date,
    poznamka character varying,
    barva character varying(10),
    mtime timestamp with time zone,
    muser character varying(20),
    typ integer,
    koordinator integer,
    dokumentace character varying,
    notifikace boolean DEFAULT false
);


ALTER TABLE public.ci_projekt OWNER TO intranet;

--
-- Name: COLUMN ci_projekt.notifikace; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_projekt.notifikace IS 'posilat emailove notifikace?';


--
-- Name: s_ci_projekt_tym; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_projekt_tym
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_projekt_tym OWNER TO intranet;

--
-- Name: ci_projekt_tym; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_projekt_tym (
    id integer DEFAULT nextval('s_ci_projekt_tym'::regclass) NOT NULL,
    date_from date,
    date_to date,
    ci_kontakt integer,
    poznamka text,
    m_time timestamp without time zone,
    m_uid integer,
    ci_projekt integer NOT NULL
);


ALTER TABLE public.ci_projekt_tym OWNER TO intranet;

--
-- Name: ci_projekt_typ; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_projekt_typ (
    id integer DEFAULT nextval(('"ci_projekt_typ_id_seq"'::text)::regclass) NOT NULL,
    nazev character varying,
    narodni boolean
);


ALTER TABLE public.ci_projekt_typ OWNER TO intranet;

--
-- Name: TABLE ci_projekt_typ; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE ci_projekt_typ IS 'typy projektu';


--
-- Name: ci_projekt_typ_id_seq; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE ci_projekt_typ_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.ci_projekt_typ_id_seq OWNER TO intranet;

--
-- Name: ci_projekt_ucast; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_projekt_ucast (
    id smallint NOT NULL,
    text character varying(10)
);


ALTER TABLE public.ci_projekt_ucast OWNER TO intranet;

--
-- Name: ci_schuzka_backup; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_schuzka_backup (
    id integer NOT NULL,
    firma integer NOT NULL,
    lc character varying(10) NOT NULL,
    datum timestamp with time zone,
    kdo_firma character varying,
    kdo_lc character varying,
    typ character varying(50),
    prubeh character varying,
    projekt character varying,
    dalsi_schuzka character varying(50),
    perspektiva character varying,
    kontaktovat boolean,
    mtime timestamp with time zone,
    muser character varying(20),
    poznamka character varying
);


ALTER TABLE public.ci_schuzka_backup OWNER TO intranet;

--
-- Name: TABLE ci_schuzka_backup; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE ci_schuzka_backup IS 'nahrazeno ci_ukol';


--
-- Name: ci_spoluprace; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_spoluprace (
    id integer NOT NULL,
    firma integer NOT NULL,
    projekt integer NOT NULL,
    lc character varying(10) NOT NULL,
    muser character varying(20),
    mtime timestamp with time zone,
    ucast smallint,
    poznamka character varying
);


ALTER TABLE public.ci_spoluprace OWNER TO intranet;

--
-- Name: ci_spoluprace_detail; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_spoluprace_detail (
    id serial NOT NULL,
    mtime timestamp with time zone NOT NULL,
    muser integer NOT NULL,
    faktura_polozka integer NOT NULL,
    spoluprace integer NOT NULL,
    cena integer
);


ALTER TABLE public.ci_spoluprace_detail OWNER TO intranet;

--
-- Name: COLUMN ci_spoluprace_detail.cena; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_spoluprace_detail.cena IS 'částka za zvolenou položku na fakturu, pokud není z číselníku';


--
-- Name: ci_typ_spoluprace; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_typ_spoluprace (
    id integer NOT NULL,
    nazev character varying,
    value integer
);


ALTER TABLE public.ci_typ_spoluprace OWNER TO intranet;

--
-- Name: ci_ukol; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE ci_ukol (
    id integer NOT NULL,
    firma integer NOT NULL,
    lc character varying(10) NOT NULL,
    datum timestamp with time zone,
    kdo_firma character varying,
    kdo_lc integer,
    nazev character varying(100),
    prubeh character varying,
    projekt integer,
    dalsi_schuzka character varying,
    perspektiva character varying,
    ukol boolean,
    poznamka character varying,
    vlozil integer,
    vlozeno timestamp with time zone,
    pristup integer,
    cas_plan integer,
    cas_skutecny integer,
    dulezitost integer,
    mtime timestamp with time zone,
    muid integer,
    datum_od date
);


ALTER TABLE public.ci_ukol OWNER TO intranet;

--
-- Name: COLUMN ci_ukol.datum; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN ci_ukol.datum IS 'datum do';


--
-- Name: colors; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE colors (
    name character varying(50),
    code character varying(20)
);


ALTER TABLE public.colors OWNER TO intranet;

--
-- Name: company_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE company_tmp (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    worksclosed character varying,
    within_the_from2 character varying,
    within_the_to2 character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    pay_pmonth_dad character varying(50),
    pay_pmonth_employer character varying(50),
    "year" smallint
);


ALTER TABLE public.company_tmp OWNER TO intranet;

--
-- Name: TABLE company_tmp; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE company_tmp IS 'some backup?';


--
-- Name: countries; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW countries AS
    SELECT exch_country.name_en, exch_country.name_cz, exch_country.abbr FROM exch.exch_country;


ALTER TABLE public.countries OWNER TO intranet;

--
-- Name: VIEW countries; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW countries IS 'seznam zemi z exchange';


--
-- Name: countries_old; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE countries_old (
    name_en character varying,
    name_cz character varying,
    abbr character varying(10)
);


ALTER TABLE public.countries_old OWNER TO intranet;

--
-- Name: cvut_faktury; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_faktury (
    faktura_id integer NOT NULL,
    firma_id integer NOT NULL,
    firmanazev character varying(100),
    ico character varying(20),
    dic character varying(20),
    ulice character varying(100),
    mesto character varying(100),
    psc character varying(10),
    vystaveni integer,
    splatnost integer,
    uhrada integer,
    konstsym character varying(30),
    uhrazeno integer,
    poznamka text,
    soucet real,
    slevanzv character varying(30),
    slevasum real,
    dph real,
    celkem real,
    vysledna real,
    storno integer,
    fakturoval integer,
    stav integer,
    "timestamp" integer,
    slevaproc real,
    dphsum integer,
    interni integer NOT NULL,
    cislo character varying(20) NOT NULL
);


ALTER TABLE public.cvut_faktury OWNER TO intranet;

--
-- Name: TABLE cvut_faktury; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_faktury IS 'intranet cvut';


--
-- Name: cvut_faktury_items; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_faktury_items (
    fitem_id integer NOT NULL,
    faktura_id integer DEFAULT 0 NOT NULL,
    firma_id integer DEFAULT 0 NOT NULL,
    nazev character varying(255),
    pocet integer DEFAULT 1,
    cena real,
    stav integer DEFAULT (-1),
    dph real DEFAULT (- (1)::real),
    sleva real DEFAULT (0)::real,
    vysledna real DEFAULT (0)::real
);


ALTER TABLE public.cvut_faktury_items OWNER TO intranet;

--
-- Name: TABLE cvut_faktury_items; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_faktury_items IS 'intranet cvut';


--
-- Name: cvut_firma_prj; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_firma_prj (
    firma_id integer DEFAULT 0 NOT NULL,
    projekt_id integer DEFAULT 0 NOT NULL,
    user_id integer,
    stav integer DEFAULT 0
);


ALTER TABLE public.cvut_firma_prj OWNER TO intranet;

--
-- Name: TABLE cvut_firma_prj; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_firma_prj IS 'intranet cvut';


--
-- Name: cvut_firmy; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_firmy (
    firma_id integer NOT NULL,
    nazev character varying(100) DEFAULT ''::character varying NOT NULL,
    ico character varying(20),
    dic character varying(20),
    ulice character varying(100),
    mesto character varying(100),
    psc character varying(10),
    kontosoba character varying(100),
    osloveni character varying(50),
    tel character varying(50),
    fax character varying(50),
    email text,
    www character varying(100),
    obor character varying(100),
    fakulty text,
    poznamka text,
    pridal integer DEFAULT 0 NOT NULL,
    funkce character varying(50) DEFAULT ''::character varying NOT NULL,
    ico_num integer,
    ci_firma integer
);


ALTER TABLE public.cvut_firmy OWNER TO intranet;

--
-- Name: TABLE cvut_firmy; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_firmy IS 'intranet cvut';


--
-- Name: cvut_historie; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_historie (
    hist_id integer NOT NULL,
    firma_id integer NOT NULL,
    dulezitost integer,
    obsah text,
    "timestamp" bigint,
    pridal integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.cvut_historie OWNER TO intranet;

--
-- Name: TABLE cvut_historie; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_historie IS 'intranet cvut';


--
-- Name: cvut_projekty; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_projekty (
    projekt_id integer NOT NULL,
    datum_od integer,
    datum_do integer,
    nazev character varying(100),
    popis text,
    ci_projekt integer
);


ALTER TABLE public.cvut_projekty OWNER TO intranet;

--
-- Name: TABLE cvut_projekty; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_projekty IS 'intranet cvut';


--
-- Name: COLUMN cvut_projekty.ci_projekt; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN cvut_projekty.ci_projekt IS 'vazba na ci_projekt';


--
-- Name: cvut_users; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE cvut_users (
    user_id integer NOT NULL,
    "login" character varying(50) DEFAULT ''::character varying NOT NULL,
    passw character varying(32) DEFAULT ''::character varying NOT NULL,
    prava character varying(30) DEFAULT '000000000000000000000000000000'::character varying,
    lastlogin bigint,
    jmeno character varying(20) DEFAULT ''::character varying NOT NULL,
    prijmeni character varying(20) DEFAULT ''::character varying NOT NULL,
    funkceia character varying(50) DEFAULT ''::character varying,
    adresa character varying(100) DEFAULT ''::character varying,
    mail character varying(50) DEFAULT ''::character varying,
    iamail character varying(50) DEFAULT ''::character varying,
    mobil character varying(30) DEFAULT ''::character varying,
    tel character varying(30) DEFAULT ''::character varying
);


ALTER TABLE public.cvut_users OWNER TO intranet;

--
-- Name: TABLE cvut_users; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE cvut_users IS 'intranet cvut';


--
-- Name: download; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE download (
    id integer DEFAULT nextval(('s_download'::text)::regclass) NOT NULL,
    "owner" character varying(50) NOT NULL,
    author character varying(50),
    description character varying,
    groupid integer,
    ctime timestamp with time zone,
    mtime timestamp with time zone,
    filename character varying,
    preview character varying
);


ALTER TABLE public.download OWNER TO intranet;

--
-- Name: TABLE download; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE download IS 'old-not used';


--
-- Name: download_group; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE download_group (
    id integer NOT NULL,
    "owner" character varying(50) NOT NULL,
    parent integer,
    name character varying(100),
    description character varying,
    directory character varying
);


ALTER TABLE public.download_group OWNER TO intranet;

--
-- Name: TABLE download_group; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE download_group IS 'old-not used';


--
-- Name: emptytable; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE emptytable (
    id integer NOT NULL,
    integer_ integer,
    char_ character varying,
    date_ date,
    datetime_ timestamp with time zone
);


ALTER TABLE public.emptytable OWNER TO intranet;

--
-- Name: exch_ci_trainees; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_ci_trainees (
    id serial NOT NULL,
    rok smallint NOT NULL,
    lc smallint NOT NULL,
    ci_firma integer NOT NULL
);


ALTER TABLE public.exch_ci_trainees OWNER TO intranet;

--
-- Name: TABLE exch_ci_trainees; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE exch_ci_trainees IS 'id ci_firma pro trigger z echange';


--
-- Name: exch_coming_trainees; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW exch_coming_trainees AS
    SELECT exch_training.id, s.first_name, s.family_name, c.ref_no, c.company_name, lc.username AS lc, exch_training.arrival_date, exch_training.arrival_time, exch_training.arrival_place, co.name_cz AS country FROM ((((exch.exch_training LEFT JOIN exch.exch_company c ON ((exch_training.exch_company_id = c.id))) LEFT JOIN exch.exch_student s ON ((exch_training.exch_student_id = s.id))) LEFT JOIN exch.exch_country co ON (((s.nominating_country_abbr)::text = (co.abbr)::text))) LEFT JOIN lc ON ((c.exch_local_centre_id = lc.id))) WHERE ((exch_training.student_arrival_changed IS NOT NULL) AND (exch_training.arrival_date >= ('now'::text)::date)) ORDER BY exch_training.arrival_time, exch_training.arrival_place;


ALTER TABLE public.exch_coming_trainees OWNER TO intranet;

--
-- Name: VIEW exch_coming_trainees; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW exch_coming_trainees IS 'prijizdejici praktikanti';


--
-- Name: exch_company_in; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_company_in (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    worksclosed character varying,
    within_the_from2 character varying,
    within_the_to2 character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    pay_pmonth_dad character varying(50),
    pay_pmonth_employer character varying(50),
    "year" smallint
);


ALTER TABLE public.exch_company_in OWNER TO intranet;

--
-- Name: exch_country; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_country (
    country_id integer NOT NULL,
    c_name character varying(100),
    c_spec character varying(100)
);


ALTER TABLE public.exch_country OWNER TO intranet;

--
-- Name: exch_exam; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_exam (
    id integer NOT NULL,
    lc character varying(10),
    datum character varying(50),
    cas character varying(50),
    misto character varying(50),
    url character varying(50),
    poznamka character varying,
    rok integer,
    prihlaseni_do date
);


ALTER TABLE public.exch_exam OWNER TO intranet;

--
-- Name: exch_konkurz_email; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_konkurz_email (
    id integer NOT NULL,
    subject character varying,
    text1 text,
    text2 text,
    text3 text
);


ALTER TABLE public.exch_konkurz_email OWNER TO intranet;

--
-- Name: exch_konkurz_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_konkurz_tmp (
    id integer NOT NULL,
    titul character varying(50),
    jmeno character varying(100),
    prijmeni character varying(100),
    tel character varying(100),
    email character varying(100),
    faculty integer,
    program integer,
    rocnik integer,
    obor1 integer,
    obor2 integer,
    obor3 integer,
    specializace character varying,
    popis character varying,
    jazyk1n integer,
    jazyk1u integer,
    jazyk2n integer,
    jazyk2u integer,
    jazyk3n integer,
    jazyk3u integer,
    jazyk4n character varying(50),
    jazyk4u integer,
    jazyk5n character varying(50),
    jazyk5u integer,
    konkurzjazyk character varying(50),
    konkurzmisto integer,
    prumer1 double precision,
    prumer2 double precision,
    prumer3 double precision,
    prumer4 double precision,
    prumer5 double precision,
    prumer6 double precision,
    prumerpgs double precision,
    ocinnost boolean,
    clenia boolean,
    pref1 integer,
    pref2 integer,
    pref3 integer,
    pref4 integer,
    pref5 integer,
    npref1 integer,
    npref2 integer,
    npref3 integer,
    manpr integer,
    delka integer,
    termin character varying,
    date timestamp with time zone,
    "session" bigint,
    preferences_entered timestamp with time zone,
    body_konkurz_x integer,
    muser character varying(50),
    mtime_x character varying(50),
    mtime timestamp without time zone,
    body_konkurz real,
    poznamka character varying,
    email_poslan timestamp with time zone,
    body_pisemne real,
    body_poslech real,
    body_pohovor real,
    body_motivace real,
    body_prospech real,
    body_odb_cinnost real,
    body_iaeste real
);


ALTER TABLE public.exch_konkurz_tmp OWNER TO intranet;

--
-- Name: exch_lang; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_lang (
    lang_id integer NOT NULL,
    la_name_cz character varying(50),
    la_name_en character varying(50),
    la_abbrev character varying(50)
);


ALTER TABLE public.exch_lang OWNER TO intranet;

--
-- Name: exch_out_preffered_training; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_out_preffered_training (
    oform integer NOT NULL,
    student integer NOT NULL,
    priority integer
);


ALTER TABLE public.exch_out_preffered_training OWNER TO intranet;

--
-- Name: exch_out_preffered_training_pri; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_out_preffered_training_pri (
    id integer NOT NULL
);


ALTER TABLE public.exch_out_preffered_training_pri OWNER TO intranet;

--
-- Name: exch_participant; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_participant (
    p_id integer NOT NULL,
    p_email character varying(40) DEFAULT ''::character varying NOT NULL,
    p_first_name character varying(30) DEFAULT ''::character varying NOT NULL,
    p_surname character varying(40) DEFAULT ''::character varying NOT NULL,
    p_password character varying(40) DEFAULT ''::character varying NOT NULL,
    u_id integer DEFAULT 0,
    f_id integer DEFAULT 0,
    p_phone character varying(20),
    p_degree integer DEFAULT 1 NOT NULL,
    cs_id integer DEFAULT 1 NOT NULL,
    ys_id integer DEFAULT 1 NOT NULL,
    p_specialization character varying(50) DEFAULT ''::character varying NOT NULL,
    p_additional_specialization character varying(250) DEFAULT ''::character varying NOT NULL,
    sp_id_1 integer DEFAULT 1 NOT NULL,
    sp_id_2 integer DEFAULT 1 NOT NULL,
    sp_id_3 integer DEFAULT 1 NOT NULL,
    la_id_1 integer DEFAULT 0 NOT NULL,
    lal_id_1 integer DEFAULT 0 NOT NULL,
    la_id_2 integer DEFAULT 0 NOT NULL,
    lal_id_2 integer DEFAULT 0 NOT NULL,
    la_id_3 integer DEFAULT 0 NOT NULL,
    lal_id_3 integer DEFAULT 0 NOT NULL,
    la_name_4 character varying(30),
    lal_id_4 integer DEFAULT 0 NOT NULL,
    la_name_5 character varying(30),
    lal_id_5 integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.exch_participant OWNER TO intranet;

--
-- Name: exch_prefered_country; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_prefered_country (
    id integer NOT NULL,
    p_id integer DEFAULT 0 NOT NULL,
    c_id integer DEFAULT 0 NOT NULL,
    y_id integer DEFAULT 0 NOT NULL,
    te_id integer DEFAULT 0 NOT NULL,
    pc_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.exch_prefered_country OWNER TO intranet;

--
-- Name: exch_report_cz; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_report_cz (
    prijmeni character varying,
    jmeno character varying,
    titul character varying,
    vysoka_skola character varying,
    vysoka_skola_jina character varying,
    fakulta character varying,
    rocnik character varying,
    specializace character varying,
    cislo_praxe character varying,
    misto_pobytu character varying,
    termin_pobytu character varying,
    delka_pobytu character varying,
    vizum character varying,
    vizum_cena character varying,
    doprava character varying,
    pojisteni character varying,
    popis_mista character varying,
    popis_mesta character varying,
    popis_okoli character varying,
    firma character varying,
    napln_prace character varying,
    plat character varying,
    jazyky character varying,
    bydleni character varying,
    zabava_iaeste character varying,
    zabava_praktikanti character varying,
    sport_kultura character varying,
    jidlo character varying,
    komunikace_cr character varying,
    doporuceni character varying,
    nezapomenout character varying,
    prinos_praxe character varying,
    iaeste_zahranici character varying,
    iaeste_cr character varying,
    email character varying,
    web_student character varying,
    web_firma character varying,
    web_ostatni character varying,
    dalsi_text character varying,
    fotogalerie character varying,
    rok integer,
    id integer DEFAULT nextval(('s_exch_report_cz'::text)::regclass) NOT NULL
);


ALTER TABLE public.exch_report_cz OWNER TO intranet;

--
-- Name: exch_specialization; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_specialization (
    specialization_id integer NOT NULL,
    sp_value character varying(100)
);


ALTER TABLE public.exch_specialization OWNER TO intranet;

--
-- Name: exch_student_in; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_student_in (
    id integer NOT NULL,
    refno character varying(50),
    familyname character varying(50),
    firstname character varying(50),
    homeadress character varying(100),
    homecode character varying(50),
    hometown character varying(50),
    homephone character varying(50),
    duringaddress character varying(100),
    duringcode character varying(50),
    durringtown character varying(50),
    duringphone character varying(50),
    email character varying(50),
    dateofbirth date,
    placeofbirth character varying(50),
    nationality character varying(50),
    passportno character varying(50),
    issuepassportat character varying(50),
    passportvalid date,
    sex character varying(10),
    maritalstatus character varying(10),
    medicallyfit boolean,
    university character varying(100),
    faculty character varying(100),
    specialization character varying(100),
    language1 character varying(50),
    knowledge1 integer,
    language2 character varying(50),
    knowledge2 integer,
    language3 character varying(50),
    knowledge3 integer,
    comletedyearsofstudy integer,
    totalyearsrequired integer,
    date_from date,
    date_to date,
    technicalreport boolean,
    date_nominated date,
    nominatingcountry character varying,
    sendingcountry character varying,
    "year" smallint
);


ALTER TABLE public.exch_student_in OWNER TO intranet;

--
-- Name: exch_student_n5b; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_student_n5b (
    id integer NOT NULL,
    student_in integer NOT NULL,
    student_name character varying(100),
    student_email character varying(50),
    student_passport character varying(50),
    student_visa character varying(50),
    work_from date,
    work_to date,
    lodging_from date,
    lodging_to date,
    arrival_day date,
    arrival_time character varying(50),
    arrival_by character varying(10),
    arrival_no character varying(50),
    arrival_from character varying(50),
    arrival_place character varying(50),
    entered timestamp with time zone,
    sessionid character varying(50)
);


ALTER TABLE public.exch_student_n5b OWNER TO intranet;

--
-- Name: exch_tmp_company_in; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_tmp_company_in (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    worksclosed character varying,
    within_the_from2 character varying,
    within_the_to2 character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    pay_pmonth_dad character varying(50),
    pay_pmonth_employer character varying(50),
    "year" smallint
);


ALTER TABLE public.exch_tmp_company_in OWNER TO intranet;

--
-- Name: exch_trainees_all; Type: VIEW; Schema: public; Owner: alfi
--

CREATE VIEW exch_trainees_all AS
    SELECT exch_training.id, s.first_name, s.family_name, c.ref_no, c.company_name, lc.username AS lc, exch_training.arrival_date, exch_training.arrival_time, exch_training.arrival_place, co.name_cz AS country, exch_training.training_period_from, exch_training.training_period_to FROM ((((exch.exch_training LEFT JOIN exch.exch_company c ON ((exch_training.exch_company_id = c.id))) LEFT JOIN exch.exch_student s ON ((exch_training.exch_student_id = s.id))) LEFT JOIN exch.exch_country co ON (((s.nominating_country_abbr)::text = (co.abbr)::text))) LEFT JOIN lc ON ((c.exch_local_centre_id = lc.id))) WHERE (exch_training.exch_training_state_id = 12) ORDER BY exch_training.arrival_date, exch_training.arrival_place;


ALTER TABLE public.exch_trainees_all OWNER TO alfi;

--
-- Name: VIEW exch_trainees_all; Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON VIEW exch_trainees_all IS 'test calendar';


--
-- Name: exch_training_info; Type: VIEW; Schema: public; Owner: alfi
--

CREATE VIEW exch_training_info AS
    SELECT exch_company.company_name, exch_company.id AS company_id, exch_company."year", exch_company.ref_no, exch_student.first_name, exch_student.family_name, exch_student.id AS student_id, exch_training.id AS training_id, exch_student.email, lc.username AS lc_company, lc2.username AS lc_student FROM ((((exch.exch_company LEFT JOIN exch.exch_training ON ((exch_company.id = exch_training.exch_company_id))) LEFT JOIN exch.exch_student ON ((exch_training.exch_student_id = exch_student.id))) LEFT JOIN lc ON ((exch_company.exch_local_centre_id = lc.id))) LEFT JOIN lc lc2 ON ((exch_student.exch_local_centre_id = lc2.id)));


ALTER TABLE public.exch_training_info OWNER TO alfi;

--
-- Name: VIEW exch_training_info; Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON VIEW exch_training_info IS 'nahled na praxe pro vyhledavani';


--
-- Name: exch_university_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE exch_university_tmp (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE public.exch_university_tmp OWNER TO intranet;

--
-- Name: firmy_katalog; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE firmy_katalog (
    id integer NOT NULL,
    name character varying,
    profile character varying,
    looking_for character varying,
    requirements character varying,
    offers character varying,
    place character varying,
    person character varying,
    www character varying,
    email character varying(50),
    image character varying(50)
);


ALTER TABLE public.firmy_katalog OWNER TO intranet;

--
-- Name: sendmail_list; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE sendmail_list (
    id integer DEFAULT nextval(('s_sendmail_list'::text)::regclass) NOT NULL,
    listname character varying(50) NOT NULL,
    "comment" character varying(100),
    "owner" character varying(20) NOT NULL,
    created timestamp with time zone,
    private boolean DEFAULT true,
    closed boolean DEFAULT true,
    log boolean DEFAULT true,
    query character varying,
    maxsize integer,
    archive_dir character varying
);


ALTER TABLE public.sendmail_list OWNER TO intranet;

--
-- Name: global_abook; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW global_abook AS
    SELECT 'global'::text AS "owner", lc.username AS nickname, translate((k.jmeno)::text, 'éěščřžýáíúůĚŠČŘŽÝÁÍÉÚŮďň'::text, 'eescrzyaiuuESCRZYAIEUUdn'::text) AS firstname, translate((k.jmeno_prijmeni)::text, 'éěščřžýáíúůĚŠČŘŽÝÁÍÉÚŮďň'::text, 'eescrzyaiuuESCRZYAIEUUdn'::text) AS lastname, k.email, (translate((k.funkce)::text, 'éěščřžýáíúůĚŠČŘŽÝÁÍÉÚŮďň'::text, 'eescrzyaiuuESCRZYAIEUUdn'::text))::character varying AS label, lc.id AS lc_id FROM (ci_kontakt k LEFT JOIN lc ON ((k.firma = (- lc.id)))) WHERE (((k.firma <= 0) AND (k.firma > (-20))) AND (k.aktivni = true)) UNION SELECT 'global'::text AS "owner", 'skupina'::text AS nickname, ''::text AS firstname, (('NC - skupina'::text || ': '::text) || (l.listname)::text) AS lastname, ((l.listname)::text || ('@iaeste.cz'::character varying)::text) AS email, translate((l."comment")::text, 'éěščřžýáíúůĚŠČŘŽÝÁÍÉÚŮďň'::text, 'eescrzyaiuuESCRZYAIEUUdn'::text) AS label, (-1) AS lc_id FROM sendmail_list l WHERE (l.private = false) ORDER BY 7, 4;


ALTER TABLE public.global_abook OWNER TO intranet;

--
-- Name: VIEW global_abook; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW global_abook IS 'address book pro squirrelmail. read-only
';


--
-- Name: kpp2002; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp2002 (
    id bigint DEFAULT nextval(('s_kpp2002'::text)::regclass) NOT NULL,
    ci integer,
    obrazek character varying(50),
    jmeno character varying(100),
    profil character varying,
    hleda character varying,
    pozaduje character varying,
    nabizi character varying,
    sidlo character varying,
    www character varying(50),
    www2 character varying(50),
    kontakt1 character varying(100),
    kontakt2 character varying(100),
    email1 character varying(50),
    kontakt3 character varying(100),
    kontakt4 character varying(100),
    prezentace character varying(50),
    obory integer,
    uzivatel character varying(50),
    heslo character varying(50),
    komentar character varying,
    vyrizuje character varying(50),
    tel character varying(50),
    email character varying(50),
    lc character varying(10),
    "session" bigint,
    zmena timestamp with time zone,
    grafika character(2),
    stav character varying(100),
    edit boolean,
    zmena_kym character varying(50),
    rok integer,
    studentum_nabizi text,
    absolventum_s_praxi_nabizi text,
    studentum_nabizi_flags integer,
    varianta_stranky integer,
    souhlas_ikariera boolean
);


ALTER TABLE public.kpp2002 OWNER TO intranet;

--
-- Name: kpp2003_anketa; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp2003_anketa (
    id integer DEFAULT nextval(('s_kpp2003_anketa'::text)::regclass) NOT NULL,
    skola integer,
    fakulta integer,
    obor integer,
    rocnik integer,
    jinynavstevnik character varying(255),
    b01pocetfirem integer,
    b01spektrumoboru integer,
    b01terminvydani integer,
    b02tistenykpp integer,
    b03nedostatecneobory character varying(255),
    b04chybejicifirmy character varying(255),
    b05celkovydojem integer,
    b05celkovydojemjine character varying(255),
    b06lonskykpp integer,
    b06srovnanikpp integer,
    b06srovnanitext character varying(255),
    c08vydelek integer,
    c08zajimavaprace integer,
    c08vyhody integer,
    c08bydleni integer,
    c08vzdelavani integer,
    c08kariera integer,
    c09vppucast integer,
    c09vppsrovnani character varying(255),
    c10tisk integer,
    c10tisktext character varying(255),
    c10media integer,
    c10mediatext character varying(255),
    c10vpp integer,
    c10vpptext character varying(255),
    c10vyvesky integer,
    c10pracak integer,
    c10znami integer,
    c10internet integer,
    c10internettext character varying(255),
    d12email character varying(100),
    d12jmeno character varying(100),
    d12adresa character varying(255),
    d12mesto character varying(100),
    d12psc character varying(20),
    d12zasilatinfo integer,
    host character varying(50),
    date timestamp with time zone
);


ALTER TABLE public.kpp2003_anketa OWNER TO intranet;

--
-- Name: kpp2003_anketa_firma; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp2003_anketa_firma (
    id integer DEFAULT nextval(('s_kpp2003_anketa_firma'::text)::regclass) NOT NULL,
    dotaznik integer,
    firma integer
);


ALTER TABLE public.kpp2003_anketa_firma OWNER TO intranet;

--
-- Name: kpp_obory; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp_obory (
    id integer NOT NULL,
    jmeno character varying(50)
);


ALTER TABLE public.kpp_obory OWNER TO intranet;

--
-- Name: kpp_rocnik; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp_rocnik (
    rok integer,
    view_state integer
);


ALTER TABLE public.kpp_rocnik OWNER TO intranet;

--
-- Name: kpp_studentum_nabizi; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp_studentum_nabizi (
    id serial NOT NULL,
    nazev character varying(100) NOT NULL
);


ALTER TABLE public.kpp_studentum_nabizi OWNER TO intranet;

--
-- Name: TABLE kpp_studentum_nabizi; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE kpp_studentum_nabizi IS 'firma nabizi studentum, ciselnik';


--
-- Name: kpp_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp_tmp (
    id bigint,
    ci integer,
    obrazek character varying(50),
    jmeno character varying(100),
    profil character varying,
    hleda character varying,
    pozaduje character varying,
    nabizi character varying,
    sidlo character varying,
    www character varying(50),
    www2 character varying(50),
    kontakt1 character varying(100),
    kontakt2 character varying(100),
    email1 character varying(50),
    kontakt3 character varying(100),
    kontakt4 character varying(100),
    email2 character varying(50),
    obory integer,
    uzivatel character varying(50),
    heslo character varying(50),
    komentar character varying,
    vyrizuje character varying(50),
    tel character varying(50),
    email character varying(50),
    lc character varying(10),
    "session" bigint,
    zmena timestamp with time zone,
    grafika character(2),
    stav character varying(100),
    edit boolean,
    zmena_kym character varying(50),
    rok integer,
    studentum_nabizi text,
    absolventum_s_praxi_nabizi text
);


ALTER TABLE public.kpp_tmp OWNER TO intranet;

--
-- Name: kpp_varianty; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE kpp_varianty (
    id serial NOT NULL,
    zkratka character varying(10) NOT NULL,
    popis character varying NOT NULL
);


ALTER TABLE public.kpp_varianty OWNER TO intranet;

--
-- Name: TABLE kpp_varianty; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE kpp_varianty IS 'varianty stranek v katalogu';


--
-- Name: lang_levels; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE lang_levels (
    "level" integer NOT NULL,
    name character varying
);


ALTER TABLE public.lang_levels OWNER TO intranet;

--
-- Name: lc_summer_exchange; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE lc_summer_exchange (
    id integer NOT NULL,
    lc character varying(10),
    date_from date,
    date_to date,
    person integer NOT NULL,
    mtime timestamp with time zone,
    muser character varying(20)
);


ALTER TABLE public.lc_summer_exchange OWNER TO intranet;

--
-- Name: lidi_lc; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE lidi_lc (
    id integer NOT NULL,
    jmeno character varying,
    fuknce character varying
);


ALTER TABLE public.lidi_lc OWNER TO intranet;

--
-- Name: TABLE lidi_lc; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE lidi_lc IS 'old-not used';


--
-- Name: mailman_content; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE mailman_content (
    id serial NOT NULL,
    list character varying(100) NOT NULL,
    link character varying(255) NOT NULL,
    subject character varying(255) NOT NULL,
    "from" character varying(255) NOT NULL,
    date timestamp without time zone,
    lc character varying(50) NOT NULL
);


ALTER TABLE public.mailman_content OWNER TO intranet;

--
-- Name: TABLE mailman_content; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE mailman_content IS 'historie mailovych skupin';


--
-- Name: offers; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE offers (
    id integer NOT NULL,
    refno character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phoneno character varying,
    fax character varying,
    e_mail character varying,
    workingplace character varying,
    airport character varying,
    transport character varying,
    employees real,
    workinghoursperweek real,
    dailyworkinghours real,
    faculty character varying,
    specialization character varying,
    studylevel character varying,
    previoustraining boolean,
    english real,
    and1 character varying,
    german real,
    and2 character varying,
    french real,
    other character varying,
    otherrequrements character varying,
    kindofwork character varying,
    category character varying,
    numberweeksmin real,
    numberweeksmax real,
    withinthemonthsfrom real,
    withinthedayfrom real,
    withinthemonthsto real,
    withinthedayto real,
    withinthefrom2 character varying,
    withintheto2 character varying,
    worksclosed character varying,
    traingavailable boolean,
    grosspay character varying,
    currency character varying,
    payper character varying,
    maxdeduction real,
    lodgingarrenged character varying,
    canteenfacilities boolean,
    estimatedlodging character varying,
    estimatedliving character varying,
    deadlinefornomination date,
    date_ date,
    nationality character varying
);


ALTER TABLE public.offers OWNER TO intranet;

--
-- Name: oforms; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms (
    id integer NOT NULL,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    within_the_from2 character varying,
    within_the_to2 character varying,
    worksclosed character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    "year" integer,
    reserved boolean DEFAULT false
);


ALTER TABLE public.oforms OWNER TO intranet;

--
-- Name: oforms2002; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms2002 (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    worksclosed character varying,
    within_the_from2 character varying,
    within_the_to2 character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    pay_per_month_by_dad character varying,
    pay_per_month_by_employer character varying
);


ALTER TABLE public.oforms2002 OWNER TO intranet;

--
-- Name: oforms2003; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms2003 (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    within_the_from2 character varying,
    within_the_to2 character varying,
    worksclosed character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date
);


ALTER TABLE public.oforms2003 OWNER TO intranet;

--
-- Name: oforms_2004; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms_2004 (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    within_the_from2 character varying,
    within_the_to2 character varying,
    worksclosed character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    "year" integer
);


ALTER TABLE public.oforms_2004 OWNER TO intranet;

--
-- Name: oforms_2005_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms_2005_tmp (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    within_the_from2 character varying,
    within_the_to2 character varying,
    worksclosed character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    "year" integer
);


ALTER TABLE public.oforms_2005_tmp OWNER TO intranet;

--
-- Name: oforms_2006; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms_2006 (
    id integer,
    ref_no character varying,
    company character varying,
    address character varying,
    psc real,
    town character varying,
    bussiness character varying,
    responsible character varying,
    phone_no character varying,
    fax character varying,
    e_mail character varying,
    working_place character varying,
    airport character varying,
    transport character varying,
    employees integer,
    working_hours_per_week real,
    daily_working_hours real,
    faculty character varying,
    specialization character varying,
    study_level character varying,
    previous_training boolean,
    english integer,
    and1 character varying,
    german integer,
    and2 character varying,
    french integer,
    other character varying,
    other_requirements character varying,
    kind_of_work character varying,
    category character varying,
    number_weeks_min integer,
    number_weeks_max integer,
    within_the_months_from integer,
    within_the_day_from integer,
    within_the_months_to integer,
    within_the_day_to integer,
    within_the_from2 character varying,
    within_the_to2 character varying,
    worksclosed character varying,
    traing_available boolean,
    gross_pay character varying,
    currency character varying,
    pay_per character varying,
    max_deduction character varying,
    lodging_arranged_by character varying,
    canteen_facilities boolean,
    estimated_lodging character varying,
    estimated_living character varying,
    deadline_for_nomination date,
    date_ date,
    "year" integer
);


ALTER TABLE public.oforms_2006 OWNER TO intranet;

--
-- Name: oforms_access; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE oforms_access (
    id integer,
    hits integer
);


ALTER TABLE public.oforms_access OWNER TO intranet;

--
-- Name: postpraxe; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE postpraxe (
    id integer NOT NULL,
    ref_no character varying(20),
    faculty character varying,
    specialization character varying,
    enter_date date,
    filename character varying(40),
    document boolean,
    abb boolean,
    deadline date
);


ALTER TABLE public.postpraxe OWNER TO intranet;

--
-- Name: praxe_konkurs; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE praxe_konkurs (
    id integer NOT NULL,
    lc character varying(10) NOT NULL,
    datum character varying(20),
    cas character varying(20),
    misto character varying(20),
    url character varying(30),
    poznamka character varying
);


ALTER TABLE public.praxe_konkurs OWNER TO intranet;

--
-- Name: TABLE praxe_konkurs; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE praxe_konkurs IS 'old-not used';


--
-- Name: rok; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE rok (
    rok integer NOT NULL
);


ALTER TABLE public.rok OWNER TO intranet;

--
-- Name: s_banner; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_banner
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_banner OWNER TO intranet;

--
-- Name: s_board; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_board
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_board OWNER TO intranet;

--
-- Name: s_ci_faktura; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura OWNER TO intranet;

--
-- Name: s_ci_faktura_lang; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura_lang
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura_lang OWNER TO intranet;

--
-- Name: s_ci_faktura_polozka; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura_polozka
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura_polozka OWNER TO intranet;

--
-- Name: s_ci_faktura_seznam; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura_seznam
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura_seznam OWNER TO intranet;

--
-- Name: s_ci_faktura_text; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura_text
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura_text OWNER TO intranet;

--
-- Name: s_ci_faktura_ucet; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_faktura_ucet
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_faktura_ucet OWNER TO intranet;

--
-- Name: s_ci_firma; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_firma
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_firma OWNER TO intranet;

--
-- Name: s_ci_firma_historie; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_firma_historie
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_firma_historie OWNER TO intranet;

--
-- Name: s_ci_hledani; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_hledani
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_hledani OWNER TO intranet;

--
-- Name: s_ci_kontakt; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_kontakt
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_kontakt OWNER TO intranet;

--
-- Name: s_ci_projekt; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_projekt
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_projekt OWNER TO intranet;

--
-- Name: s_ci_schuzka; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_schuzka
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_schuzka OWNER TO intranet;

--
-- Name: s_ci_spoluprace; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_spoluprace
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_spoluprace OWNER TO intranet;

--
-- Name: s_ci_ukol; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_ci_ukol
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_ci_ukol OWNER TO intranet;

--
-- Name: s_display_banner; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_display_banner
    INCREMENT BY 1
    MAXVALUE 8
    NO MINVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.s_display_banner OWNER TO intranet;

--
-- Name: s_download; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_download
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_download OWNER TO intranet;

--
-- Name: s_exch_konkurz_tmp; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_konkurz_tmp
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_konkurz_tmp OWNER TO intranet;

--
-- Name: s_exch_participant; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_participant
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_participant OWNER TO intranet;

--
-- Name: s_exch_prefered_country; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_prefered_country
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_prefered_country OWNER TO intranet;

--
-- Name: s_exch_report_cz; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_report_cz
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_report_cz OWNER TO intranet;

--
-- Name: s_exch_student_in; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_student_in
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_student_in OWNER TO intranet;

--
-- Name: s_exch_student_n5b; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_exch_student_n5b
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_exch_student_n5b OWNER TO intranet;

--
-- Name: s_katalog; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_katalog
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_katalog OWNER TO intranet;

--
-- Name: s_konkurs; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_konkurs
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_konkurs OWNER TO intranet;

--
-- Name: s_kpp2002; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_kpp2002
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_kpp2002 OWNER TO intranet;

--
-- Name: s_kpp2003_anketa; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_kpp2003_anketa
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_kpp2003_anketa OWNER TO intranet;

--
-- Name: s_kpp2003_anketa_firma; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_kpp2003_anketa_firma
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_kpp2003_anketa_firma OWNER TO intranet;

--
-- Name: s_lc_summer_exchange; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_lc_summer_exchange
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_lc_summer_exchange OWNER TO intranet;

--
-- Name: s_postpraxe; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_postpraxe
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_postpraxe OWNER TO intranet;

--
-- Name: s_sendmail_list; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_sendmail_list
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_sendmail_list OWNER TO intranet;

--
-- Name: s_sendmail_list_subscriber; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_sendmail_list_subscriber
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_sendmail_list_subscriber OWNER TO intranet;

--
-- Name: s_sendmail_log; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_sendmail_log
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_sendmail_log OWNER TO intranet;

--
-- Name: s_sendmail_temp_list_subscriber; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_sendmail_temp_list_subscriber
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_sendmail_temp_list_subscriber OWNER TO intranet;

--
-- Name: s_share; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_share
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_share OWNER TO intranet;

--
-- Name: s_states_lang; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_states_lang
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_states_lang OWNER TO intranet;

--
-- Name: s_states_lang_old; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_states_lang_old
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_states_lang_old OWNER TO intranet;

--
-- Name: s_student; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_student
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_student OWNER TO intranet;

--
-- Name: s_trainees; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_trainees
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_trainees OWNER TO intranet;

--
-- Name: s_trainees_events; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_trainees_events
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_trainees_events OWNER TO intranet;

--
-- Name: s_users; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_users
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_users OWNER TO intranet;

--
-- Name: s_vpp2002; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_vpp2002
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_vpp2002 OWNER TO intranet;

--
-- Name: s_vpp_pozice; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE s_vpp_pozice
    INCREMENT BY 1
    MAXVALUE 2147483647
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.s_vpp_pozice OWNER TO intranet;

--
-- Name: sendmail_list_subscriber; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE sendmail_list_subscriber (
    id integer DEFAULT nextval(('s_sendmail_list_subscriber'::text)::regclass) NOT NULL,
    list integer NOT NULL,
    fullname character varying(100),
    email character varying(100) NOT NULL,
    since timestamp with time zone,
    ip character varying(50),
    subscribed_by character varying(20),
    owner_id integer
);


ALTER TABLE public.sendmail_list_subscriber OWNER TO intranet;

--
-- Name: sendmail_log_content; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE sendmail_log_content (
    id bigint DEFAULT nextval(('s_sendmail_log'::text)::regclass) NOT NULL,
    mailfrom character varying(100),
    rcpt character varying(100),
    subject character varying(100),
    content character varying,
    date timestamp with time zone,
    "time" integer,
    count integer,
    size character varying(10)
);


ALTER TABLE public.sendmail_log_content OWNER TO intranet;

--
-- Name: sendmail_log_rcpt; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE sendmail_log_rcpt (
    rcpt_email character varying(100),
    content bigint
);


ALTER TABLE public.sendmail_log_rcpt OWNER TO intranet;

--
-- Name: sendmail_test_list_subscriber; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE sendmail_test_list_subscriber (
    id integer,
    list integer,
    fullname character varying(100),
    email character varying(100),
    since timestamp with time zone,
    ip character varying(50),
    subscribed_by character varying(20),
    owner_id integer
);


ALTER TABLE public.sendmail_test_list_subscriber OWNER TO intranet;

--
-- Name: share; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE "share" (
    id integer NOT NULL,
    directory character varying(50),
    "comment" character varying,
    "access" character varying,
    search_index character varying
);


ALTER TABLE public."share" OWNER TO intranet;

--
-- Name: share_access; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE share_access (
    id integer DEFAULT nextval(('"share_access_id_seq"'::text)::regclass) NOT NULL,
    "share" integer NOT NULL,
    "user" integer,
    "group" character varying(30),
    "right" character varying(1)
);


ALTER TABLE public.share_access OWNER TO intranet;

--
-- Name: TABLE share_access; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE share_access IS 'users-rights matching to share dir access';


--
-- Name: share_access_id_seq; Type: SEQUENCE; Schema: public; Owner: intranet
--

CREATE SEQUENCE share_access_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.share_access_id_seq OWNER TO intranet;

--
-- Name: share_group_access; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW share_group_access AS
    SELECT DISTINCT s.directory, u.username, u.id AS user_id, s.id AS share_id FROM ((share_access sa LEFT JOIN "share" s ON ((sa."share" = s.id))) LEFT JOIN users u ON ((((u.id = sa."user") OR ((u.groupname)::text = (sa."group")::text)) OR (u.rights ~~ ((('%'::character varying)::text || (sa."right")::text) || ('%'::character varying)::text))))) WHERE (u.password_hash IS NOT NULL) ORDER BY s.directory, u.username, u.id, s.id;


ALTER TABLE public.share_group_access OWNER TO intranet;

--
-- Name: VIEW share_group_access; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW share_group_access IS 'nepouzivano... 
nahled pro apache+mod_auth_pg pro kontrolu pristupu na share';


--
-- Name: share_group_access_old; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW share_group_access_old AS
    SELECT s.directory, u.username FROM "share" s, users u WHERE ((((s."access")::text ~~ (('% '::text || (u.username)::text) || '%'::text)) OR ((s."access")::text ~~ (('% '::text || (u.groupname)::text) || '%'::text))) OR (u.rights ~~ (('%'::text || (s."access")::text) || '%'::text)));


ALTER TABLE public.share_group_access_old OWNER TO intranet;

--
-- Name: share_rights; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE share_rights (
    id serial NOT NULL,
    "share" integer NOT NULL,
    ro_user integer,
    rw_user integer,
    ro_groupname character varying(30),
    rw_groupname character varying(30),
    ro_right character varying(1),
    rw_right character varying(1),
    m_uid integer NOT NULL,
    m_time timestamp without time zone NOT NULL
);


ALTER TABLE public.share_rights OWNER TO intranet;

--
-- Name: TABLE share_rights; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE share_rights IS 'prava k adresarum sharedisku';


--
-- Name: COLUMN share_rights."share"; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights."share" IS 'share.id';


--
-- Name: COLUMN share_rights.ro_user; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.ro_user IS 'user.id';


--
-- Name: COLUMN share_rights.rw_user; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.rw_user IS 'user.id';


--
-- Name: COLUMN share_rights.ro_groupname; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.ro_groupname IS 'user.groupname';


--
-- Name: COLUMN share_rights.rw_groupname; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.rw_groupname IS 'user.groupname';


--
-- Name: COLUMN share_rights.ro_right; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.ro_right IS '_groups.id';


--
-- Name: COLUMN share_rights.rw_right; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.rw_right IS '_groups.id';


--
-- Name: COLUMN share_rights.m_uid; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN share_rights.m_uid IS 'user.id posledni modifikace';


--
-- Name: share_rights_users; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW share_rights_users AS
    ((((SELECT DISTINCT 'RO'::text AS "access", s.directory, u.username, u.id AS user_id, s.id AS share_id FROM ((share_rights sr LEFT JOIN "share" s ON ((sr."share" = s.id))) LEFT JOIN users u ON ((((u.id = sr.ro_user) OR ((u.groupname)::text = (sr.ro_groupname)::text)) OR (u.rights ~~ ((('%'::character varying)::text || (sr.ro_right)::text) || ('%'::character varying)::text))))) WHERE (u.password_hash IS NOT NULL) ORDER BY 'RO'::text, s.directory, u.username, u.id, s.id) UNION (SELECT DISTINCT 'RW'::text AS "access", s.directory, u.username, u.id AS user_id, s.id AS share_id FROM ((share_rights sr LEFT JOIN "share" s ON ((sr."share" = s.id))) LEFT JOIN users u ON ((((u.id = sr.rw_user) OR ((u.groupname)::text = (sr.rw_groupname)::text)) OR (u.rights ~~ ((('%'::character varying)::text || (sr.rw_right)::text) || ('%'::character varying)::text))))) WHERE (u.password_hash IS NOT NULL) ORDER BY 'RW'::text, s.directory, u.username, u.id, s.id)) UNION (SELECT DISTINCT 'RO'::text AS "access", 'archiv'::text AS directory, u.username, u.id AS user_id, 12 AS share_id FROM users u WHERE (u.password_hash IS NOT NULL) ORDER BY 'RO'::text, 'archiv'::text, u.username, u.id, 12::integer)) UNION (SELECT DISTINCT 'RO'::text AS "access", 'projekty'::text AS directory, u.username, u.id AS user_id, 62 AS share_id FROM users u WHERE (u.password_hash IS NOT NULL) ORDER BY 'RO'::text, 'projekty'::text, u.username, u.id, 62::integer)) UNION (SELECT DISTINCT 'RW'::text AS "access", 'public'::text AS directory, u.username, u.id AS user_id, 2 AS share_id FROM users u WHERE (u.password_hash IS NOT NULL) ORDER BY 'RW'::text, 'public'::text, u.username, u.id, 2::integer) ORDER BY 1, 2, 3;


ALTER TABLE public.share_rights_users OWNER TO intranet;

--
-- Name: VIEW share_rights_users; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW share_rights_users IS 'uzivatele s pristupem k adresarum sharedisku. view vcetne hardcoded prav k vybranym adresarum';


--
-- Name: share_rights_users2ldap; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW share_rights_users2ldap AS
    (SELECT DISTINCT 'RO'::text AS "access", s.directory, u.udb_username AS username, u.id AS user_id, s.id AS share_id FROM ((share_rights sr LEFT JOIN "share" s ON ((sr."share" = s.id))) LEFT JOIN users u ON (((u.id = sr.ro_user) OR (u.rights ~~ ((('%'::character varying)::text || (sr.ro_right)::text) || ('%'::character varying)::text))))) WHERE (u.password_hash IS NOT NULL) ORDER BY 'RO'::text, s.directory, u.udb_username, u.id, s.id) UNION (SELECT DISTINCT 'RW'::text AS "access", s.directory, u.udb_username AS username, u.id AS user_id, s.id AS share_id FROM ((share_rights sr LEFT JOIN "share" s ON ((sr."share" = s.id))) LEFT JOIN users u ON (((u.id = sr.rw_user) OR (u.rights ~~ ((('%'::character varying)::text || (sr.rw_right)::text) || ('%'::character varying)::text))))) WHERE (u.password_hash IS NOT NULL) ORDER BY 'RW'::text, s.directory, u.udb_username, u.id, s.id) ORDER BY 1, 2, 3;


ALTER TABLE public.share_rights_users2ldap OWNER TO intranet;

--
-- Name: VIEW share_rights_users2ldap; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW share_rights_users2ldap IS 'kopie share_rights_users, vynechava parovani LC';


--
-- Name: st_lang_levels; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE st_lang_levels (
    id smallint NOT NULL,
    "level" character varying(15)
);


ALTER TABLE public.st_lang_levels OWNER TO intranet;

--
-- Name: st_study_types; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE st_study_types (
    id smallint NOT NULL,
    "type" character varying(30),
    short_type character varying(50)
);


ALTER TABLE public.st_study_types OWNER TO intranet;

--
-- Name: states; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE states (
    state integer NOT NULL,
    query character varying,
    id character varying,
    name character varying,
    active character varying,
    "next" integer,
    style character varying,
    "before" integer,
    "after" integer,
    toolbar smallint,
    rights character(50),
    insert_ integer,
    update_ integer,
    delete_ integer,
    orderby character varying,
    pagestyle character varying,
    "comment" character varying,
    update_admin_rights character(50),
    delete_admin_rights character(50),
    "content-type" character varying,
    count_query character varying,
    single_query character varying,
    set_limit integer,
    mtime timestamp without time zone,
    muser integer
);


ALTER TABLE public.states OWNER TO intranet;

--
-- Name: COLUMN states.muser; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN states.muser IS 'users.id, bez FK';


--
-- Name: states_backup; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE states_backup (
    state integer,
    query character varying,
    id character varying,
    name character varying,
    active character varying,
    "next" integer,
    style character varying,
    "before" integer,
    "after" integer,
    toolbar smallint,
    rights character(50),
    insert_ integer,
    update_ integer,
    delete_ integer,
    orderby character varying,
    pagestyle character varying,
    "comment" character varying,
    update_admin_rights character(50),
    delete_admin_rights character(50),
    setl character varying,
    count_query character varying,
    single_query character varying,
    set_limit integer
);


ALTER TABLE public.states_backup OWNER TO intranet;

--
-- Name: states_lang; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE states_lang (
    id integer NOT NULL,
    locale character varying(5) NOT NULL,
    pagestyle character varying,
    style character varying,
    state integer,
    "key" character varying NOT NULL,
    value character varying,
    "type" character varying,
    "comment" character varying
);


ALTER TABLE public.states_lang OWNER TO intranet;

--
-- Name: states_lang_old; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE states_lang_old (
    id integer NOT NULL,
    locale character varying(5) NOT NULL,
    state integer,
    style character varying,
    "key" character varying NOT NULL,
    value character varying,
    "type" character varying,
    "comment" character varying
);


ALTER TABLE public.states_lang_old OWNER TO intranet;

--
-- Name: TABLE states_lang_old; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE states_lang_old IS 'localized texts for states';


--
-- Name: states_old; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE states_old (
    state integer NOT NULL,
    query character varying,
    id character varying,
    name character varying,
    active character varying,
    "next" integer,
    style character varying,
    "before" integer,
    "after" integer,
    toolbar smallint,
    rights character(30),
    insert_ integer,
    update_ integer,
    delete_ integer,
    orderby character varying,
    pagestyle character varying,
    "comment" character varying,
    update_admin_rights character(30),
    delete_admin_rights character(30),
    setl character varying,
    count_query character varying,
    single_query character varying,
    set_limit integer
);


ALTER TABLE public.states_old OWNER TO intranet;

--
-- Name: TABLE states_old; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE states_old IS 's kratkym groups';


--
-- Name: student; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE student (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    email character varying,
    address_street character varying,
    address_city character varying,
    address_zip character(6),
    phone character varying,
    university smallint,
    faculty character varying,
    specialisation character varying,
    study_type smallint,
    year_to_finish smallint,
    english smallint,
    german smallint,
    french smallint,
    spanish smallint,
    other_langs character varying,
    training character varying,
    knowledges character varying,
    info_jobs boolean,
    info_training boolean,
    enter_day date,
    valid_email boolean,
    valid_entry boolean,
    russian smallint,
    polish smallint
);


ALTER TABLE public.student OWNER TO intranet;

--
-- Name: test; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE test (
    xx integer NOT NULL,
    xxx character(10),
    text character varying,
    pole integer[]
);


ALTER TABLE public.test OWNER TO intranet;

--
-- Name: test_calendar; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE test_calendar (
    id integer NOT NULL,
    days integer,
    name character varying
);


ALTER TABLE public.test_calendar OWNER TO intranet;

--
-- Name: tmp_firm; Type: TABLE; Schema: public; Owner: alfi; Tablespace: 
--

CREATE TABLE tmp_firm (
    id serial NOT NULL,
    name character varying,
    id_intranet integer
);


ALTER TABLE public.tmp_firm OWNER TO alfi;

--
-- Name: TABLE tmp_firm; Type: COMMENT; Schema: public; Owner: alfi
--

COMMENT ON TABLE tmp_firm IS 'kopie slunicka';


--
-- Name: trainees; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE trainees (
    id integer DEFAULT nextval(('s_trainees'::text)::regclass) NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    email character varying(50),
    sex character varying(10),
    number character varying(50),
    nationality character varying(50),
    country_training character varying(50),
    other character varying,
    entered timestamp with time zone,
    other2 character varying,
    other3 character varying,
    other4 character varying,
    isic character varying(50),
    event character varying(50),
    other5 character varying,
    other6 character varying,
    "comment" character varying,
    other7 character varying
);


ALTER TABLE public.trainees OWNER TO intranet;

--
-- Name: trainees_events; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE trainees_events (
    id integer NOT NULL,
    name character varying(50),
    description character varying,
    lc character varying(50),
    "access" character varying,
    mail_from character varying(100),
    mail_text text,
    mtime timestamp with time zone,
    muser character varying(50),
    show_list boolean,
    mail_subject character varying(100)
);


ALTER TABLE public.trainees_events OWNER TO intranet;

--
-- Name: trainees_tmp; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE trainees_tmp (
    id integer,
    first_name character varying(50),
    last_name character varying(50),
    email character varying(50),
    sex character varying(10),
    number character varying(50),
    nationality character varying(50),
    country_training character varying(50),
    other character varying,
    entered timestamp with time zone,
    other2 character varying,
    other3 character varying,
    other4 character varying,
    isic character varying(50),
    event character varying(50),
    other5 character varying,
    other6 character varying,
    "comment" character varying,
    other7 character varying
);


ALTER TABLE public.trainees_tmp OWNER TO intranet;

--
-- Name: trainees_tmp_radim; Type: TABLE; Schema: public; Owner: alfi; Tablespace: 
--

CREATE TABLE trainees_tmp_radim (
    id integer NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    email character varying(50),
    sex character varying(10),
    number character varying(50),
    nationality character varying(50),
    country_training character varying(50),
    other character varying,
    entered timestamp with time zone,
    other2 character varying,
    other3 character varying,
    other4 character varying,
    isic character varying(50),
    event character varying(50),
    other5 character varying,
    other6 character varying,
    "comment" character varying,
    other7 character varying
);


ALTER TABLE public.trainees_tmp_radim OWNER TO alfi;

SET search_path = udb, pg_catalog;

--
-- Name: conferencies; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE conferencies (
    name character varying(50) NOT NULL,
    info character varying(255),
    lcs_id integer,
    rights_id integer NOT NULL,
    teams_id integer,
    conference_types_id integer NOT NULL,
    id serial NOT NULL
);


ALTER TABLE udb.conferencies OWNER TO udbmaster;

SET search_path = public, pg_catalog;

--
-- Name: udb_conferencies; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW udb_conferencies AS
    SELECT conferencies.name, conferencies.info, conferencies.lcs_id, conferencies.rights_id, conferencies.teams_id, conferencies.conference_types_id, conferencies.id, CASE WHEN ("position"((conferencies.name)::text, '.'::text) > 0) THEN "substring"((conferencies.name)::text, ("position"((conferencies.name)::text, '.'::text) + 1)) ELSE 'nc'::text END AS lc FROM udb.conferencies WHERE (conferencies.conference_types_id = 1) ORDER BY conferencies.name;


ALTER TABLE public.udb_conferencies OWNER TO intranet;

--
-- Name: VIEW udb_conferencies; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW udb_conferencies IS 'konference v udb';


SET search_path = udb, pg_catalog;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: udb; Owner: udbmaster
--

CREATE SEQUENCE users_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE udb.users_id_seq OWNER TO udbmaster;

--
-- Name: users; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE users (
    id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
    username character varying(20) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    iaeste_mail character varying(50),
    personal_mail character varying(50),
    telephone character varying(20),
    "password" character varying(100),
    additional_contacts character varying(255),
    lcs_id integer NOT NULL,
    teams_id integer,
    rights_id integer NOT NULL,
    info character varying(255),
    info_hr character varying(255),
    ci_contact integer
);


ALTER TABLE udb.users OWNER TO udbmaster;

SET search_path = public, pg_catalog;

--
-- Name: udb_users; Type: VIEW; Schema: public; Owner: intranet
--

CREATE VIEW udb_users AS
    SELECT users.id AS udb_id, lc.username AS udb_lc, users.username AS udb_username, users.first_name, users.last_name, users.iaeste_mail, users.personal_mail, (users.rights_id = 10) AS udb_disabled, u.groupname AS lc, u.username, u.fullname, u.email, (u.password_hash IS NULL) AS disabled, u.ci_kontakt FROM ((udb.users LEFT JOIN lc ON ((users.lcs_id = lc.id))) FULL JOIN users u ON ((u.udb_id = users.id)));


ALTER TABLE public.udb_users OWNER TO intranet;

--
-- Name: VIEW udb_users; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON VIEW udb_users IS 'parovani uzivatelu s udb';


--
-- Name: university; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE university (
    id smallint NOT NULL,
    name character varying,
    short character varying(10)
);


ALTER TABLE public.university OWNER TO intranet;

--
-- Name: user_preferences; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE user_preferences (
    id serial NOT NULL,
    name character varying(50) NOT NULL,
    value character varying(255),
    uid integer NOT NULL
);


ALTER TABLE public.user_preferences OWNER TO intranet;

--
-- Name: TABLE user_preferences; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON TABLE user_preferences IS 'volby ulozene uzivatelem';


--
-- Name: COLUMN user_preferences.uid; Type: COMMENT; Schema: public; Owner: intranet
--

COMMENT ON COLUMN user_preferences.uid IS 'user id';


--
-- Name: users_bak; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE users_bak (
    username character varying(50),
    "password" character varying(30),
    rights character(50),
    id integer,
    groupname character varying(30),
    lastlogin timestamp with time zone,
    fullname character varying(50),
    email character varying(50),
    password_hash character varying(32),
    m_time timestamp without time zone,
    m_uid integer,
    ci_kontakt integer,
    udb_username character varying(20),
    udb_id integer
);


ALTER TABLE public.users_bak OWNER TO intranet;

--
-- Name: vpp2002; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE vpp2002 (
    id bigint DEFAULT nextval(('s_vpp2002'::text)::regclass) NOT NULL,
    ci integer,
    logo character varying(50),
    jmeno character varying(80),
    sidlo character varying,
    www character varying(50),
    www2 character varying(50),
    kontakt character varying,
    email_kontakt character varying(50),
    pobocky character varying,
    profil character varying,
    hleda character varying,
    nabizi character varying,
    obory integer,
    studentum integer,
    student5 character varying(50),
    vpp integer,
    komentar character varying,
    vyrizuje character varying(50),
    tel character varying(50),
    email character varying(50),
    lc character varying(10),
    "session" bigint,
    zmena timestamp with time zone,
    stav character varying(100),
    edit boolean,
    nazev_stanek character varying(50),
    person1 character varying(50),
    person2 character varying(50),
    person3 character varying(50),
    person4 character varying(50),
    person5 character varying(50),
    person6 character varying(50),
    pocet_lidi character varying(50),
    obed boolean,
    raut boolean,
    tiskovka boolean
);


ALTER TABLE public.vpp2002 OWNER TO intranet;

--
-- Name: vpp_pozice; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE vpp_pozice (
    id bigint DEFAULT nextval(('s_vpp_pozice'::text)::regclass) NOT NULL,
    nazev character varying(100),
    pozadavky character varying,
    firma bigint NOT NULL
);


ALTER TABLE public.vpp_pozice OWNER TO intranet;

--
-- Name: www; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE www (
    id integer NOT NULL,
    name character varying,
    "comment" character varying,
    url character varying,
    group_ character varying
);


ALTER TABLE public.www OWNER TO intranet;

--
-- Name: years; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE years (
    id smallint NOT NULL
);


ALTER TABLE public.years OWNER TO intranet;

--
-- Name: yesno; Type: TABLE; Schema: public; Owner: intranet; Tablespace: 
--

CREATE TABLE yesno (
    tf boolean,
    yn character varying(5),
    an character(4),
    ano character varying(10),
    a character(1)
);


ALTER TABLE public.yesno OWNER TO intranet;

SET search_path = regsys, pg_catalog;

--
-- Name: events; Type: TABLE; Schema: regsys; Owner: rikard0; Tablespace: 
--

CREATE TABLE events (
    id serial NOT NULL,
    title character varying(50) NOT NULL,
    info character varying(200),
    "actions-id" integer,
    more_term boolean DEFAULT false NOT NULL,
    max_reg integer DEFAULT 0 NOT NULL
);


ALTER TABLE regsys.events OWNER TO rikard0;

--
-- Name: COLUMN events.id; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN events.id IS 'Event ID';


--
-- Name: COLUMN events.title; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN events.title IS 'Title for event';


--
-- Name: COLUMN events.info; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN events.info IS 'Info to event';


--
-- Name: COLUMN events.more_term; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN events.more_term IS 'Can user check more terms or only one (TRUE can, FALSE can''t)';


--
-- Name: COLUMN events.max_reg; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN events.max_reg IS 'Haw many people may register to event';


--
-- Name: actions; Type: TABLE; Schema: regsys; Owner: rikard0; Tablespace: 
--

CREATE TABLE actions (
    id serial NOT NULL,
    name character varying(50) NOT NULL,
    info character varying(200),
    max_reg integer DEFAULT 0 NOT NULL
);


ALTER TABLE regsys.actions OWNER TO rikard0;

--
-- Name: COLUMN actions.id; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN actions.id IS 'Action ID';


--
-- Name: COLUMN actions.name; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN actions.name IS 'Name of action';


--
-- Name: COLUMN actions.info; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN actions.info IS 'Info to action';


--
-- Name: COLUMN actions.max_reg; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN actions.max_reg IS 'Haw many people may register to event ';


--
-- Name: regs_action; Type: TABLE; Schema: regsys; Owner: rikard0; Tablespace: 
--

CREATE TABLE regs_action (
    id serial NOT NULL,
    "actions-id" integer NOT NULL,
    "user" character varying NOT NULL,
    info serial NOT NULL
);


ALTER TABLE regsys.regs_action OWNER TO rikard0;

--
-- Name: COLUMN regs_action.id; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_action.id IS 'Registration ID';


--
-- Name: COLUMN regs_action."actions-id"; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_action."actions-id" IS 'Registration to Action';


--
-- Name: COLUMN regs_action."user"; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_action."user" IS 'Username from intranet (public) schema. Name, e-mail, t.c from intranet';


--
-- Name: COLUMN regs_action.info; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_action.info IS 'Info to registration';


--
-- Name: regs_event; Type: TABLE; Schema: regsys; Owner: rikard0; Tablespace: 
--

CREATE TABLE regs_event (
    id serial NOT NULL,
    "events-id" integer NOT NULL,
    "user" serial NOT NULL,
    info serial NOT NULL
);


ALTER TABLE regsys.regs_event OWNER TO rikard0;

--
-- Name: COLUMN regs_event.id; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_event.id IS 'Registration ID';


--
-- Name: COLUMN regs_event."events-id"; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_event."events-id" IS 'Registration to Event';


--
-- Name: COLUMN regs_event."user"; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_event."user" IS 'Username from intranet (public) schema. Name, e-mail, t.c from intranet';


--
-- Name: COLUMN regs_event.info; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN regs_event.info IS 'Info to registration';


--
-- Name: terms; Type: TABLE; Schema: regsys; Owner: rikard0; Tablespace: 
--

CREATE TABLE terms (
    id serial NOT NULL,
    title character varying(50) NOT NULL,
    start_date date NOT NULL,
    start_time serial NOT NULL,
    end_date date NOT NULL,
    end_time serial NOT NULL,
    info character varying(200),
    "events-id" integer NOT NULL
);


ALTER TABLE regsys.terms OWNER TO rikard0;

--
-- Name: COLUMN terms.id; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.id IS 'Term ID';


--
-- Name: COLUMN terms.title; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.title IS 'Term title';


--
-- Name: COLUMN terms.start_date; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.start_date IS 'Date when term start';


--
-- Name: COLUMN terms.start_time; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.start_time IS 'Time when term start';


--
-- Name: COLUMN terms.end_date; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.end_date IS 'Date when term end';


--
-- Name: COLUMN terms.end_time; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.end_time IS 'Time when term end';


--
-- Name: COLUMN terms.info; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms.info IS 'Info of Term';


--
-- Name: COLUMN terms."events-id"; Type: COMMENT; Schema: regsys; Owner: rikard0
--

COMMENT ON COLUMN terms."events-id" IS 'This term is fo Action with ID ...';


SET search_path = udb, pg_catalog;

--
-- Name: conference_types; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE conference_types (
    id serial NOT NULL,
    conference_type character varying(20),
    info character varying(100)
);


ALTER TABLE udb.conference_types OWNER TO udbmaster;

--
-- Name: conferencies_mail; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE conferencies_mail (
    conferencies_id integer NOT NULL,
    member_mail character varying(100) NOT NULL
);


ALTER TABLE udb.conferencies_mail OWNER TO udbmaster;

--
-- Name: conferencies_users; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE conferencies_users (
    conferencies_id integer NOT NULL,
    users_id integer
);


ALTER TABLE udb.conferencies_users OWNER TO udbmaster;

--
-- Name: history_id_seq; Type: SEQUENCE; Schema: udb; Owner: udbmaster
--

CREATE SEQUENCE history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE udb.history_id_seq OWNER TO udbmaster;

--
-- Name: history; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE history (
    id integer DEFAULT nextval('history_id_seq'::regclass) NOT NULL,
    "type" integer NOT NULL,
    table_id integer NOT NULL,
    column_id integer NOT NULL,
    "time" timestamp without time zone[] NOT NULL,
    who character varying(50) NOT NULL,
    before_change character varying(100) NOT NULL,
    after_change character varying(100) NOT NULL
);


ALTER TABLE udb.history OWNER TO udbmaster;

--
-- Name: lcs_id_seq; Type: SEQUENCE; Schema: udb; Owner: udbmaster
--

CREATE SEQUENCE lcs_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE udb.lcs_id_seq OWNER TO udbmaster;

--
-- Name: lcs; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE lcs (
    id integer DEFAULT nextval('lcs_id_seq'::regclass) NOT NULL,
    code character varying(10) NOT NULL,
    name character varying(100),
    address character varying(100),
    telephone character varying(20),
    fax character varying(20),
    ico character varying(20),
    dic character varying(20)
);


ALTER TABLE udb.lcs OWNER TO udbmaster;

--
-- Name: rights_id_seq; Type: SEQUENCE; Schema: udb; Owner: udbmaster
--

CREATE SEQUENCE rights_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE udb.rights_id_seq OWNER TO udbmaster;

--
-- Name: rights; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE rights (
    id integer DEFAULT nextval('rights_id_seq'::regclass) NOT NULL,
    info character varying(255) NOT NULL,
    right_name character varying(20) NOT NULL,
    priority integer NOT NULL
);


ALTER TABLE udb.rights OWNER TO udbmaster;

--
-- Name: services; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE services (
    share_read boolean DEFAULT false,
    share_write boolean DEFAULT false,
    wiki boolean DEFAULT false,
    mail boolean DEFAULT false,
    users_id integer NOT NULL,
    intranet character varying(40),
    slunicko_admin boolean DEFAULT false NOT NULL,
    mrak character varying DEFAULT 'user'::character varying NOT NULL,
    CONSTRAINT "Services_check_mrak" CHECK (((((mrak)::text = 'user'::text) OR ((mrak)::text = 'admin'::text)) OR ((mrak)::text = 'leader'::text)))
);


ALTER TABLE udb.services OWNER TO udbmaster;

--
-- Name: services_view; Type: VIEW; Schema: udb; Owner: rikard0
--

CREATE VIEW services_view AS
    SELECT services.share_read, services.share_write, services.wiki, services.mail, services.users_id, services.intranet, services.slunicko_admin, users.username FROM (services JOIN users ON ((services.users_id = users.id)));


ALTER TABLE udb.services_view OWNER TO rikard0;

--
-- Name: slunicko_companies_view; Type: VIEW; Schema: udb; Owner: rikard0
--

CREATE VIEW slunicko_companies_view AS
    SELECT ci_firma.id AS ci_id, ci_firma.nazev AS ci_name, ci_firma.ico AS ci_ico, ci_firma.lc_reg AS ci_lc, ci_firma.ulice AS ci_street, ci_firma.mesto AS ci_town, ci_firma.psc AS ci_psc FROM public.ci_firma;


ALTER TABLE udb.slunicko_companies_view OWNER TO rikard0;

--
-- Name: slunicko_view; Type: VIEW; Schema: udb; Owner: rikard0
--

CREATE VIEW slunicko_view AS
    SELECT users.id AS users_id, users.username, users.first_name, users.last_name, users.iaeste_mail, users.personal_mail, users.lcs_id, lcs.code AS lcs_name, services.slunicko_admin, services.mrak, rights.id AS rights_id, rights.right_name, rights.priority AS rights_priority FROM (((users JOIN lcs ON ((users.lcs_id = lcs.id))) JOIN services ON ((users.id = services.users_id))) JOIN rights ON ((users.rights_id = rights.id))) WHERE (rights.id <> 10);


ALTER TABLE udb.slunicko_view OWNER TO rikard0;

--
-- Name: teams_id_seq; Type: SEQUENCE; Schema: udb; Owner: udbmaster
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE udb.teams_id_seq OWNER TO udbmaster;

--
-- Name: teams; Type: TABLE; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE TABLE teams (
    id integer DEFAULT nextval('teams_id_seq'::regclass) NOT NULL,
    name character varying(20) NOT NULL,
    info character varying(255),
    lcs_id integer NOT NULL
);


ALTER TABLE udb.teams OWNER TO udbmaster;

SET search_path = exch, pg_catalog;

--
-- Name: exch_and_or_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_and_or
    ADD CONSTRAINT exch_and_or_pkey PRIMARY KEY (id);


--
-- Name: exch_catalog_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_catalog
    ADD CONSTRAINT exch_catalog_pkey PRIMARY KEY (id);


--
-- Name: exch_company_id_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT exch_company_id_pkey PRIMARY KEY (id);


--
-- Name: exch_company_intraweb_mask_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_company_intraweb_mask
    ADD CONSTRAINT exch_company_intraweb_mask_pkey PRIMARY KEY (ref_no);


--
-- Name: exch_company_intraweb_status_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_company_intraweb_status
    ADD CONSTRAINT exch_company_intraweb_status_pkey PRIMARY KEY (id);


--
-- Name: exch_count_company_year_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_company_count_year
    ADD CONSTRAINT exch_count_company_year_pkey PRIMARY KEY (id);


--
-- Name: exch_country_id-pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_country
    ADD CONSTRAINT "exch_country_id-pkey" UNIQUE (id);


--
-- Name: exch_country_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_country
    ADD CONSTRAINT exch_country_pkey PRIMARY KEY (abbr);


--
-- Name: exch_employers_report_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_employers_report
    ADD CONSTRAINT exch_employers_report_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_country_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam_country
    ADD CONSTRAINT exch_exam_country_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam
    ADD CONSTRAINT exch_exam_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_student_file_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam_student_file
    ADD CONSTRAINT exch_exam_student_file_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_student_file_type_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam_student_file_type
    ADD CONSTRAINT exch_exam_student_file_type_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_student_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT exch_exam_student_pkey PRIMARY KEY (id);


--
-- Name: exch_exam_university_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_exam_university
    ADD CONSTRAINT exch_exam_university_pkey PRIMARY KEY (id);


--
-- Name: exch_faculty_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_faculty
    ADD CONSTRAINT exch_faculty_pkey PRIMARY KEY (id);


--
-- Name: exch_language_knowledge_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_language_knowledge
    ADD CONSTRAINT exch_language_knowledge_pkey PRIMARY KEY (id);


--
-- Name: exch_language_level_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_language_level
    ADD CONSTRAINT exch_language_level_pkey PRIMARY KEY (id);


--
-- Name: exch_language_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_language
    ADD CONSTRAINT exch_language_pkey PRIMARY KEY (id);


--
-- Name: exch_month_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_month
    ADD CONSTRAINT exch_month_pkey PRIMARY KEY (id);


--
-- Name: exch_not_prefered_country_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_not_prefered_country
    ADD CONSTRAINT exch_not_prefered_country_pkey PRIMARY KEY (id);


--
-- Name: exch_postpraxe_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_postpraxe
    ADD CONSTRAINT exch_postpraxe_pkey PRIMARY KEY (id);


--
-- Name: exch_prefered_country_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_prefered_country
    ADD CONSTRAINT exch_prefered_country_pkey PRIMARY KEY (id);


--
-- Name: exch_prefered_month_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_prefered_month
    ADD CONSTRAINT exch_prefered_month_pkey PRIMARY KEY (id);


--
-- Name: exch_prefered_study_section_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_prefered_faculty
    ADD CONSTRAINT exch_prefered_study_section_pkey PRIMARY KEY (id);


--
-- Name: exch_public_text_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_public_text
    ADD CONSTRAINT exch_public_text_pkey PRIMARY KEY (id);


--
-- Name: exch_request_state_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_request_state
    ADD CONSTRAINT exch_request_state_pkey PRIMARY KEY (id);


--
-- Name: exch_residency_length_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_residency_length
    ADD CONSTRAINT exch_residency_length_pkey PRIMARY KEY (id);


--
-- Name: exch_student_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_student_pkey PRIMARY KEY (id);


--
-- Name: exch_study_level_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_study_level
    ADD CONSTRAINT exch_study_level_pkey PRIMARY KEY (id);


--
-- Name: exch_study_program_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_study_program
    ADD CONSTRAINT exch_study_program_pkey PRIMARY KEY (id);


--
-- Name: exch_title_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_title
    ADD CONSTRAINT exch_title_pkey PRIMARY KEY (id);


--
-- Name: exch_training_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT exch_training_pkey PRIMARY KEY (id);


--
-- Name: exch_training_state_id; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_training_state
    ADD CONSTRAINT exch_training_state_id PRIMARY KEY (id);


--
-- Name: exch_translate_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_translate
    ADD CONSTRAINT exch_translate_pkey PRIMARY KEY (id);


--
-- Name: exch_work_category_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_work_category
    ADD CONSTRAINT exch_work_category_pkey PRIMARY KEY (id);


--
-- Name: exch_year_class_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_year_class
    ADD CONSTRAINT exch_year_class_pkey PRIMARY KEY (id);


--
-- Name: exch_yes_no_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_yes_no
    ADD CONSTRAINT exch_yes_no_pkey PRIMARY KEY (id);


--
-- Name: pkey_exch_request; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_request
    ADD CONSTRAINT pkey_exch_request PRIMARY KEY (id);


--
-- Name: training_state_cancel_pkey; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_training_state_cancel
    ADD CONSTRAINT training_state_cancel_pkey PRIMARY KEY (id);


--
-- Name: work_category_abbr_key; Type: CONSTRAINT; Schema: exch; Owner: exchange; Tablespace: 
--

ALTER TABLE ONLY exch_work_category
    ADD CONSTRAINT work_category_abbr_key UNIQUE (abbr);


SET search_path = public, pg_catalog;

--
-- Name: _groups_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY _groups
    ADD CONSTRAINT _groups_pkey PRIMARY KEY (id);


--
-- Name: board_name_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY board_name
    ADD CONSTRAINT board_name_pkey PRIMARY KEY (id);


--
-- Name: board_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY board
    ADD CONSTRAINT board_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura
    ADD CONSTRAINT ci_faktura_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_polozka_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_polozka
    ADD CONSTRAINT ci_faktura_polozka_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_seznam_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_seznam
    ADD CONSTRAINT ci_faktura_seznam_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_stav_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_stav
    ADD CONSTRAINT ci_faktura_stav_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_text_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_text
    ADD CONSTRAINT ci_faktura_text_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_typ_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_typ
    ADD CONSTRAINT ci_faktura_typ_pkey PRIMARY KEY (id);


--
-- Name: ci_faktura_ucet_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_faktura_ucet
    ADD CONSTRAINT ci_faktura_ucet_pkey PRIMARY KEY (id);


--
-- Name: ci_firma_historie_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_firma_historie
    ADD CONSTRAINT ci_firma_historie_pkey PRIMARY KEY (id);


--
-- Name: ci_firma_odkazy_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_firma_odkaz
    ADD CONSTRAINT ci_firma_odkazy_pkey PRIMARY KEY (id);


--
-- Name: ci_firma_odkazy_typ_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_firma_odkaz_typ
    ADD CONSTRAINT ci_firma_odkazy_typ_pkey PRIMARY KEY (id);


--
-- Name: ci_firma_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_firma
    ADD CONSTRAINT ci_firma_pkey PRIMARY KEY (id);


--
-- Name: ci_hledani_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_hledani
    ADD CONSTRAINT ci_hledani_pkey PRIMARY KEY (id);


--
-- Name: ci_hledani_typ_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_hledani_typ
    ADD CONSTRAINT ci_hledani_typ_pkey PRIMARY KEY (id);


--
-- Name: ci_kontakt_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_kontakt
    ADD CONSTRAINT ci_kontakt_pkey PRIMARY KEY (id);


--
-- Name: ci_kontakt_typ_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_kontakt_typ
    ADD CONSTRAINT ci_kontakt_typ_pkey PRIMARY KEY (id);


--
-- Name: ci_projekt_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_projekt
    ADD CONSTRAINT ci_projekt_pkey PRIMARY KEY (id);


--
-- Name: ci_projekt_tym_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_projekt_tym
    ADD CONSTRAINT ci_projekt_tym_pkey PRIMARY KEY (id);


--
-- Name: ci_projekt_ucast_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_projekt_ucast
    ADD CONSTRAINT ci_projekt_ucast_pkey PRIMARY KEY (id);


--
-- Name: ci_schuzka_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_schuzka_backup
    ADD CONSTRAINT ci_schuzka_pkey PRIMARY KEY (id);


--
-- Name: ci_spoluprace_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_spoluprace_detail
    ADD CONSTRAINT ci_spoluprace_detail_pkey PRIMARY KEY (id);


--
-- Name: ci_spoluprace_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_spoluprace
    ADD CONSTRAINT ci_spoluprace_pkey PRIMARY KEY (id);


--
-- Name: ci_typ_spoluprace_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_typ_spoluprace
    ADD CONSTRAINT ci_typ_spoluprace_pkey PRIMARY KEY (id);


--
-- Name: ci_ukol_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY ci_ukol
    ADD CONSTRAINT ci_ukol_pkey PRIMARY KEY (id);


--
-- Name: cvut_users_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_users
    ADD CONSTRAINT cvut_users_pkey PRIMARY KEY (user_id);


--
-- Name: download_group_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY download_group
    ADD CONSTRAINT download_group_pkey PRIMARY KEY (id);


--
-- Name: download_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY download
    ADD CONSTRAINT download_pkey PRIMARY KEY (id);


--
-- Name: emptytable_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY emptytable
    ADD CONSTRAINT emptytable_pkey PRIMARY KEY (id);


--
-- Name: exch_ci_trainees_lc_rok; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_ci_trainees
    ADD CONSTRAINT exch_ci_trainees_lc_rok UNIQUE (rok, lc);


--
-- Name: exch_country_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_country
    ADD CONSTRAINT exch_country_pkey PRIMARY KEY (country_id);


--
-- Name: exch_exam_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_exam
    ADD CONSTRAINT exch_exam_pkey PRIMARY KEY (id);


--
-- Name: exch_konkurz_email_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_konkurz_email
    ADD CONSTRAINT exch_konkurz_email_pkey PRIMARY KEY (id);


--
-- Name: exch_konkurz_tmp_id; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_konkurz_tmp
    ADD CONSTRAINT exch_konkurz_tmp_id PRIMARY KEY (id);


--
-- Name: exch_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_lang
    ADD CONSTRAINT exch_lang_pkey PRIMARY KEY (lang_id);


--
-- Name: exch_out_preffered_trainin_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_out_preffered_training_pri
    ADD CONSTRAINT exch_out_preffered_trainin_pkey PRIMARY KEY (id);


--
-- Name: exch_participant_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_participant
    ADD CONSTRAINT exch_participant_pkey PRIMARY KEY (p_id);


--
-- Name: exch_prefered_country_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_prefered_country
    ADD CONSTRAINT exch_prefered_country_pkey PRIMARY KEY (id);


--
-- Name: exch_report_cz_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_report_cz
    ADD CONSTRAINT exch_report_cz_pkey PRIMARY KEY (id);


--
-- Name: exch_specialization_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_specialization
    ADD CONSTRAINT exch_specialization_pkey PRIMARY KEY (specialization_id);


--
-- Name: exch_student_in_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_student_in
    ADD CONSTRAINT exch_student_in_pkey PRIMARY KEY (id);


--
-- Name: exch_student_n5b_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_student_n5b
    ADD CONSTRAINT exch_student_n5b_pkey PRIMARY KEY (id);


--
-- Name: exch_university_tmp_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY exch_university_tmp
    ADD CONSTRAINT exch_university_tmp_pkey PRIMARY KEY (id);


--
-- Name: faktury_items_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_faktury_items
    ADD CONSTRAINT faktury_items_pkey PRIMARY KEY (fitem_id);


--
-- Name: faktury_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_faktury
    ADD CONSTRAINT faktury_pkey PRIMARY KEY (faktura_id);


--
-- Name: firma_prj_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_firma_prj
    ADD CONSTRAINT firma_prj_pkey PRIMARY KEY (firma_id, projekt_id);


--
-- Name: firmy_katalog_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY firmy_katalog
    ADD CONSTRAINT firmy_katalog_pkey PRIMARY KEY (id);


--
-- Name: firmy_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_firmy
    ADD CONSTRAINT firmy_pkey PRIMARY KEY (firma_id);


--
-- Name: historie_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_historie
    ADD CONSTRAINT historie_pkey PRIMARY KEY (hist_id);


--
-- Name: kpp2002_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp2002
    ADD CONSTRAINT kpp2002_pkey PRIMARY KEY (id);


--
-- Name: kpp2003_anketa_firma_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp2003_anketa_firma
    ADD CONSTRAINT kpp2003_anketa_firma_pkey PRIMARY KEY (id);


--
-- Name: kpp2003_anketa_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp2003_anketa
    ADD CONSTRAINT kpp2003_anketa_pkey PRIMARY KEY (id);


--
-- Name: kpp_obory_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp_obory
    ADD CONSTRAINT kpp_obory_pkey PRIMARY KEY (id);


--
-- Name: kpp_studentum_nabizi_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp_studentum_nabizi
    ADD CONSTRAINT kpp_studentum_nabizi_pkey PRIMARY KEY (id);


--
-- Name: kpp_varianty_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY kpp_varianty
    ADD CONSTRAINT kpp_varianty_pkey PRIMARY KEY (id);


--
-- Name: lang_levels_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY lang_levels
    ADD CONSTRAINT lang_levels_pkey PRIMARY KEY ("level");


--
-- Name: lc_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY lc
    ADD CONSTRAINT lc_pkey PRIMARY KEY (id);


--
-- Name: lc_summer_exchange_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY lc_summer_exchange
    ADD CONSTRAINT lc_summer_exchange_pkey PRIMARY KEY (id);


--
-- Name: lidi_lc_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY lidi_lc
    ADD CONSTRAINT lidi_lc_pkey PRIMARY KEY (id);


--
-- Name: mailman_content_link; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY mailman_content
    ADD CONSTRAINT mailman_content_link UNIQUE (link);


--
-- Name: mailman_content_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY mailman_content
    ADD CONSTRAINT mailman_content_pkey PRIMARY KEY (id);


--
-- Name: offers_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (id);


--
-- Name: oforms_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY oforms
    ADD CONSTRAINT oforms_pkey PRIMARY KEY (id);


--
-- Name: postpraxe_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY postpraxe
    ADD CONSTRAINT postpraxe_pkey PRIMARY KEY (id);


--
-- Name: praxe_konkurs_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY praxe_konkurs
    ADD CONSTRAINT praxe_konkurs_pkey PRIMARY KEY (id);


--
-- Name: projekty_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY cvut_projekty
    ADD CONSTRAINT projekty_pkey PRIMARY KEY (projekt_id);


--
-- Name: sendmail_list_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY sendmail_list
    ADD CONSTRAINT sendmail_list_pkey PRIMARY KEY (id);


--
-- Name: sendmail_list_subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY sendmail_list_subscriber
    ADD CONSTRAINT sendmail_list_subscriber_pkey PRIMARY KEY (id);


--
-- Name: sendmail_log_content_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY sendmail_log_content
    ADD CONSTRAINT sendmail_log_content_pkey PRIMARY KEY (id);


--
-- Name: share_directory; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY "share"
    ADD CONSTRAINT share_directory UNIQUE (directory);


--
-- Name: share_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY "share"
    ADD CONSTRAINT share_pkey PRIMARY KEY (id);


--
-- Name: share_rights_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY share_rights
    ADD CONSTRAINT share_rights_pkey PRIMARY KEY (id);


--
-- Name: st_lang_levels_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY st_lang_levels
    ADD CONSTRAINT st_lang_levels_pkey PRIMARY KEY (id);


--
-- Name: st_study_types_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY st_study_types
    ADD CONSTRAINT st_study_types_pkey PRIMARY KEY (id);


--
-- Name: states_lang_old_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY states_lang_old
    ADD CONSTRAINT states_lang_old_pkey PRIMARY KEY (id);


--
-- Name: states_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY states_lang
    ADD CONSTRAINT states_lang_pkey PRIMARY KEY (id);


--
-- Name: states_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY states
    ADD CONSTRAINT states_pkey PRIMARY KEY (state);


--
-- Name: student_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY student
    ADD CONSTRAINT student_pkey PRIMARY KEY (id);


--
-- Name: test_calendar_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY test_calendar
    ADD CONSTRAINT test_calendar_pkey PRIMARY KEY (id);


--
-- Name: test_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY test
    ADD CONSTRAINT test_pkey PRIMARY KEY (xx);


--
-- Name: tmp_firm_pkey; Type: CONSTRAINT; Schema: public; Owner: alfi; Tablespace: 
--

ALTER TABLE ONLY tmp_firm
    ADD CONSTRAINT tmp_firm_pkey PRIMARY KEY (id);


--
-- Name: trainees_events_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY trainees_events
    ADD CONSTRAINT trainees_events_pkey PRIMARY KEY (id);


--
-- Name: trainees_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY trainees
    ADD CONSTRAINT trainees_pkey PRIMARY KEY (id);


--
-- Name: trainess_events_name; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY trainees_events
    ADD CONSTRAINT trainess_events_name UNIQUE (name);


--
-- Name: university_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY university
    ADD CONSTRAINT university_pkey PRIMARY KEY (id);


--
-- Name: user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY user_preferences
    ADD CONSTRAINT user_preferences_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_udb_id; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_udb_id UNIQUE (udb_id);


--
-- Name: users_udb_username; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_udb_username UNIQUE (udb_username);


--
-- Name: vpp2002_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY vpp2002
    ADD CONSTRAINT vpp2002_pkey PRIMARY KEY (id);


--
-- Name: vpp_pozice_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY vpp_pozice
    ADD CONSTRAINT vpp_pozice_pkey PRIMARY KEY (id);


--
-- Name: www_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY www
    ADD CONSTRAINT www_pkey PRIMARY KEY (id);


--
-- Name: years_pkey; Type: CONSTRAINT; Schema: public; Owner: intranet; Tablespace: 
--

ALTER TABLE ONLY years
    ADD CONSTRAINT years_pkey PRIMARY KEY (id);


SET search_path = regsys, pg_catalog;

--
-- Name: Events_pkey; Type: CONSTRAINT; Schema: regsys; Owner: rikard0; Tablespace: 
--

ALTER TABLE ONLY events
    ADD CONSTRAINT "Events_pkey" PRIMARY KEY (id);


--
-- Name: actions_pkey; Type: CONSTRAINT; Schema: regsys; Owner: rikard0; Tablespace: 
--

ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_pkey PRIMARY KEY (id);


--
-- Name: reg_action_pkey; Type: CONSTRAINT; Schema: regsys; Owner: rikard0; Tablespace: 
--

ALTER TABLE ONLY regs_action
    ADD CONSTRAINT reg_action_pkey PRIMARY KEY (id);


--
-- Name: reg_events_pkey; Type: CONSTRAINT; Schema: regsys; Owner: rikard0; Tablespace: 
--

ALTER TABLE ONLY regs_event
    ADD CONSTRAINT reg_events_pkey PRIMARY KEY (id);


--
-- Name: terms_pkey; Type: CONSTRAINT; Schema: regsys; Owner: rikard0; Tablespace: 
--

ALTER TABLE ONLY terms
    ADD CONSTRAINT terms_pkey PRIMARY KEY (id);


SET search_path = udb, pg_catalog;

--
-- Name: Conferencies_type_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conference_types
    ADD CONSTRAINT "Conferencies_type_pkey" PRIMARY KEY (id);


--
-- Name: Conferencies_type_unique; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conference_types
    ADD CONSTRAINT "Conferencies_type_unique" UNIQUE (conference_type);


--
-- Name: Conferencies_unique; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT "Conferencies_unique" UNIQUE (name);


--
-- Name: Lcs_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY lcs
    ADD CONSTRAINT "Lcs_pkey" PRIMARY KEY (id);


--
-- Name: Lcs_unique; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY lcs
    ADD CONSTRAINT "Lcs_unique" UNIQUE (code, name, dic, ico);


--
-- Name: Rights_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY rights
    ADD CONSTRAINT "Rights_pkey" PRIMARY KEY (id);


--
-- Name: Services_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY services
    ADD CONSTRAINT "Services_pkey" PRIMARY KEY (users_id);


--
-- Name: Teams_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT "Teams_pkey" PRIMARY KEY (id);


--
-- Name: Teams_unique; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT "Teams_unique" UNIQUE (name, lcs_id);


--
-- Name: Users_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- Name: Users_unique_mail; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_unique_mail" UNIQUE (iaeste_mail);


--
-- Name: Users_unique_username; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_unique_username" UNIQUE (username);


--
-- Name: conferencies_mail_ukey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conferencies_mail
    ADD CONSTRAINT conferencies_mail_ukey UNIQUE (conferencies_id, member_mail);


--
-- Name: conferencies_pkey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT conferencies_pkey PRIMARY KEY (id);


--
-- Name: conferencies_users_ukey; Type: CONSTRAINT; Schema: udb; Owner: udbmaster; Tablespace: 
--

ALTER TABLE ONLY conferencies_users
    ADD CONSTRAINT conferencies_users_ukey UNIQUE (conferencies_id, users_id);


SET search_path = exch, pg_catalog;

--
-- Name: exch_company_available; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_company_available ON exch_company USING btree (available);


--
-- Name: exch_company_refno_year; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_company_refno_year ON exch_company USING btree (ref_no, "year");


--
-- Name: exch_company_year; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_company_year ON exch_company USING btree ("year");


--
-- Name: exch_country_id_pkey; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_country_id_pkey ON exch_country USING btree (id);


--
-- Name: exch_country_name_en; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_country_name_en ON exch_country USING btree (name_en);


--
-- Name: exch_exam_student_points_sum; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_exam_student_points_sum ON exch_exam_student USING btree (points_sum);


--
-- Name: exch_exam_year; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_exam_year ON exch_exam USING btree ("year");


--
-- Name: exch_month_or_week_pkey; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_month_or_week_pkey ON exch_month_or_week USING btree (id);


--
-- Name: exch_not_prefered_country_index_1; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_not_prefered_country_index_1 ON exch_not_prefered_country USING btree (exch_exam_student_id, exch_country_id);


--
-- Name: exch_prefered_country_index_1; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_prefered_country_index_1 ON exch_prefered_country USING btree (exch_exam_student_id, exch_country_id);


--
-- Name: exch_public_text_index_1; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_public_text_index_1 ON exch_public_text USING btree ("key");


--
-- Name: exch_public_text_index_2; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_public_text_index_2 ON exch_public_text USING btree ("key", "type");


--
-- Name: exch_request_company_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_request_company_id ON exch_request USING btree (exch_company_id);


--
-- Name: exch_request_date_created; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_request_date_created ON exch_request USING btree (date_created);


--
-- Name: exch_request_exam_student_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_request_exam_student_id ON exch_request USING btree (exch_exam_student_id);


--
-- Name: exch_request_pkey; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_request_pkey ON exch_request USING btree (id);


--
-- Name: exch_request_priority; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_request_priority ON exch_request USING btree (priority);


--
-- Name: exch_request_state_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_request_state_id ON exch_request USING btree (exch_request_state_id);


--
-- Name: exch_training_company; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_training_company ON exch_training USING btree (exch_company_id);


--
-- Name: exch_training_company_student; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX exch_training_company_student ON exch_training USING btree (exch_company_id, exch_student_id);


--
-- Name: exch_training_student; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exch_training_student ON exch_training USING btree (exch_student_id);


--
-- Name: exma_index; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX exma_index ON exch_exam_student USING btree (exch_exam_id);


--
-- Name: index_hash; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE INDEX index_hash ON exch_exam_student USING btree (hash);


--
-- Name: index_unique; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX index_unique ON exch_translate USING btree (table_name, table_id, lang);


--
-- Name: un_company_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX un_company_id ON exch_company USING btree (ref_no);


--
-- Name: unique_martital_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX unique_martital_id ON exch_marital_status USING btree (id);


--
-- Name: unique_sex_id; Type: INDEX; Schema: exch; Owner: exchange; Tablespace: 
--

CREATE UNIQUE INDEX unique_sex_id ON exch_sex USING btree (id);


SET search_path = public, pg_catalog;

--
-- Name: board_replyto; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX board_replyto ON board USING btree (replyto);


--
-- Name: board_strom; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX board_strom ON board USING btree (strom);


--
-- Name: ci_faktura_lang_key-locale; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_faktura_lang_key-locale" ON ci_faktura_lang USING btree ("key", locale);


--
-- Name: ci_faktura_lang_pkey; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_faktura_lang_pkey ON ci_faktura_lang USING btree (id);


--
-- Name: ci_faktura_lc-typ-rok; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_faktura_lc-typ-rok" ON ci_faktura USING btree (rok, typ, lc);


--
-- Name: ci_faktura_text_nazev; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_faktura_text_nazev ON ci_faktura_text USING btree (nazev);


--
-- Name: ci_faktura_typ_text_typ-locale; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_faktura_typ_text_typ-locale" ON ci_faktura_typ_text USING btree (typ, locale);


--
-- Name: ci_firma_alias; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_firma_alias ON ci_firma USING btree (alias);


--
-- Name: ci_firma_historie_mtime; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_firma_historie_mtime ON ci_firma_historie USING btree (mtime);


--
-- Name: ci_firma_ico; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_firma_ico ON ci_firma USING btree (ico);


--
-- Name: ci_firma_lcreg; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_firma_lcreg ON ci_firma USING btree (lc_reg);


--
-- Name: ci_firma_nazev; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_firma_nazev ON ci_firma USING btree (nazev);


--
-- Name: ci_kontakt_aktivni; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_kontakt_aktivni ON ci_kontakt USING btree (aktivni);


--
-- Name: ci_kontakt_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_kontakt_firma ON ci_kontakt USING btree (firma);


--
-- Name: ci_kontakt_firma-id; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_kontakt_firma-id" ON ci_kontakt USING btree (firma, id);


--
-- Name: ci_kontakt_ico_typ; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_kontakt_ico_typ ON ci_kontakt USING btree (ico, typ);


--
-- Name: ci_kontakt_prijmeni_jmeno; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_kontakt_prijmeni_jmeno ON ci_kontakt USING btree (jmeno_prijmeni, jmeno);


--
-- Name: ci_projekt_typ_id_key; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE UNIQUE INDEX ci_projekt_typ_id_key ON ci_projekt_typ USING btree (id);


--
-- Name: ci_schuzka_datum; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_schuzka_datum ON ci_schuzka_backup USING btree (datum);


--
-- Name: ci_schuzka_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_schuzka_firma ON ci_schuzka_backup USING btree (firma);


--
-- Name: ci_schuzka_kontaktovat; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_schuzka_kontaktovat ON ci_schuzka_backup USING btree (kontaktovat);


--
-- Name: ci_schuzka_mtime; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_schuzka_mtime ON ci_schuzka_backup USING btree (mtime);


--
-- Name: ci_schuzka_projekt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_schuzka_projekt ON ci_schuzka_backup USING btree (projekt);


--
-- Name: ci_spoluprace_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_spoluprace_firma ON ci_spoluprace USING btree (firma);


--
-- Name: ci_spoluprace_projekt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_spoluprace_projekt ON ci_spoluprace USING btree (projekt);


--
-- Name: ci_spoluprace_ucast; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_spoluprace_ucast ON ci_spoluprace USING btree (ucast);


--
-- Name: ci_ukol_datum; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_datum ON ci_ukol USING btree (datum);


--
-- Name: ci_ukol_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_firma ON ci_ukol USING btree (firma);


--
-- Name: ci_ukol_kdo_lc; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_kdo_lc ON ci_ukol USING btree (kdo_lc);


--
-- Name: ci_ukol_lc; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_lc ON ci_ukol USING btree (lc);


--
-- Name: ci_ukol_lc-firma-vlozeno; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_ukol_lc-firma-vlozeno" ON ci_ukol USING btree (firma, lc, vlozeno);


--
-- Name: ci_ukol_muid; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_muid ON ci_ukol USING btree (muid);


--
-- Name: ci_ukol_nazev; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_nazev ON ci_ukol USING btree (nazev);


--
-- Name: ci_ukol_perspektiva; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_perspektiva ON ci_ukol USING btree (perspektiva);


--
-- Name: ci_ukol_projekt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_projekt ON ci_ukol USING btree (projekt);


--
-- Name: ci_ukol_projekt-ukol; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "ci_ukol_projekt-ukol" ON ci_ukol USING btree (projekt, ukol);


--
-- Name: ci_ukol_prubeh; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_prubeh ON ci_ukol USING btree (prubeh);


--
-- Name: ci_ukol_ukol; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_ukol ON ci_ukol USING btree (ukol);


--
-- Name: ci_ukol_vlozeno; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_vlozeno ON ci_ukol USING btree (vlozeno);


--
-- Name: ci_ukol_vlozil; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX ci_ukol_vlozil ON ci_ukol USING btree (vlozil);


--
-- Name: exch_ci_trainees_pkey; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_ci_trainees_pkey ON exch_ci_trainees USING btree (id);


--
-- Name: exch_company_in_id; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE UNIQUE INDEX exch_company_in_id ON exch_company_in USING btree (id);


--
-- Name: exch_exam_lc; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_exam_lc ON exch_exam USING btree (lc);


--
-- Name: exch_exam_lc-rok; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "exch_exam_lc-rok" ON exch_exam USING btree (lc, rok);


--
-- Name: exch_exam_rok; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_exam_rok ON exch_exam USING btree (rok);


--
-- Name: exch_konkurz_faculty; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_faculty ON exch_konkurz_tmp USING btree (faculty);


--
-- Name: exch_konkurz_jazyk1; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_jazyk1 ON exch_konkurz_tmp USING btree (jazyk1n);


--
-- Name: exch_konkurz_jazyk2; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_jazyk2 ON exch_konkurz_tmp USING btree (jazyk2n);


--
-- Name: exch_konkurz_jazyk3; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_jazyk3 ON exch_konkurz_tmp USING btree (jazyk3n);


--
-- Name: exch_konkurz_konkurzmisto; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_konkurzmisto ON exch_konkurz_tmp USING btree (konkurzmisto);


--
-- Name: exch_konkurz_npref1; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_npref1 ON exch_konkurz_tmp USING btree (npref1);


--
-- Name: exch_konkurz_npref2; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_npref2 ON exch_konkurz_tmp USING btree (npref2);


--
-- Name: exch_konkurz_npref3; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_npref3 ON exch_konkurz_tmp USING btree (npref3);


--
-- Name: exch_konkurz_pref1; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_pref1 ON exch_konkurz_tmp USING btree (pref1);


--
-- Name: exch_konkurz_pref2; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_pref2 ON exch_konkurz_tmp USING btree (pref2);


--
-- Name: exch_konkurz_pref3; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_pref3 ON exch_konkurz_tmp USING btree (pref3);


--
-- Name: exch_konkurz_pref4; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_pref4 ON exch_konkurz_tmp USING btree (pref4);


--
-- Name: exch_konkurz_pref5; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_pref5 ON exch_konkurz_tmp USING btree (pref5);


--
-- Name: exch_konkurz_specialization1; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_specialization1 ON exch_konkurz_tmp USING btree (obor1);


--
-- Name: exch_konkurz_specialization2; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_specialization2 ON exch_konkurz_tmp USING btree (obor2);


--
-- Name: exch_konkurz_specialization3; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_konkurz_specialization3 ON exch_konkurz_tmp USING btree (obor3);


--
-- Name: exch_out_preffered_tr_oform; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_out_preffered_tr_oform ON exch_out_preffered_training USING btree (oform);


--
-- Name: exch_out_preffered_tr_student; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX exch_out_preffered_tr_student ON exch_out_preffered_training USING btree (student);


--
-- Name: faktury_firma_id; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX faktury_firma_id ON cvut_faktury USING btree (firma_id);


--
-- Name: faktury_items_faktura_id; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX faktury_items_faktura_id ON cvut_faktury_items USING btree (faktura_id);


--
-- Name: firma_prj_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firma_prj_firma ON cvut_firma_prj USING btree (firma_id);


--
-- Name: firma_prj_projekt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firma_prj_projekt ON cvut_firma_prj USING btree (projekt_id);


--
-- Name: firma_prj_user; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firma_prj_user ON cvut_firma_prj USING btree (user_id);


--
-- Name: firmy_ico; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firmy_ico ON cvut_firmy USING btree (ico);


--
-- Name: firmy_ico_num; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firmy_ico_num ON cvut_firmy USING btree (ico_num);


--
-- Name: firmy_nazev; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firmy_nazev ON cvut_firmy USING btree (nazev);


--
-- Name: firmy_poznamka; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX firmy_poznamka ON cvut_firmy USING btree (poznamka);


--
-- Name: historie_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX historie_firma ON cvut_historie USING btree (firma_id);


--
-- Name: i_share_access; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX i_share_access ON "share" USING btree ("access");


--
-- Name: idx_share_access_us-gr-right; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "idx_share_access_us-gr-right" ON share_access USING btree ("user", "group", "right");


--
-- Name: kpp2002_grafika; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX kpp2002_grafika ON kpp2002 USING btree (grafika);


--
-- Name: kpp2002_lc; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX kpp2002_lc ON kpp2002 USING btree (lc);


--
-- Name: kpp2002_lc-grafika-rok; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "kpp2002_lc-grafika-rok" ON kpp2002 USING btree (lc, grafika, rok);


--
-- Name: kpp2002_rok; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX kpp2002_rok ON kpp2002 USING btree (rok);


--
-- Name: lc_username_key; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE UNIQUE INDEX lc_username_key ON lc USING btree (username);


--
-- Name: mailman_content_date; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX mailman_content_date ON mailman_content USING btree (date);


--
-- Name: mailman_content_date_lc_list; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX mailman_content_date_lc_list ON mailman_content USING btree (date, lc, list);


--
-- Name: mailman_content_list; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX mailman_content_list ON mailman_content USING btree (list);


--
-- Name: mailman_subject; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX mailman_subject ON mailman_content USING btree (subject);


--
-- Name: oforms_year_reserved; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX oforms_year_reserved ON oforms USING btree ("year", reserved);


--
-- Name: projekty_ci_projekt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX projekty_ci_projekt ON cvut_projekty USING btree (ci_projekt);


--
-- Name: s_ci_faktura_firma; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX s_ci_faktura_firma ON ci_faktura USING btree (firma);


--
-- Name: s_ci_faktura_lc; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX s_ci_faktura_lc ON ci_faktura USING btree (lc);


--
-- Name: s_ci_faktura_polozka_faktura; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX s_ci_faktura_polozka_faktura ON ci_faktura_polozka USING btree (faktura);


--
-- Name: s_ci_faktura_stav; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX s_ci_faktura_stav ON ci_faktura USING btree (stav_uhrazeni);


--
-- Name: s_ci_faktura_typ; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX s_ci_faktura_typ ON ci_faktura USING btree (typ);


--
-- Name: sendmail_log_content_date; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX sendmail_log_content_date ON sendmail_log_content USING btree (date);


--
-- Name: sendmail_log_content_rcpt; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX sendmail_log_content_rcpt ON sendmail_log_content USING btree (rcpt);


--
-- Name: sendmail_log_rcpt_content; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX sendmail_log_rcpt_content ON sendmail_log_rcpt USING btree (content);


--
-- Name: share_access_id_key; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE UNIQUE INDEX share_access_id_key ON share_access USING btree (id);


--
-- Name: share_rights_user_group_right_ro; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX share_rights_user_group_right_ro ON share_rights USING btree (ro_user, ro_groupname, ro_right);


--
-- Name: states_lang-states-style-pagest; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "states_lang-states-style-pagest" ON states_lang USING btree (pagestyle, style, state);


--
-- Name: states_lang_loc-style-state; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX "states_lang_loc-style-state" ON states_lang USING btree (locale, pagestyle, style, state);


--
-- Name: trainees_event; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX trainees_event ON trainees USING btree (event);


--
-- Name: trainees_event_fkey; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX trainees_event_fkey ON trainees USING btree (event);


--
-- Name: user_preferences_user_name; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX user_preferences_user_name ON user_preferences USING btree (name, uid);


--
-- Name: users_groupname; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX users_groupname ON users USING btree (groupname);


--
-- Name: users_password_hash; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX users_password_hash ON users USING btree (password_hash);


--
-- Name: users_username; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE UNIQUE INDEX users_username ON users USING btree (username);


--
-- Name: yesno_tf; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX yesno_tf ON yesno USING btree (tf);


--
-- Name: yesno_yn; Type: INDEX; Schema: public; Owner: intranet; Tablespace: 
--

CREATE INDEX yesno_yn ON yesno USING btree (yn);


SET search_path = udb, pg_catalog;

--
-- Name: Users_fkey_rights_id; Type: INDEX; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE INDEX "Users_fkey_rights_id" ON users USING btree (rights_id);


--
-- Name: pket_index_conferencies_mail; Type: INDEX; Schema: udb; Owner: udbmaster; Tablespace: 
--

CREATE UNIQUE INDEX pket_index_conferencies_mail ON conferencies_mail USING btree (conferencies_id, member_mail);


SET search_path = exch, pg_catalog;

--
-- Name: TR_acceptation_date; Type: TRIGGER; Schema: exch; Owner: exchange
--

CREATE TRIGGER "TR_acceptation_date"
    BEFORE UPDATE ON exch_training
    FOR EACH ROW
    EXECUTE PROCEDURE f_training_acceptation_date();


--
-- Name: TR_delete_train; Type: TRIGGER; Schema: exch; Owner: exchange
--

CREATE TRIGGER "TR_delete_train"
    AFTER DELETE ON exch_training
    FOR EACH ROW
    EXECUTE PROCEDURE "F_update_company_after_training"();


--
-- Name: TR_update_company_after_training; Type: TRIGGER; Schema: exch; Owner: exchange
--

CREATE TRIGGER "TR_update_company_after_training"
    AFTER INSERT OR UPDATE ON exch_training
    FOR EACH ROW
    EXECUTE PROCEDURE "F_update_company_after_training"();


--
-- Name: exch_request_update; Type: TRIGGER; Schema: exch; Owner: exchange
--

CREATE TRIGGER exch_request_update
    AFTER UPDATE ON exch_request
    FOR EACH ROW
    EXECUTE PROCEDURE f_update_company_request();


--
-- Name: tr_ci_kontakt_trainee; Type: TRIGGER; Schema: exch; Owner: exchange
--

CREATE TRIGGER tr_ci_kontakt_trainee
    AFTER UPDATE ON exch_training
    FOR EACH ROW
    EXECUTE PROCEDURE public.exch_ci_kontakt_trainee();


SET search_path = public, pg_catalog;

--
-- Name: RI_ConstraintTrigger_50465; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_kontakt
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50466; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50467; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50468; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON vpp_pozice
    FROM vpp2002
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'vpp_pozice', 'vpp2002', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50469; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON vpp2002
    FROM vpp_pozice
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'vpp_pozice', 'vpp2002', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50470; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON vpp2002
    FROM vpp_pozice
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'vpp_pozice', 'vpp2002', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50471; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON sendmail_log_rcpt
    FROM sendmail_log_content
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'sendmail_log_rcpt', 'sendmail_log_content', 'UNSPECIFIED', 'content', 'id');


--
-- Name: RI_ConstraintTrigger_50472; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON sendmail_log_content
    FROM sendmail_log_rcpt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'sendmail_log_rcpt', 'sendmail_log_content', 'UNSPECIFIED', 'content', 'id');


--
-- Name: RI_ConstraintTrigger_50473; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON sendmail_log_content
    FROM sendmail_log_rcpt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'sendmail_log_rcpt', 'sendmail_log_content', 'UNSPECIFIED', 'content', 'id');


--
-- Name: RI_ConstraintTrigger_50474; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON sendmail_list_subscriber
    FROM sendmail_list
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'sendmail_list_subscriber', 'sendmail_list', 'UNSPECIFIED', 'list', 'id');


--
-- Name: RI_ConstraintTrigger_50475; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON sendmail_list
    FROM sendmail_list_subscriber
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'sendmail_list_subscriber', 'sendmail_list', 'UNSPECIFIED', 'list', 'id');


--
-- Name: RI_ConstraintTrigger_50476; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON sendmail_list
    FROM sendmail_list_subscriber
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'sendmail_list_subscriber', 'sendmail_list', 'UNSPECIFIED', 'list', 'id');


--
-- Name: RI_ConstraintTrigger_50477; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_firma_historie
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_firma_historie', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50478; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_firma_historie
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'ci_firma_historie', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50479; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_firma_historie
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_firma_historie', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50480; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON download_group
    FROM download_group
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'download_group', 'download_group', 'UNSPECIFIED', 'parent', 'id');


--
-- Name: RI_ConstraintTrigger_50481; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON download_group
    FROM download_group
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'download_group', 'download_group', 'UNSPECIFIED', 'parent', 'id');


--
-- Name: RI_ConstraintTrigger_50482; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON download_group
    FROM download_group
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'download_group', 'download_group', 'UNSPECIFIED', 'parent', 'id');


--
-- Name: RI_ConstraintTrigger_50483; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON download
    FROM download_group
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'download', 'download_group', 'UNSPECIFIED', 'groupid', 'id');


--
-- Name: RI_ConstraintTrigger_50484; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON download_group
    FROM download
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'download', 'download_group', 'UNSPECIFIED', 'groupid', 'id');


--
-- Name: RI_ConstraintTrigger_50485; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON download_group
    FROM download
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'download', 'download_group', 'UNSPECIFIED', 'groupid', 'id');


--
-- Name: RI_ConstraintTrigger_50486; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON kpp2003_anketa_firma
    FROM kpp2003_anketa
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'kpp2003_anketa_firma', 'kpp2003_anketa', 'UNSPECIFIED', 'dotaznik', 'id');


--
-- Name: RI_ConstraintTrigger_50487; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON kpp2003_anketa
    FROM kpp2003_anketa_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'kpp2003_anketa_firma', 'kpp2003_anketa', 'UNSPECIFIED', 'dotaznik', 'id');


--
-- Name: RI_ConstraintTrigger_50488; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON kpp2003_anketa
    FROM kpp2003_anketa_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'kpp2003_anketa_firma', 'kpp2003_anketa', 'UNSPECIFIED', 'dotaznik', 'id');


--
-- Name: RI_ConstraintTrigger_50489; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_spoluprace
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_spoluprace', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50490; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_spoluprace
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_spoluprace', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50491; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_spoluprace
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_spoluprace', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50492; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER projekt_fk
    AFTER INSERT OR UPDATE ON ci_spoluprace
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('projekt_fk', 'ci_spoluprace', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50493; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER projekt_fk
    AFTER DELETE ON ci_projekt
    FROM ci_spoluprace
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('projekt_fk', 'ci_spoluprace', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50494; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER projekt_fk
    AFTER UPDATE ON ci_projekt
    FROM ci_spoluprace
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('projekt_fk', 'ci_spoluprace', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50495; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_kontakt_firma
    AFTER INSERT OR UPDATE ON ci_kontakt
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('ci_kontakt_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50496; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_kontakt_firma
    AFTER DELETE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('ci_kontakt_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50497; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_kontakt_firma
    AFTER UPDATE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('ci_kontakt_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50498; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_firma_historie_firma
    AFTER INSERT OR UPDATE ON ci_kontakt
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('ci_firma_historie_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50499; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_firma_historie_firma
    AFTER DELETE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('ci_firma_historie_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50500; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_firma_historie_firma
    AFTER UPDATE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('ci_firma_historie_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50501; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_schuzka_firma
    AFTER INSERT OR UPDATE ON ci_kontakt
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('ci_schuzka_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50502; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_schuzka_firma
    AFTER DELETE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('ci_schuzka_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50503; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_schuzka_firma
    AFTER UPDATE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('ci_schuzka_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50504; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_spoluprace_firma
    AFTER INSERT OR UPDATE ON ci_kontakt
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('ci_spoluprace_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50505; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_spoluprace_firma
    AFTER DELETE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('ci_spoluprace_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50506; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER ci_spoluprace_firma
    AFTER UPDATE ON ci_firma
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('ci_spoluprace_firma', 'ci_kontakt', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50507; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON exch_student_n5b
    FROM exch_student_in
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'exch_student_n5b', 'exch_student_in', 'UNSPECIFIED', 'student_in', 'id');


--
-- Name: RI_ConstraintTrigger_50508; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON exch_student_in
    FROM exch_student_n5b
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'exch_student_n5b', 'exch_student_in', 'UNSPECIFIED', 'student_in', 'id');


--
-- Name: RI_ConstraintTrigger_50509; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON exch_student_in
    FROM exch_student_n5b
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'exch_student_n5b', 'exch_student_in', 'UNSPECIFIED', 'student_in', 'id');


--
-- Name: RI_ConstraintTrigger_50510; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_faktura', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50511; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_faktura', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50512; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON cvut_historie
NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'cvut_historie', 'firmy', 'UNSPECIFIED', 'firma_id', 'firma_id');


--
-- Name: RI_ConstraintTrigger_50513; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON cvut_projekty
NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'firma_prj', 'cvut_projekty', 'UNSPECIFIED', 'projekt_id', 'projekt_id');


--
-- Name: RI_ConstraintTrigger_50514; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON cvut_projekty
NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'firma_prj', 'cvut_projekty', 'UNSPECIFIED', 'projekt_id', 'projekt_id');


--
-- Name: RI_ConstraintTrigger_50515; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_kontakt
    FROM lc_summer_exchange
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'lc_summer_exchange', 'ci_kontakt', 'UNSPECIFIED', 'person', 'id');


--
-- Name: RI_ConstraintTrigger_50516; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_kontakt
    FROM lc_summer_exchange
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'lc_summer_exchange', 'ci_kontakt', 'UNSPECIFIED', 'person', 'id');


--
-- Name: RI_ConstraintTrigger_50517; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON exch_out_preffered_training
    FROM oforms
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'exch_out_preffered_training', 'oforms', 'UNSPECIFIED', 'oform', 'id');


--
-- Name: RI_ConstraintTrigger_50518; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON oforms
    FROM exch_out_preffered_training
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'exch_out_preffered_training', 'oforms', 'UNSPECIFIED', 'oform', 'id');


--
-- Name: RI_ConstraintTrigger_50519; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON oforms
    FROM exch_out_preffered_training
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'exch_out_preffered_training', 'oforms', 'UNSPECIFIED', 'oform', 'id');


--
-- Name: RI_ConstraintTrigger_50520; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON exch_out_preffered_training
    FROM exch_konkurz_tmp
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'exch_out_preffered_training', 'exch_konkurz_tmp', 'UNSPECIFIED', 'student', 'id');


--
-- Name: RI_ConstraintTrigger_50521; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON exch_konkurz_tmp
    FROM exch_out_preffered_training
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'exch_out_preffered_training', 'exch_konkurz_tmp', 'UNSPECIFIED', 'student', 'id');


--
-- Name: RI_ConstraintTrigger_50522; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON exch_konkurz_tmp
    FROM exch_out_preffered_training
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'exch_out_preffered_training', 'exch_konkurz_tmp', 'UNSPECIFIED', 'student', 'id');


--
-- Name: RI_ConstraintTrigger_50523; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_faktura
    FROM ci_faktura_typ
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_faktura', 'ci_faktura_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50524; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_faktura_typ
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_faktura', 'ci_faktura_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50525; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_faktura_typ
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_faktura', 'ci_faktura_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50526; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_faktura
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_faktura', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50527; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_faktura', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50528; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_faktura', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50529; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_faktura
    FROM ci_faktura_stav
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_faktura', 'ci_faktura_stav', 'UNSPECIFIED', 'stav_uhrazeni', 'id');


--
-- Name: RI_ConstraintTrigger_50530; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_faktura_stav
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_faktura', 'ci_faktura_stav', 'UNSPECIFIED', 'stav_uhrazeni', 'id');


--
-- Name: RI_ConstraintTrigger_50531; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_faktura_stav
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_faktura', 'ci_faktura_stav', 'UNSPECIFIED', 'stav_uhrazeni', 'id');


--
-- Name: RI_ConstraintTrigger_50532; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_faktura_polozka
    FROM ci_faktura
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_faktura_polozka', 'ci_faktura', 'UNSPECIFIED', 'faktura', 'id');


--
-- Name: RI_ConstraintTrigger_50533; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_faktura
    FROM ci_faktura_polozka
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_del"('<unnamed>', 'ci_faktura_polozka', 'ci_faktura', 'UNSPECIFIED', 'faktura', 'id');


--
-- Name: RI_ConstraintTrigger_50534; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_faktura
    FROM ci_faktura_polozka
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_faktura_polozka', 'ci_faktura', 'UNSPECIFIED', 'faktura', 'id');


--
-- Name: RI_ConstraintTrigger_50535; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON users
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'users', 'users', 'UNSPECIFIED', 'm_uid', 'id');


--
-- Name: RI_ConstraintTrigger_50536; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON users
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'users', 'users', 'UNSPECIFIED', 'm_uid', 'id');


--
-- Name: RI_ConstraintTrigger_50537; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON users
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'users', 'users', 'UNSPECIFIED', 'm_uid', 'id');


--
-- Name: RI_ConstraintTrigger_50538; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM ci_firma
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50539; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_firma
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50540; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_firma
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_ukol', 'ci_firma', 'UNSPECIFIED', 'firma', 'id');


--
-- Name: RI_ConstraintTrigger_50541; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM lc
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'lc', 'UNSPECIFIED', 'lc', 'username');


--
-- Name: RI_ConstraintTrigger_50542; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON lc
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'lc', 'UNSPECIFIED', 'lc', 'username');


--
-- Name: RI_ConstraintTrigger_50543; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON lc
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_ukol', 'lc', 'UNSPECIFIED', 'lc', 'username');


--
-- Name: RI_ConstraintTrigger_50544; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'ci_kontakt', 'UNSPECIFIED', 'kdo_lc', 'id');


--
-- Name: RI_ConstraintTrigger_50545; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_kontakt
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'ci_kontakt', 'UNSPECIFIED', 'kdo_lc', 'id');


--
-- Name: RI_ConstraintTrigger_50546; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_kontakt
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_ukol', 'ci_kontakt', 'UNSPECIFIED', 'kdo_lc', 'id');


--
-- Name: RI_ConstraintTrigger_50547; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50548; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_projekt
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50549; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_projekt
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_ukol', 'ci_projekt', 'UNSPECIFIED', 'projekt', 'id');


--
-- Name: RI_ConstraintTrigger_50550; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'vlozil', 'id');


--
-- Name: RI_ConstraintTrigger_50551; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON users
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'vlozil', 'id');


--
-- Name: RI_ConstraintTrigger_50552; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON users
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'vlozil', 'id');


--
-- Name: RI_ConstraintTrigger_50553; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_ukol
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'muid', 'id');


--
-- Name: RI_ConstraintTrigger_50554; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON users
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'muid', 'id');


--
-- Name: RI_ConstraintTrigger_50555; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON users
    FROM ci_ukol
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_ukol', 'users', 'UNSPECIFIED', 'muid', 'id');


--
-- Name: RI_ConstraintTrigger_50556; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON users
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'users', 'ci_kontakt', 'UNSPECIFIED', 'ci_kontakt', 'id');


--
-- Name: RI_ConstraintTrigger_50557; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_kontakt
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'users', 'ci_kontakt', 'UNSPECIFIED', 'ci_kontakt', 'id');


--
-- Name: RI_ConstraintTrigger_50558; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_kontakt
    FROM users
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'users', 'ci_kontakt', 'UNSPECIFIED', 'ci_kontakt', 'id');


--
-- Name: RI_ConstraintTrigger_50559; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_projekt
    FROM ci_projekt_typ
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_projekt', 'ci_projekt_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50560; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_projekt_typ
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_projekt', 'ci_projekt_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50561; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_projekt_typ
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_upd"('<unnamed>', 'ci_projekt', 'ci_projekt_typ', 'UNSPECIFIED', 'typ', 'id');


--
-- Name: RI_ConstraintTrigger_50562; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER INSERT OR UPDATE ON ci_projekt
    FROM ci_kontakt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_check_ins"('<unnamed>', 'ci_projekt', 'ci_kontakt', 'UNSPECIFIED', 'koordinator', 'id');


--
-- Name: RI_ConstraintTrigger_50563; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER DELETE ON ci_kontakt
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_noaction_del"('<unnamed>', 'ci_projekt', 'ci_kontakt', 'UNSPECIFIED', 'koordinator', 'id');


--
-- Name: RI_ConstraintTrigger_50564; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE CONSTRAINT TRIGGER "<unnamed>"
    AFTER UPDATE ON ci_kontakt
    FROM ci_projekt
    NOT DEFERRABLE INITIALLY IMMEDIATE
    FOR EACH ROW
    EXECUTE PROCEDURE "RI_FKey_cascade_upd"('<unnamed>', 'ci_projekt', 'ci_kontakt', 'UNSPECIFIED', 'koordinator', 'id');


--
-- Name: share2ldap_update; Type: TRIGGER; Schema: public; Owner: intranet
--

CREATE TRIGGER share2ldap_update
    AFTER UPDATE ON users
    FOR EACH ROW
    EXECUTE PROCEDURE share2ldap_update_user();


SET search_path = udb, pg_catalog;

--
-- Name: udb2intranet_insert; Type: TRIGGER; Schema: udb; Owner: udbmaster
--

CREATE TRIGGER udb2intranet_insert
    AFTER INSERT ON users
    FOR EACH ROW
    EXECUTE PROCEDURE public.udb2intranet_insert();


--
-- Name: udb2intranet_services_insert; Type: TRIGGER; Schema: udb; Owner: udbmaster
--

CREATE TRIGGER udb2intranet_services_insert
    AFTER INSERT ON services
    FOR EACH ROW
    EXECUTE PROCEDURE public.udb2intranet_services();


--
-- Name: udb2intranet_services_update; Type: TRIGGER; Schema: udb; Owner: udbmaster
--

CREATE TRIGGER udb2intranet_services_update
    AFTER UPDATE ON services
    FOR EACH ROW
    EXECUTE PROCEDURE public.udb2intranet_services();


--
-- Name: udb2intranet_update; Type: TRIGGER; Schema: udb; Owner: udbmaster
--

CREATE TRIGGER udb2intranet_update
    AFTER UPDATE ON users
    FOR EACH ROW
    EXECUTE PROCEDURE public.udb2intranet_update();


SET search_path = exch, pg_catalog;

--
-- Name: company_categories_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT company_categories_fkey FOREIGN KEY (exch_work_category_abbr) REFERENCES exch_work_category(abbr) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_company_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT exch_company_fkey FOREIGN KEY (exch_company_id) REFERENCES exch_company(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_country_fkey2; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_country_fkey2 FOREIGN KEY (nominating_country_abbr) REFERENCES exch_country(abbr) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: exch_exam_student_citizenship_country_id_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT exch_exam_student_citizenship_country_id_fkey FOREIGN KEY (citizenship_country_id) REFERENCES exch_country(id);


--
-- Name: exch_exam_student_file_exchtype_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student_file
    ADD CONSTRAINT exch_exam_student_file_exchtype_fkey FOREIGN KEY (exchtype) REFERENCES exch_exam_student_file_type(id);


--
-- Name: exch_exam_student_sex_id_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT exch_exam_student_sex_id_fkey FOREIGN KEY (sex_id) REFERENCES exch_sex(id);


--
-- Name: exch_language1_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_language1_fkey FOREIGN KEY (language1_level_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_language2_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_language2_fkey FOREIGN KEY (language2_level_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_language3_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_language3_fkey FOREIGN KEY (language3_level_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_language4_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_language4_fkey FOREIGN KEY (language4_other_level_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_language5_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_language5_fkey FOREIGN KEY (language5_other_level_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_marital_status_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_marital_status_fkey FOREIGN KEY (marital_status_id) REFERENCES exch_marital_status(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_request_limits_exch_local_centre_id_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_request_limits
    ADD CONSTRAINT exch_request_limits_exch_local_centre_id_fkey FOREIGN KEY (exch_local_centre_id) REFERENCES public.lc(id);


--
-- Name: exch_sex_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_student
    ADD CONSTRAINT exch_sex_fkey FOREIGN KEY (sex_id) REFERENCES exch_sex(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_student_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT exch_student_fkey FOREIGN KEY (exch_student_id) REFERENCES exch_student(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: exch_training_state_fkey; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT exch_training_state_fkey FOREIGN KEY (exch_training_state_id) REFERENCES exch_training_state(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_company_id; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_request
    ADD CONSTRAINT fk_company_id FOREIGN KEY (exch_company_id) REFERENCES exch_company(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_company_id; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT fk_company_id FOREIGN KEY (exch_company_id) REFERENCES exch_company(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_english_and_or ; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT "fk_english_and_or " FOREIGN KEY (english_and_or) REFERENCES exch_and_or(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exam_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_faculty
    ADD CONSTRAINT fk_exam_student FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON DELETE CASCADE;


--
-- Name: fk_exam_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_country
    ADD CONSTRAINT fk_exam_student FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON DELETE CASCADE;


--
-- Name: fk_exam_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_not_prefered_country
    ADD CONSTRAINT fk_exam_student FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON DELETE CASCADE;


--
-- Name: fk_exam_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_language_knowledge
    ADD CONSTRAINT fk_exam_student FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON DELETE CASCADE;


--
-- Name: fk_exam_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_month
    ADD CONSTRAINT fk_exam_student FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON DELETE CASCADE;


--
-- Name: fk_exch_company; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company_faculty
    ADD CONSTRAINT fk_exch_company FOREIGN KEY (exch_company_id) REFERENCES exch_company(id) ON DELETE CASCADE;


--
-- Name: fk_exch_country; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_not_prefered_country
    ADD CONSTRAINT fk_exch_country FOREIGN KEY (exch_country_id) REFERENCES exch_exam_country(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_exam; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_exch_exam FOREIGN KEY (exch_exam_id) REFERENCES exch_exam(id) ON DELETE RESTRICT;


--
-- Name: fk_exch_faculty; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company_faculty
    ADD CONSTRAINT fk_exch_faculty FOREIGN KEY (exch_faculty_id) REFERENCES exch_faculty(id) ON DELETE RESTRICT;


--
-- Name: fk_exch_month; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_month
    ADD CONSTRAINT fk_exch_month FOREIGN KEY (exch_month_id) REFERENCES exch_month(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_prefered_residency_length; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_exch_prefered_residency_length FOREIGN KEY (exch_prefered_residency_length_id) REFERENCES exch_residency_length(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_student; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_training
    ADD CONSTRAINT fk_exch_student FOREIGN KEY (exch_student_id) REFERENCES exch_student(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_study_program; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_exch_study_program FOREIGN KEY (exch_study_program_id) REFERENCES exch_study_program(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_title; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_exch_title FOREIGN KEY (exch_title_id) REFERENCES exch_title(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_exch_training; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_employers_report
    ADD CONSTRAINT fk_exch_training FOREIGN KEY (exch_training_id) REFERENCES exch_training(id);


--
-- Name: fk_exma_language; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_exma_language FOREIGN KEY (exam_language) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_faculty; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_faculty
    ADD CONSTRAINT fk_faculty FOREIGN KEY (exch_faculty_id) REFERENCES exch_faculty(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_faculty_id; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_faculty_id FOREIGN KEY (exch_faculty_id) REFERENCES exch_exam_university(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_faculty_id; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_prefered_faculty
    ADD CONSTRAINT fk_faculty_id FOREIGN KEY (exch_faculty_id) REFERENCES exch_faculty(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_french_and_or; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_french_and_or FOREIGN KEY (french_and_or) REFERENCES exch_and_or(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_german_and_or ; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT "fk_german_and_or " FOREIGN KEY (german_and_or) REFERENCES exch_and_or(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_lang_level_de; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_lang_level_de FOREIGN KEY (german) REFERENCES exch_language_level(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_lang_level_en; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_lang_level_en FOREIGN KEY (english) REFERENCES exch_language_level(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_lang_level_fr; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_lang_level_fr FOREIGN KEY (french) REFERENCES exch_language_level(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_language; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_language_knowledge
    ADD CONSTRAINT fk_language FOREIGN KEY (exch_language_id) REFERENCES exch_language(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_pay_per_employe; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_pay_per_employe FOREIGN KEY (pay_per_employer) REFERENCES exch_month_or_week(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_pay_per_gross; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_pay_per_gross FOREIGN KEY (pay_per_gross) REFERENCES exch_month_or_week(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_pay_per_iaeste; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_pay_per_iaeste FOREIGN KEY (pay_per_iaeste) REFERENCES exch_month_or_week(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_pay_per_lodging; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT fk_pay_per_lodging FOREIGN KEY (pay_per_lodging) REFERENCES exch_month_or_week(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_student_id; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_request
    ADD CONSTRAINT fk_student_id FOREIGN KEY (exch_exam_student_id) REFERENCES exch_exam_student(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_year_class; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_exam_student
    ADD CONSTRAINT fk_year_class FOREIGN KEY (exch_year_class_id) REFERENCES exch_year_class(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: pay_per_living; Type: FK CONSTRAINT; Schema: exch; Owner: exchange
--

ALTER TABLE ONLY exch_company
    ADD CONSTRAINT pay_per_living FOREIGN KEY (pay_per_living) REFERENCES exch_month_or_week(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


SET search_path = public, pg_catalog;

--
-- Name: ci_firma_odkaz_typ-id; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_firma_odkaz
    ADD CONSTRAINT "ci_firma_odkaz_typ-id" FOREIGN KEY (typ) REFERENCES ci_firma_odkaz_typ(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ci_firma_odkazy_firma-id; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_firma_odkaz
    ADD CONSTRAINT "ci_firma_odkazy_firma-id" FOREIGN KEY (firma) REFERENCES ci_firma(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: ci_kontakt_typ_fkey; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_kontakt
    ADD CONSTRAINT ci_kontakt_typ_fkey FOREIGN KEY (typ) REFERENCES ci_kontakt_typ(id);


--
-- Name: ci_projekt_tym_ci_kontakt_fkey; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_projekt_tym
    ADD CONSTRAINT ci_projekt_tym_ci_kontakt_fkey FOREIGN KEY (ci_kontakt) REFERENCES ci_kontakt(id);


--
-- Name: ci_projekt_tym_ci_projekt; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_projekt_tym
    ADD CONSTRAINT ci_projekt_tym_ci_projekt FOREIGN KEY (ci_projekt) REFERENCES ci_projekt(id);


--
-- Name: ci_projekt_tym_m_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY ci_projekt_tym
    ADD CONSTRAINT ci_projekt_tym_m_uid_fkey FOREIGN KEY (m_uid) REFERENCES users(id);


--
-- Name: exch_ci_trainees_ci_firma; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY exch_ci_trainees
    ADD CONSTRAINT exch_ci_trainees_ci_firma FOREIGN KEY (ci_firma) REFERENCES ci_firma(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: exch_ci_trainees_lc; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY exch_ci_trainees
    ADD CONSTRAINT exch_ci_trainees_lc FOREIGN KEY (lc) REFERENCES lc(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: fk_person_id; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY lc_summer_exchange
    ADD CONSTRAINT fk_person_id FOREIGN KEY (person) REFERENCES ci_kontakt(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: share_directory_fkey; Type: FK CONSTRAINT; Schema: public; Owner: intranet
--

ALTER TABLE ONLY share_rights
    ADD CONSTRAINT share_directory_fkey FOREIGN KEY ("share") REFERENCES "share"(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


SET search_path = udb, pg_catalog;

--
-- Name: Conferencies_fkey_conference_type_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT "Conferencies_fkey_conference_type_id" FOREIGN KEY (conference_types_id) REFERENCES conference_types(id) ON DELETE CASCADE;


--
-- Name: Conferencies_fkey_lcs_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT "Conferencies_fkey_lcs_id" FOREIGN KEY (lcs_id) REFERENCES lcs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Conferencies_fkey_rights_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT "Conferencies_fkey_rights_id" FOREIGN KEY (rights_id) REFERENCES rights(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Conferencies_fkey_teams_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies
    ADD CONSTRAINT "Conferencies_fkey_teams_id" FOREIGN KEY (teams_id) REFERENCES teams(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Temas_fkey_lcs_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT "Temas_fkey_lcs_id" FOREIGN KEY (lcs_id) REFERENCES lcs(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: Users_fkey_lcs_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_fkey_lcs_id" FOREIGN KEY (lcs_id) REFERENCES lcs(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Users_fkey_rights_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_fkey_rights_id" FOREIGN KEY (rights_id) REFERENCES rights(id);


--
-- Name: Users_fkey_teams_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY users
    ADD CONSTRAINT "Users_fkey_teams_id" FOREIGN KEY (teams_id) REFERENCES teams(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: Users_fkey_users_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY services
    ADD CONSTRAINT "Users_fkey_users_id" FOREIGN KEY (users_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: conference_user_fkey_users_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies_users
    ADD CONSTRAINT conference_user_fkey_users_id FOREIGN KEY (users_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: conferencies_mail_conferencies_id_fkey; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies_mail
    ADD CONSTRAINT conferencies_mail_conferencies_id_fkey FOREIGN KEY (conferencies_id) REFERENCES conferencies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: onference_user_fkey_conferencies_id; Type: FK CONSTRAINT; Schema: udb; Owner: udbmaster
--

ALTER TABLE ONLY conferencies_users
    ADD CONSTRAINT onference_user_fkey_conferencies_id FOREIGN KEY (conferencies_id) REFERENCES conferencies(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: exch; Type: ACL; Schema: -; Owner: exchange
--

REVOKE ALL ON SCHEMA exch FROM PUBLIC;
REVOKE ALL ON SCHEMA exch FROM exchange;
GRANT ALL ON SCHEMA exch TO exchange;


--
-- Name: public; Type: ACL; Schema: -; Owner: intranet
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM intranet;
GRANT ALL ON SCHEMA public TO intranet;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: udb; Type: ACL; Schema: -; Owner: udbmaster
--

REVOKE ALL ON SCHEMA udb FROM PUBLIC;
REVOKE ALL ON SCHEMA udb FROM udbmaster;
GRANT ALL ON SCHEMA udb TO udbmaster;
GRANT ALL ON SCHEMA udb TO postgres;
GRANT USAGE ON SCHEMA udb TO udbrights_r;


SET search_path = public, pg_catalog;

--
-- Name: exch_ci_kontakt_trainee(); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION exch_ci_kontakt_trainee() FROM PUBLIC;
REVOKE ALL ON FUNCTION exch_ci_kontakt_trainee() FROM intranet;
GRANT ALL ON FUNCTION exch_ci_kontakt_trainee() TO intranet;
GRANT ALL ON FUNCTION exch_ci_kontakt_trainee() TO PUBLIC;
GRANT ALL ON FUNCTION exch_ci_kontakt_trainee() TO exchange;


--
-- Name: ldap_modify_share(text, text, text, text, text, text, text); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) FROM PUBLIC;
REVOKE ALL ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) FROM intranet;
GRANT ALL ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) TO intranet;
GRANT ALL ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) TO PUBLIC;
GRANT ALL ON FUNCTION ldap_modify_share(server text, base text, username text, "password" text, filter text, share_ro text, share_rw text) TO udbmaster;


--
-- Name: rights_set(character, integer, boolean); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) FROM PUBLIC;
REVOKE ALL ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) FROM intranet;
GRANT ALL ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) TO intranet;
GRANT ALL ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) TO PUBLIC;
GRANT ALL ON FUNCTION rights_set(rights character, "level" integer, "set" boolean) TO udbmaster;


--
-- Name: share2ldap(text); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION share2ldap(username text) FROM PUBLIC;
REVOKE ALL ON FUNCTION share2ldap(username text) FROM intranet;
GRANT ALL ON FUNCTION share2ldap(username text) TO intranet;
GRANT ALL ON FUNCTION share2ldap(username text) TO PUBLIC;
GRANT ALL ON FUNCTION share2ldap(username text) TO udbmaster;


--
-- Name: udb2intranet_insert(); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION udb2intranet_insert() FROM PUBLIC;
REVOKE ALL ON FUNCTION udb2intranet_insert() FROM intranet;
GRANT ALL ON FUNCTION udb2intranet_insert() TO intranet;
GRANT ALL ON FUNCTION udb2intranet_insert() TO PUBLIC;
GRANT ALL ON FUNCTION udb2intranet_insert() TO udbmaster;


--
-- Name: udb2intranet_services(); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION udb2intranet_services() FROM PUBLIC;
REVOKE ALL ON FUNCTION udb2intranet_services() FROM intranet;
GRANT ALL ON FUNCTION udb2intranet_services() TO intranet;
GRANT ALL ON FUNCTION udb2intranet_services() TO PUBLIC;
GRANT ALL ON FUNCTION udb2intranet_services() TO udbmaster;


--
-- Name: udb2intranet_update(); Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON FUNCTION udb2intranet_update() FROM PUBLIC;
REVOKE ALL ON FUNCTION udb2intranet_update() FROM intranet;
GRANT ALL ON FUNCTION udb2intranet_update() TO intranet;
GRANT ALL ON FUNCTION udb2intranet_update() TO PUBLIC;
GRANT ALL ON FUNCTION udb2intranet_update() TO udbmaster;


--
-- Name: ci_firma; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_firma FROM PUBLIC;
REVOKE ALL ON TABLE ci_firma FROM intranet;
GRANT ALL ON TABLE ci_firma TO intranet;
GRANT SELECT ON TABLE ci_firma TO exchange;
GRANT ALL ON TABLE ci_firma TO alex;


--
-- Name: ci_kontakt; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_kontakt FROM PUBLIC;
REVOKE ALL ON TABLE ci_kontakt FROM intranet;
GRANT ALL ON TABLE ci_kontakt TO intranet;
GRANT INSERT,SELECT,UPDATE ON TABLE ci_kontakt TO exchange;
GRANT SELECT,REFERENCES ON TABLE ci_kontakt TO udbmaster;
GRANT ALL ON TABLE ci_kontakt TO alex;


SET search_path = exch, pg_catalog;

--
-- Name: exch_company; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_company FROM PUBLIC;
REVOKE ALL ON TABLE exch_company FROM exchange;
GRANT ALL ON TABLE exch_company TO exchange;
GRANT ALL ON TABLE exch_company TO alex;


--
-- Name: exch_exam_student; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_exam_student FROM PUBLIC;
REVOKE ALL ON TABLE exch_exam_student FROM exchange;
GRANT ALL ON TABLE exch_exam_student TO exchange;
GRANT ALL ON TABLE exch_exam_student TO alex;


--
-- Name: exch_request; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_request FROM PUBLIC;
REVOKE ALL ON TABLE exch_request FROM exchange;
GRANT ALL ON TABLE exch_request TO exchange;
GRANT ALL ON TABLE exch_request TO alex;


--
-- Name: exch_and_or; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_and_or FROM PUBLIC;
REVOKE ALL ON TABLE exch_and_or FROM exchange;
GRANT ALL ON TABLE exch_and_or TO exchange;
GRANT ALL ON TABLE exch_and_or TO alex;


--
-- Name: exch_catalog; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_catalog FROM PUBLIC;
REVOKE ALL ON TABLE exch_catalog FROM exchange;
GRANT ALL ON TABLE exch_catalog TO exchange;
GRANT ALL ON TABLE exch_catalog TO alex;


--
-- Name: exch_company_count_year; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_company_count_year FROM PUBLIC;
REVOKE ALL ON TABLE exch_company_count_year FROM exchange;
GRANT ALL ON TABLE exch_company_count_year TO exchange;
GRANT ALL ON TABLE exch_company_count_year TO alex;


--
-- Name: exch_company_faculty; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_company_faculty FROM PUBLIC;
REVOKE ALL ON TABLE exch_company_faculty FROM exchange;
GRANT ALL ON TABLE exch_company_faculty TO exchange;
GRANT ALL ON TABLE exch_company_faculty TO alex;


--
-- Name: exch_country; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_country FROM exchange;
GRANT ALL ON TABLE exch_country TO exchange;
GRANT ALL ON TABLE exch_country TO alex;


--
-- Name: exch_employers_report; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_employers_report FROM PUBLIC;
REVOKE ALL ON TABLE exch_employers_report FROM exchange;
GRANT ALL ON TABLE exch_employers_report TO exchange;
GRANT ALL ON TABLE exch_employers_report TO alex;


--
-- Name: exch_exam; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_exam FROM PUBLIC;
REVOKE ALL ON TABLE exch_exam FROM exchange;
GRANT ALL ON TABLE exch_exam TO exchange;
GRANT ALL ON TABLE exch_exam TO alex;


--
-- Name: exch_exam_country; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_exam_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_exam_country FROM exchange;
GRANT ALL ON TABLE exch_exam_country TO exchange;
GRANT ALL ON TABLE exch_exam_country TO alex;


--
-- Name: exch_exam_university; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_exam_university FROM PUBLIC;
REVOKE ALL ON TABLE exch_exam_university FROM exchange;
GRANT ALL ON TABLE exch_exam_university TO exchange;
GRANT ALL ON TABLE exch_exam_university TO alex;


--
-- Name: exch_faculty; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_faculty FROM PUBLIC;
REVOKE ALL ON TABLE exch_faculty FROM exchange;
GRANT ALL ON TABLE exch_faculty TO exchange;
GRANT ALL ON TABLE exch_faculty TO alex;


--
-- Name: exch_language; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_language FROM PUBLIC;
REVOKE ALL ON TABLE exch_language FROM exchange;
GRANT ALL ON TABLE exch_language TO exchange;
GRANT ALL ON TABLE exch_language TO alex;


--
-- Name: exch_language_knowledge; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_language_knowledge FROM PUBLIC;
REVOKE ALL ON TABLE exch_language_knowledge FROM exchange;
GRANT ALL ON TABLE exch_language_knowledge TO exchange;
GRANT ALL ON TABLE exch_language_knowledge TO alex;


--
-- Name: exch_language_level; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_language_level FROM PUBLIC;
REVOKE ALL ON TABLE exch_language_level FROM exchange;
GRANT ALL ON TABLE exch_language_level TO exchange;
GRANT ALL ON TABLE exch_language_level TO alex;


--
-- Name: exch_marital_status; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_marital_status FROM PUBLIC;
REVOKE ALL ON TABLE exch_marital_status FROM exchange;
GRANT ALL ON TABLE exch_marital_status TO exchange;
GRANT ALL ON TABLE exch_marital_status TO alex;


--
-- Name: exch_month; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_month FROM PUBLIC;
REVOKE ALL ON TABLE exch_month FROM exchange;
GRANT ALL ON TABLE exch_month TO exchange;
GRANT ALL ON TABLE exch_month TO alex;


--
-- Name: exch_month_or_week; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_month_or_week FROM PUBLIC;
REVOKE ALL ON TABLE exch_month_or_week FROM exchange;
GRANT ALL ON TABLE exch_month_or_week TO exchange;
GRANT ALL ON TABLE exch_month_or_week TO alex;


--
-- Name: exch_not_prefered_country; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_not_prefered_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_not_prefered_country FROM exchange;
GRANT ALL ON TABLE exch_not_prefered_country TO exchange;
GRANT ALL ON TABLE exch_not_prefered_country TO alex;


--
-- Name: exch_postpraxe; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_postpraxe FROM PUBLIC;
REVOKE ALL ON TABLE exch_postpraxe FROM exchange;
GRANT ALL ON TABLE exch_postpraxe TO exchange;
GRANT ALL ON TABLE exch_postpraxe TO alex;


--
-- Name: exch_prefered_country; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_prefered_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_prefered_country FROM exchange;
GRANT ALL ON TABLE exch_prefered_country TO exchange;
GRANT ALL ON TABLE exch_prefered_country TO alex;


--
-- Name: exch_prefered_faculty; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_prefered_faculty FROM PUBLIC;
REVOKE ALL ON TABLE exch_prefered_faculty FROM exchange;
GRANT ALL ON TABLE exch_prefered_faculty TO exchange;
GRANT ALL ON TABLE exch_prefered_faculty TO alex;


--
-- Name: exch_prefered_month; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_prefered_month FROM PUBLIC;
REVOKE ALL ON TABLE exch_prefered_month FROM exchange;
GRANT ALL ON TABLE exch_prefered_month TO exchange;
GRANT ALL ON TABLE exch_prefered_month TO alex;


--
-- Name: exch_public_text; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_public_text FROM PUBLIC;
REVOKE ALL ON TABLE exch_public_text FROM exchange;
GRANT ALL ON TABLE exch_public_text TO exchange;
GRANT ALL ON TABLE exch_public_text TO alex;


--
-- Name: exch_residency_length; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_residency_length FROM PUBLIC;
REVOKE ALL ON TABLE exch_residency_length FROM exchange;
GRANT ALL ON TABLE exch_residency_length TO exchange;
GRANT ALL ON TABLE exch_residency_length TO alex;


--
-- Name: exch_sex; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_sex FROM PUBLIC;
REVOKE ALL ON TABLE exch_sex FROM exchange;
GRANT ALL ON TABLE exch_sex TO exchange;
GRANT ALL ON TABLE exch_sex TO alex;


--
-- Name: exch_student; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_student FROM PUBLIC;
REVOKE ALL ON TABLE exch_student FROM exchange;
GRANT ALL ON TABLE exch_student TO exchange;
GRANT ALL ON TABLE exch_student TO alex;


--
-- Name: exch_study_level; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_study_level FROM PUBLIC;
REVOKE ALL ON TABLE exch_study_level FROM exchange;
GRANT ALL ON TABLE exch_study_level TO exchange;
GRANT ALL ON TABLE exch_study_level TO alex;


--
-- Name: exch_study_program; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_study_program FROM PUBLIC;
REVOKE ALL ON TABLE exch_study_program FROM exchange;
GRANT ALL ON TABLE exch_study_program TO exchange;
GRANT ALL ON TABLE exch_study_program TO alex;


--
-- Name: exch_title; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_title FROM PUBLIC;
REVOKE ALL ON TABLE exch_title FROM exchange;
GRANT ALL ON TABLE exch_title TO exchange;
GRANT ALL ON TABLE exch_title TO alex;


--
-- Name: exch_training; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_training FROM PUBLIC;
REVOKE ALL ON TABLE exch_training FROM exchange;
GRANT ALL ON TABLE exch_training TO exchange;
GRANT ALL ON TABLE exch_training TO alex;


--
-- Name: exch_training_state; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_training_state FROM PUBLIC;
REVOKE ALL ON TABLE exch_training_state FROM exchange;
GRANT ALL ON TABLE exch_training_state TO exchange;
GRANT ALL ON TABLE exch_training_state TO alex;


--
-- Name: exch_training_state_cancel; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_training_state_cancel FROM PUBLIC;
REVOKE ALL ON TABLE exch_training_state_cancel FROM exchange;
GRANT ALL ON TABLE exch_training_state_cancel TO exchange;
GRANT ALL ON TABLE exch_training_state_cancel TO alex;


--
-- Name: exch_translate; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_translate FROM PUBLIC;
REVOKE ALL ON TABLE exch_translate FROM exchange;
GRANT ALL ON TABLE exch_translate TO exchange;
GRANT ALL ON TABLE exch_translate TO alex;


--
-- Name: exch_translate_tables; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_translate_tables FROM PUBLIC;
REVOKE ALL ON TABLE exch_translate_tables FROM exchange;
GRANT ALL ON TABLE exch_translate_tables TO exchange;
GRANT ALL ON TABLE exch_translate_tables TO alex;


--
-- Name: exch_work_category; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_work_category FROM PUBLIC;
REVOKE ALL ON TABLE exch_work_category FROM exchange;
GRANT ALL ON TABLE exch_work_category TO exchange;
GRANT ALL ON TABLE exch_work_category TO alex;


--
-- Name: exch_year_class; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_year_class FROM PUBLIC;
REVOKE ALL ON TABLE exch_year_class FROM exchange;
GRANT ALL ON TABLE exch_year_class TO exchange;
GRANT ALL ON TABLE exch_year_class TO alex;


--
-- Name: exch_yes_no; Type: ACL; Schema: exch; Owner: exchange
--

REVOKE ALL ON TABLE exch_yes_no FROM PUBLIC;
REVOKE ALL ON TABLE exch_yes_no FROM exchange;
GRANT ALL ON TABLE exch_yes_no TO exchange;
GRANT ALL ON TABLE exch_yes_no TO alex;


SET search_path = public, pg_catalog;

--
-- Name: lc; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE lc FROM PUBLIC;
REVOKE ALL ON TABLE lc FROM intranet;
GRANT ALL ON TABLE lc TO intranet;
GRANT SELECT ON TABLE lc TO exchange;
GRANT SELECT,REFERENCES ON TABLE lc TO udbmaster;
GRANT SELECT ON TABLE lc TO alex;


--
-- Name: users; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM intranet;
GRANT ALL ON TABLE users TO intranet;
GRANT SELECT ON TABLE users TO exchange;
GRANT INSERT,SELECT,UPDATE,REFERENCES,TRIGGER ON TABLE users TO udbmaster;
GRANT ALL ON TABLE users TO alex;


--
-- Name: _groups; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE _groups FROM PUBLIC;
REVOKE ALL ON TABLE _groups FROM intranet;
GRANT ALL ON TABLE _groups TO intranet;
GRANT SELECT ON TABLE _groups TO udbmaster;
GRANT ALL ON TABLE _groups TO alex;


--
-- Name: alumni_users; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE alumni_users FROM PUBLIC;
REVOKE ALL ON TABLE alumni_users FROM intranet;
GRANT ALL ON TABLE alumni_users TO intranet;
GRANT ALL ON TABLE alumni_users TO alex;


--
-- Name: badgets; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE badgets FROM PUBLIC;
REVOKE ALL ON TABLE badgets FROM intranet;
GRANT ALL ON TABLE badgets TO intranet;
GRANT ALL ON TABLE badgets TO alex;


--
-- Name: board; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE board FROM PUBLIC;
REVOKE ALL ON TABLE board FROM intranet;
GRANT ALL ON TABLE board TO intranet;
GRANT ALL ON TABLE board TO alex;


--
-- Name: board_name; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE board_name FROM PUBLIC;
REVOKE ALL ON TABLE board_name FROM intranet;
GRANT ALL ON TABLE board_name TO intranet;
GRANT ALL ON TABLE board_name TO alex;


--
-- Name: categories; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE categories FROM PUBLIC;
REVOKE ALL ON TABLE categories FROM intranet;
GRANT ALL ON TABLE categories TO intranet;
GRANT ALL ON TABLE categories TO alex;


--
-- Name: ci_cvut_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_cvut_tmp FROM PUBLIC;
REVOKE ALL ON TABLE ci_cvut_tmp FROM intranet;
GRANT ALL ON TABLE ci_cvut_tmp TO intranet;
GRANT ALL ON TABLE ci_cvut_tmp TO alex;


--
-- Name: ci_faktura; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura FROM intranet;
GRANT ALL ON TABLE ci_faktura TO intranet;
GRANT ALL ON TABLE ci_faktura TO alex;


--
-- Name: ci_faktura_lang; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_lang FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_lang FROM intranet;
GRANT ALL ON TABLE ci_faktura_lang TO intranet;
GRANT ALL ON TABLE ci_faktura_lang TO alex;


--
-- Name: ci_faktura_polozka; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_polozka FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_polozka FROM intranet;
GRANT ALL ON TABLE ci_faktura_polozka TO intranet;
GRANT ALL ON TABLE ci_faktura_polozka TO alex;


--
-- Name: ci_faktura_seznam; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_seznam FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_seznam FROM intranet;
GRANT ALL ON TABLE ci_faktura_seznam TO intranet;
GRANT ALL ON TABLE ci_faktura_seznam TO alex;


--
-- Name: ci_faktura_stav; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_stav FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_stav FROM intranet;
GRANT ALL ON TABLE ci_faktura_stav TO intranet;
GRANT ALL ON TABLE ci_faktura_stav TO alex;


--
-- Name: ci_faktura_text; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_text FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_text FROM intranet;
GRANT ALL ON TABLE ci_faktura_text TO intranet;
GRANT ALL ON TABLE ci_faktura_text TO alex;


--
-- Name: ci_faktura_typ; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_typ FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_typ FROM intranet;
GRANT ALL ON TABLE ci_faktura_typ TO intranet;
GRANT ALL ON TABLE ci_faktura_typ TO alex;


--
-- Name: ci_faktura_typ_text; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_typ_text FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_typ_text FROM intranet;
GRANT ALL ON TABLE ci_faktura_typ_text TO intranet;
GRANT ALL ON TABLE ci_faktura_typ_text TO alex;


--
-- Name: ci_faktura_ucet; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_faktura_ucet FROM PUBLIC;
REVOKE ALL ON TABLE ci_faktura_ucet FROM intranet;
GRANT ALL ON TABLE ci_faktura_ucet TO intranet;
GRANT ALL ON TABLE ci_faktura_ucet TO alex;


--
-- Name: ci_firma_dic_bak; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_firma_dic_bak FROM PUBLIC;
REVOKE ALL ON TABLE ci_firma_dic_bak FROM intranet;
GRANT ALL ON TABLE ci_firma_dic_bak TO intranet;
GRANT ALL ON TABLE ci_firma_dic_bak TO alex;


--
-- Name: ci_firma_historie; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_firma_historie FROM PUBLIC;
REVOKE ALL ON TABLE ci_firma_historie FROM intranet;
GRANT ALL ON TABLE ci_firma_historie TO intranet;
GRANT ALL ON TABLE ci_firma_historie TO alex;


--
-- Name: ci_firma_odkaz; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_firma_odkaz FROM PUBLIC;
REVOKE ALL ON TABLE ci_firma_odkaz FROM intranet;
GRANT ALL ON TABLE ci_firma_odkaz TO intranet;
GRANT ALL ON TABLE ci_firma_odkaz TO alex;


--
-- Name: ci_firma_odkaz_typ; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_firma_odkaz_typ FROM PUBLIC;
REVOKE ALL ON TABLE ci_firma_odkaz_typ FROM intranet;
GRANT ALL ON TABLE ci_firma_odkaz_typ TO intranet;
GRANT ALL ON TABLE ci_firma_odkaz_typ TO alex;


--
-- Name: ci_hledani; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_hledani FROM PUBLIC;
REVOKE ALL ON TABLE ci_hledani FROM intranet;
GRANT ALL ON TABLE ci_hledani TO intranet;
GRANT ALL ON TABLE ci_hledani TO alex;


--
-- Name: ci_hledani_bak; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_hledani_bak FROM PUBLIC;
REVOKE ALL ON TABLE ci_hledani_bak FROM intranet;
GRANT ALL ON TABLE ci_hledani_bak TO intranet;
GRANT ALL ON TABLE ci_hledani_bak TO alex;


--
-- Name: ci_hledani_bak2; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_hledani_bak2 FROM PUBLIC;
REVOKE ALL ON TABLE ci_hledani_bak2 FROM intranet;
GRANT ALL ON TABLE ci_hledani_bak2 TO intranet;
GRANT ALL ON TABLE ci_hledani_bak2 TO alex;


--
-- Name: ci_hledani_typ; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_hledani_typ FROM PUBLIC;
REVOKE ALL ON TABLE ci_hledani_typ FROM intranet;
GRANT ALL ON TABLE ci_hledani_typ TO intranet;
GRANT ALL ON TABLE ci_hledani_typ TO alex;


--
-- Name: ci_kontakt_jmeno_prijmeni; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_kontakt_jmeno_prijmeni FROM PUBLIC;
REVOKE ALL ON TABLE ci_kontakt_jmeno_prijmeni FROM intranet;
GRANT ALL ON TABLE ci_kontakt_jmeno_prijmeni TO intranet;
GRANT ALL ON TABLE ci_kontakt_jmeno_prijmeni TO alex;


--
-- Name: ci_kontakt_oldid; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_kontakt_oldid FROM PUBLIC;
REVOKE ALL ON TABLE ci_kontakt_oldid FROM intranet;
GRANT ALL ON TABLE ci_kontakt_oldid TO intranet;
GRANT ALL ON TABLE ci_kontakt_oldid TO alex;


--
-- Name: ci_kontakt_typ; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_kontakt_typ FROM PUBLIC;
REVOKE ALL ON TABLE ci_kontakt_typ FROM intranet;
GRANT ALL ON TABLE ci_kontakt_typ TO intranet;
GRANT ALL ON TABLE ci_kontakt_typ TO alex;


--
-- Name: ci_projekt; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_projekt FROM PUBLIC;
REVOKE ALL ON TABLE ci_projekt FROM intranet;
GRANT ALL ON TABLE ci_projekt TO intranet;
GRANT ALL ON TABLE ci_projekt TO alex;


--
-- Name: ci_projekt_tym; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_projekt_tym FROM PUBLIC;
REVOKE ALL ON TABLE ci_projekt_tym FROM intranet;
GRANT ALL ON TABLE ci_projekt_tym TO intranet;
GRANT ALL ON TABLE ci_projekt_tym TO alex;


--
-- Name: ci_projekt_typ; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_projekt_typ FROM PUBLIC;
REVOKE ALL ON TABLE ci_projekt_typ FROM intranet;
GRANT ALL ON TABLE ci_projekt_typ TO intranet;
GRANT ALL ON TABLE ci_projekt_typ TO alex;


--
-- Name: ci_projekt_ucast; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_projekt_ucast FROM PUBLIC;
REVOKE ALL ON TABLE ci_projekt_ucast FROM intranet;
GRANT ALL ON TABLE ci_projekt_ucast TO intranet;
GRANT ALL ON TABLE ci_projekt_ucast TO alex;


--
-- Name: ci_schuzka_backup; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_schuzka_backup FROM PUBLIC;
REVOKE ALL ON TABLE ci_schuzka_backup FROM intranet;
GRANT ALL ON TABLE ci_schuzka_backup TO intranet;
GRANT ALL ON TABLE ci_schuzka_backup TO alex;


--
-- Name: ci_spoluprace; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_spoluprace FROM PUBLIC;
REVOKE ALL ON TABLE ci_spoluprace FROM intranet;
GRANT ALL ON TABLE ci_spoluprace TO intranet;
GRANT ALL ON TABLE ci_spoluprace TO alex;


--
-- Name: ci_spoluprace_detail; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_spoluprace_detail FROM PUBLIC;
REVOKE ALL ON TABLE ci_spoluprace_detail FROM intranet;
GRANT ALL ON TABLE ci_spoluprace_detail TO intranet;
GRANT ALL ON TABLE ci_spoluprace_detail TO alex;


--
-- Name: ci_typ_spoluprace; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_typ_spoluprace FROM PUBLIC;
REVOKE ALL ON TABLE ci_typ_spoluprace FROM intranet;
GRANT ALL ON TABLE ci_typ_spoluprace TO intranet;
GRANT ALL ON TABLE ci_typ_spoluprace TO alex;


--
-- Name: ci_ukol; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE ci_ukol FROM PUBLIC;
REVOKE ALL ON TABLE ci_ukol FROM intranet;
GRANT ALL ON TABLE ci_ukol TO intranet;
GRANT ALL ON TABLE ci_ukol TO alex;


--
-- Name: colors; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE colors FROM PUBLIC;
REVOKE ALL ON TABLE colors FROM intranet;
GRANT ALL ON TABLE colors TO intranet;
GRANT ALL ON TABLE colors TO alex;


--
-- Name: company_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE company_tmp FROM PUBLIC;
REVOKE ALL ON TABLE company_tmp FROM intranet;
GRANT ALL ON TABLE company_tmp TO intranet;
GRANT ALL ON TABLE company_tmp TO alex;


--
-- Name: countries_old; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE countries_old FROM PUBLIC;
REVOKE ALL ON TABLE countries_old FROM intranet;
GRANT ALL ON TABLE countries_old TO intranet;
GRANT ALL ON TABLE countries_old TO alex;


--
-- Name: cvut_faktury; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_faktury FROM PUBLIC;
REVOKE ALL ON TABLE cvut_faktury FROM intranet;
GRANT ALL ON TABLE cvut_faktury TO intranet;
GRANT ALL ON TABLE cvut_faktury TO alex;


--
-- Name: cvut_faktury_items; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_faktury_items FROM PUBLIC;
REVOKE ALL ON TABLE cvut_faktury_items FROM intranet;
GRANT ALL ON TABLE cvut_faktury_items TO intranet;
GRANT ALL ON TABLE cvut_faktury_items TO alex;


--
-- Name: cvut_firma_prj; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_firma_prj FROM PUBLIC;
REVOKE ALL ON TABLE cvut_firma_prj FROM intranet;
GRANT ALL ON TABLE cvut_firma_prj TO intranet;
GRANT ALL ON TABLE cvut_firma_prj TO alex;


--
-- Name: cvut_firmy; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_firmy FROM PUBLIC;
REVOKE ALL ON TABLE cvut_firmy FROM intranet;
GRANT ALL ON TABLE cvut_firmy TO intranet;
GRANT ALL ON TABLE cvut_firmy TO alex;


--
-- Name: cvut_historie; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_historie FROM PUBLIC;
REVOKE ALL ON TABLE cvut_historie FROM intranet;
GRANT ALL ON TABLE cvut_historie TO intranet;
GRANT ALL ON TABLE cvut_historie TO alex;


--
-- Name: cvut_projekty; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_projekty FROM PUBLIC;
REVOKE ALL ON TABLE cvut_projekty FROM intranet;
GRANT ALL ON TABLE cvut_projekty TO intranet;
GRANT ALL ON TABLE cvut_projekty TO alex;


--
-- Name: cvut_users; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE cvut_users FROM PUBLIC;
REVOKE ALL ON TABLE cvut_users FROM intranet;
GRANT ALL ON TABLE cvut_users TO intranet;
GRANT ALL ON TABLE cvut_users TO alex;


--
-- Name: download; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE download FROM PUBLIC;
REVOKE ALL ON TABLE download FROM intranet;
GRANT ALL ON TABLE download TO intranet;
GRANT ALL ON TABLE download TO alex;


--
-- Name: download_group; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE download_group FROM PUBLIC;
REVOKE ALL ON TABLE download_group FROM intranet;
GRANT ALL ON TABLE download_group TO intranet;
GRANT ALL ON TABLE download_group TO alex;


--
-- Name: emptytable; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE emptytable FROM PUBLIC;
REVOKE ALL ON TABLE emptytable FROM intranet;
GRANT ALL ON TABLE emptytable TO intranet;
GRANT ALL ON TABLE emptytable TO alex;


--
-- Name: exch_ci_trainees; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_ci_trainees FROM PUBLIC;
REVOKE ALL ON TABLE exch_ci_trainees FROM intranet;
GRANT ALL ON TABLE exch_ci_trainees TO intranet;
GRANT SELECT ON TABLE exch_ci_trainees TO exchange;
GRANT ALL ON TABLE exch_ci_trainees TO alex;


--
-- Name: exch_company_in; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_company_in FROM PUBLIC;
REVOKE ALL ON TABLE exch_company_in FROM intranet;
GRANT ALL ON TABLE exch_company_in TO intranet;
GRANT ALL ON TABLE exch_company_in TO alex;


--
-- Name: exch_country; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_country FROM intranet;
GRANT ALL ON TABLE exch_country TO intranet;
GRANT ALL ON TABLE exch_country TO alex;


--
-- Name: exch_exam; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_exam FROM PUBLIC;
REVOKE ALL ON TABLE exch_exam FROM intranet;
GRANT ALL ON TABLE exch_exam TO intranet;
GRANT ALL ON TABLE exch_exam TO alex;


--
-- Name: exch_konkurz_email; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_konkurz_email FROM PUBLIC;
REVOKE ALL ON TABLE exch_konkurz_email FROM intranet;
GRANT ALL ON TABLE exch_konkurz_email TO intranet;
GRANT ALL ON TABLE exch_konkurz_email TO alex;


--
-- Name: exch_konkurz_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_konkurz_tmp FROM PUBLIC;
REVOKE ALL ON TABLE exch_konkurz_tmp FROM intranet;
GRANT ALL ON TABLE exch_konkurz_tmp TO intranet;
GRANT ALL ON TABLE exch_konkurz_tmp TO alex;


--
-- Name: exch_lang; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_lang FROM PUBLIC;
REVOKE ALL ON TABLE exch_lang FROM intranet;
GRANT ALL ON TABLE exch_lang TO intranet;
GRANT ALL ON TABLE exch_lang TO alex;


--
-- Name: exch_out_preffered_training; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_out_preffered_training FROM PUBLIC;
REVOKE ALL ON TABLE exch_out_preffered_training FROM intranet;
GRANT ALL ON TABLE exch_out_preffered_training TO intranet;
GRANT ALL ON TABLE exch_out_preffered_training TO alex;


--
-- Name: exch_out_preffered_training_pri; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_out_preffered_training_pri FROM PUBLIC;
REVOKE ALL ON TABLE exch_out_preffered_training_pri FROM intranet;
GRANT ALL ON TABLE exch_out_preffered_training_pri TO intranet;
GRANT ALL ON TABLE exch_out_preffered_training_pri TO alex;


--
-- Name: exch_participant; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_participant FROM PUBLIC;
REVOKE ALL ON TABLE exch_participant FROM intranet;
GRANT ALL ON TABLE exch_participant TO intranet;
GRANT ALL ON TABLE exch_participant TO alex;


--
-- Name: exch_prefered_country; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_prefered_country FROM PUBLIC;
REVOKE ALL ON TABLE exch_prefered_country FROM intranet;
GRANT ALL ON TABLE exch_prefered_country TO intranet;
GRANT ALL ON TABLE exch_prefered_country TO alex;


--
-- Name: exch_report_cz; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_report_cz FROM PUBLIC;
REVOKE ALL ON TABLE exch_report_cz FROM intranet;
GRANT ALL ON TABLE exch_report_cz TO intranet;
GRANT ALL ON TABLE exch_report_cz TO alex;


--
-- Name: exch_specialization; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_specialization FROM PUBLIC;
REVOKE ALL ON TABLE exch_specialization FROM intranet;
GRANT ALL ON TABLE exch_specialization TO intranet;
GRANT ALL ON TABLE exch_specialization TO alex;


--
-- Name: exch_student_in; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_student_in FROM PUBLIC;
REVOKE ALL ON TABLE exch_student_in FROM intranet;
GRANT ALL ON TABLE exch_student_in TO intranet;
GRANT ALL ON TABLE exch_student_in TO alex;


--
-- Name: exch_student_n5b; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_student_n5b FROM PUBLIC;
REVOKE ALL ON TABLE exch_student_n5b FROM intranet;
GRANT ALL ON TABLE exch_student_n5b TO intranet;
GRANT ALL ON TABLE exch_student_n5b TO alex;


--
-- Name: exch_tmp_company_in; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_tmp_company_in FROM PUBLIC;
REVOKE ALL ON TABLE exch_tmp_company_in FROM intranet;
GRANT ALL ON TABLE exch_tmp_company_in TO intranet;
GRANT ALL ON TABLE exch_tmp_company_in TO alex;


--
-- Name: exch_trainees_all; Type: ACL; Schema: public; Owner: alfi
--

REVOKE ALL ON TABLE exch_trainees_all FROM PUBLIC;
REVOKE ALL ON TABLE exch_trainees_all FROM alfi;
GRANT ALL ON TABLE exch_trainees_all TO alfi;
GRANT SELECT ON TABLE exch_trainees_all TO intranet;


--
-- Name: exch_training_info; Type: ACL; Schema: public; Owner: alfi
--

REVOKE ALL ON TABLE exch_training_info FROM PUBLIC;
REVOKE ALL ON TABLE exch_training_info FROM alfi;
GRANT ALL ON TABLE exch_training_info TO alfi;
GRANT SELECT ON TABLE exch_training_info TO intranet;


--
-- Name: exch_university_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE exch_university_tmp FROM PUBLIC;
REVOKE ALL ON TABLE exch_university_tmp FROM intranet;
GRANT ALL ON TABLE exch_university_tmp TO intranet;
GRANT ALL ON TABLE exch_university_tmp TO alex;


--
-- Name: firmy_katalog; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE firmy_katalog FROM PUBLIC;
REVOKE ALL ON TABLE firmy_katalog FROM intranet;
GRANT ALL ON TABLE firmy_katalog TO intranet;
GRANT ALL ON TABLE firmy_katalog TO alex;


--
-- Name: sendmail_list; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE sendmail_list FROM PUBLIC;
REVOKE ALL ON TABLE sendmail_list FROM intranet;
GRANT ALL ON TABLE sendmail_list TO intranet;
GRANT ALL ON TABLE sendmail_list TO alex;


--
-- Name: global_abook; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE global_abook FROM PUBLIC;
REVOKE ALL ON TABLE global_abook FROM intranet;
GRANT ALL ON TABLE global_abook TO intranet;


--
-- Name: kpp2002; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp2002 FROM PUBLIC;
REVOKE ALL ON TABLE kpp2002 FROM intranet;
GRANT ALL ON TABLE kpp2002 TO intranet;
GRANT ALL ON TABLE kpp2002 TO alex;


--
-- Name: kpp2003_anketa; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp2003_anketa FROM PUBLIC;
REVOKE ALL ON TABLE kpp2003_anketa FROM intranet;
GRANT ALL ON TABLE kpp2003_anketa TO intranet;
GRANT ALL ON TABLE kpp2003_anketa TO alex;


--
-- Name: kpp2003_anketa_firma; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp2003_anketa_firma FROM PUBLIC;
REVOKE ALL ON TABLE kpp2003_anketa_firma FROM intranet;
GRANT ALL ON TABLE kpp2003_anketa_firma TO intranet;
GRANT ALL ON TABLE kpp2003_anketa_firma TO alex;


--
-- Name: kpp_obory; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp_obory FROM PUBLIC;
REVOKE ALL ON TABLE kpp_obory FROM intranet;
GRANT ALL ON TABLE kpp_obory TO intranet;
GRANT ALL ON TABLE kpp_obory TO alex;


--
-- Name: kpp_rocnik; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp_rocnik FROM PUBLIC;
REVOKE ALL ON TABLE kpp_rocnik FROM intranet;
GRANT ALL ON TABLE kpp_rocnik TO intranet;
GRANT ALL ON TABLE kpp_rocnik TO alex;


--
-- Name: kpp_studentum_nabizi; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp_studentum_nabizi FROM PUBLIC;
REVOKE ALL ON TABLE kpp_studentum_nabizi FROM intranet;
GRANT ALL ON TABLE kpp_studentum_nabizi TO intranet;
GRANT ALL ON TABLE kpp_studentum_nabizi TO alex;


--
-- Name: kpp_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp_tmp FROM PUBLIC;
REVOKE ALL ON TABLE kpp_tmp FROM intranet;
GRANT ALL ON TABLE kpp_tmp TO intranet;
GRANT ALL ON TABLE kpp_tmp TO alex;


--
-- Name: kpp_varianty; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE kpp_varianty FROM PUBLIC;
REVOKE ALL ON TABLE kpp_varianty FROM intranet;
GRANT ALL ON TABLE kpp_varianty TO intranet;
GRANT ALL ON TABLE kpp_varianty TO alex;


--
-- Name: lang_levels; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE lang_levels FROM PUBLIC;
REVOKE ALL ON TABLE lang_levels FROM intranet;
GRANT ALL ON TABLE lang_levels TO intranet;
GRANT ALL ON TABLE lang_levels TO alex;


--
-- Name: lc_summer_exchange; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE lc_summer_exchange FROM PUBLIC;
REVOKE ALL ON TABLE lc_summer_exchange FROM intranet;
GRANT ALL ON TABLE lc_summer_exchange TO intranet;
GRANT ALL ON TABLE lc_summer_exchange TO alex;


--
-- Name: lidi_lc; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE lidi_lc FROM PUBLIC;
REVOKE ALL ON TABLE lidi_lc FROM intranet;
GRANT ALL ON TABLE lidi_lc TO intranet;
GRANT ALL ON TABLE lidi_lc TO alex;


--
-- Name: mailman_content; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE mailman_content FROM PUBLIC;
REVOKE ALL ON TABLE mailman_content FROM intranet;
GRANT ALL ON TABLE mailman_content TO intranet;
GRANT INSERT,SELECT,DELETE ON TABLE mailman_content TO mailman;
GRANT ALL ON TABLE mailman_content TO alex;


--
-- Name: mailman_content_id_seq; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE mailman_content_id_seq FROM PUBLIC;
REVOKE ALL ON TABLE mailman_content_id_seq FROM intranet;
GRANT ALL ON TABLE mailman_content_id_seq TO intranet;
GRANT SELECT,UPDATE ON TABLE mailman_content_id_seq TO mailman;


--
-- Name: offers; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE offers FROM PUBLIC;
REVOKE ALL ON TABLE offers FROM intranet;
GRANT ALL ON TABLE offers TO intranet;
GRANT ALL ON TABLE offers TO alex;


--
-- Name: oforms; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms FROM PUBLIC;
REVOKE ALL ON TABLE oforms FROM intranet;
GRANT ALL ON TABLE oforms TO intranet;
GRANT ALL ON TABLE oforms TO alex;


--
-- Name: oforms2002; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms2002 FROM PUBLIC;
REVOKE ALL ON TABLE oforms2002 FROM intranet;
GRANT ALL ON TABLE oforms2002 TO intranet;
GRANT ALL ON TABLE oforms2002 TO alex;


--
-- Name: oforms2003; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms2003 FROM PUBLIC;
REVOKE ALL ON TABLE oforms2003 FROM intranet;
GRANT ALL ON TABLE oforms2003 TO intranet;
GRANT ALL ON TABLE oforms2003 TO alex;


--
-- Name: oforms_2004; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms_2004 FROM PUBLIC;
REVOKE ALL ON TABLE oforms_2004 FROM intranet;
GRANT ALL ON TABLE oforms_2004 TO intranet;
GRANT ALL ON TABLE oforms_2004 TO alex;


--
-- Name: oforms_2005_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms_2005_tmp FROM PUBLIC;
REVOKE ALL ON TABLE oforms_2005_tmp FROM intranet;
GRANT ALL ON TABLE oforms_2005_tmp TO intranet;
GRANT ALL ON TABLE oforms_2005_tmp TO alex;


--
-- Name: oforms_2006; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms_2006 FROM PUBLIC;
REVOKE ALL ON TABLE oforms_2006 FROM intranet;
GRANT ALL ON TABLE oforms_2006 TO intranet;
GRANT ALL ON TABLE oforms_2006 TO alex;


--
-- Name: oforms_access; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE oforms_access FROM PUBLIC;
REVOKE ALL ON TABLE oforms_access FROM intranet;
GRANT ALL ON TABLE oforms_access TO intranet;
GRANT ALL ON TABLE oforms_access TO alex;


--
-- Name: postpraxe; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE postpraxe FROM PUBLIC;
REVOKE ALL ON TABLE postpraxe FROM intranet;
GRANT ALL ON TABLE postpraxe TO intranet;
GRANT ALL ON TABLE postpraxe TO alex;


--
-- Name: praxe_konkurs; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE praxe_konkurs FROM PUBLIC;
REVOKE ALL ON TABLE praxe_konkurs FROM intranet;
GRANT ALL ON TABLE praxe_konkurs TO intranet;
GRANT ALL ON TABLE praxe_konkurs TO alex;


--
-- Name: rok; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE rok FROM PUBLIC;
REVOKE ALL ON TABLE rok FROM intranet;
GRANT ALL ON TABLE rok TO intranet;
GRANT ALL ON TABLE rok TO alex;


--
-- Name: s_ci_kontakt; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE s_ci_kontakt FROM PUBLIC;
REVOKE ALL ON TABLE s_ci_kontakt FROM intranet;
GRANT ALL ON TABLE s_ci_kontakt TO intranet;
GRANT SELECT,UPDATE ON TABLE s_ci_kontakt TO exchange;


--
-- Name: s_users; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE s_users FROM PUBLIC;
REVOKE ALL ON TABLE s_users FROM intranet;
GRANT ALL ON TABLE s_users TO intranet;
GRANT SELECT,UPDATE ON TABLE s_users TO udbmaster;


--
-- Name: sendmail_list_subscriber; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE sendmail_list_subscriber FROM PUBLIC;
REVOKE ALL ON TABLE sendmail_list_subscriber FROM intranet;
GRANT ALL ON TABLE sendmail_list_subscriber TO intranet;
GRANT ALL ON TABLE sendmail_list_subscriber TO alex;


--
-- Name: sendmail_log_content; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE sendmail_log_content FROM PUBLIC;
REVOKE ALL ON TABLE sendmail_log_content FROM intranet;
GRANT ALL ON TABLE sendmail_log_content TO intranet;
GRANT ALL ON TABLE sendmail_log_content TO alex;


--
-- Name: sendmail_log_rcpt; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE sendmail_log_rcpt FROM PUBLIC;
REVOKE ALL ON TABLE sendmail_log_rcpt FROM intranet;
GRANT ALL ON TABLE sendmail_log_rcpt TO intranet;
GRANT ALL ON TABLE sendmail_log_rcpt TO alex;


--
-- Name: sendmail_test_list_subscriber; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE sendmail_test_list_subscriber FROM PUBLIC;
REVOKE ALL ON TABLE sendmail_test_list_subscriber FROM intranet;
GRANT ALL ON TABLE sendmail_test_list_subscriber TO intranet;
GRANT ALL ON TABLE sendmail_test_list_subscriber TO alex;


--
-- Name: share; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE "share" FROM PUBLIC;
REVOKE ALL ON TABLE "share" FROM intranet;
GRANT ALL ON TABLE "share" TO intranet;
GRANT ALL ON TABLE "share" TO alex;


--
-- Name: share_access; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE share_access FROM PUBLIC;
REVOKE ALL ON TABLE share_access FROM intranet;
GRANT ALL ON TABLE share_access TO intranet;
GRANT ALL ON TABLE share_access TO alex;


--
-- Name: share_rights; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE share_rights FROM PUBLIC;
REVOKE ALL ON TABLE share_rights FROM intranet;
GRANT ALL ON TABLE share_rights TO intranet;
GRANT ALL ON TABLE share_rights TO alex;


--
-- Name: share_rights_users2ldap; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE share_rights_users2ldap FROM PUBLIC;
REVOKE ALL ON TABLE share_rights_users2ldap FROM intranet;
GRANT ALL ON TABLE share_rights_users2ldap TO intranet;
GRANT SELECT,REFERENCES ON TABLE share_rights_users2ldap TO udbmaster;


--
-- Name: st_lang_levels; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE st_lang_levels FROM PUBLIC;
REVOKE ALL ON TABLE st_lang_levels FROM intranet;
GRANT ALL ON TABLE st_lang_levels TO intranet;
GRANT ALL ON TABLE st_lang_levels TO alex;


--
-- Name: st_study_types; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE st_study_types FROM PUBLIC;
REVOKE ALL ON TABLE st_study_types FROM intranet;
GRANT ALL ON TABLE st_study_types TO intranet;
GRANT ALL ON TABLE st_study_types TO alex;


--
-- Name: states; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE states FROM PUBLIC;
REVOKE ALL ON TABLE states FROM intranet;
GRANT ALL ON TABLE states TO intranet;
GRANT ALL ON TABLE states TO alex;


--
-- Name: states_backup; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE states_backup FROM PUBLIC;
REVOKE ALL ON TABLE states_backup FROM intranet;
GRANT ALL ON TABLE states_backup TO intranet;
GRANT ALL ON TABLE states_backup TO alex;


--
-- Name: states_lang; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE states_lang FROM PUBLIC;
REVOKE ALL ON TABLE states_lang FROM intranet;
GRANT ALL ON TABLE states_lang TO intranet;
GRANT ALL ON TABLE states_lang TO alex;


--
-- Name: states_lang_old; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE states_lang_old FROM PUBLIC;
REVOKE ALL ON TABLE states_lang_old FROM intranet;
GRANT ALL ON TABLE states_lang_old TO intranet;
GRANT ALL ON TABLE states_lang_old TO alex;


--
-- Name: states_old; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE states_old FROM PUBLIC;
REVOKE ALL ON TABLE states_old FROM intranet;
GRANT ALL ON TABLE states_old TO intranet;
GRANT ALL ON TABLE states_old TO alex;


--
-- Name: student; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE student FROM PUBLIC;
REVOKE ALL ON TABLE student FROM intranet;
GRANT ALL ON TABLE student TO intranet;
GRANT ALL ON TABLE student TO alex;


--
-- Name: test; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE test FROM PUBLIC;
REVOKE ALL ON TABLE test FROM intranet;
GRANT ALL ON TABLE test TO intranet;
GRANT ALL ON TABLE test TO alex;


--
-- Name: test_calendar; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE test_calendar FROM PUBLIC;
REVOKE ALL ON TABLE test_calendar FROM intranet;
GRANT ALL ON TABLE test_calendar TO intranet;
GRANT ALL ON TABLE test_calendar TO alex;


--
-- Name: tmp_firm; Type: ACL; Schema: public; Owner: alfi
--

REVOKE ALL ON TABLE tmp_firm FROM PUBLIC;
REVOKE ALL ON TABLE tmp_firm FROM alfi;
GRANT ALL ON TABLE tmp_firm TO alfi;
GRANT ALL ON TABLE tmp_firm TO alex;


--
-- Name: trainees; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE trainees FROM PUBLIC;
REVOKE ALL ON TABLE trainees FROM intranet;
GRANT ALL ON TABLE trainees TO intranet;
GRANT ALL ON TABLE trainees TO alex;


--
-- Name: trainees_events; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE trainees_events FROM PUBLIC;
REVOKE ALL ON TABLE trainees_events FROM intranet;
GRANT ALL ON TABLE trainees_events TO intranet;
GRANT ALL ON TABLE trainees_events TO alex;


--
-- Name: trainees_tmp; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE trainees_tmp FROM PUBLIC;
REVOKE ALL ON TABLE trainees_tmp FROM intranet;
GRANT ALL ON TABLE trainees_tmp TO intranet;
GRANT ALL ON TABLE trainees_tmp TO alex;


SET search_path = udb, pg_catalog;

--
-- Name: conferencies; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE conferencies FROM PUBLIC;
REVOKE ALL ON TABLE conferencies FROM udbmaster;
GRANT ALL ON TABLE conferencies TO udbmaster;
GRANT ALL ON TABLE conferencies TO alex;


--
-- Name: users; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM udbmaster;
GRANT ALL ON TABLE users TO udbmaster;
GRANT ALL ON TABLE users TO alex;


SET search_path = public, pg_catalog;

--
-- Name: university; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE university FROM PUBLIC;
REVOKE ALL ON TABLE university FROM intranet;
GRANT ALL ON TABLE university TO intranet;
GRANT ALL ON TABLE university TO alex;


--
-- Name: user_preferences; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE user_preferences FROM PUBLIC;
REVOKE ALL ON TABLE user_preferences FROM intranet;
GRANT ALL ON TABLE user_preferences TO intranet;
GRANT ALL ON TABLE user_preferences TO alex;


--
-- Name: users_bak; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE users_bak FROM PUBLIC;
REVOKE ALL ON TABLE users_bak FROM intranet;
GRANT ALL ON TABLE users_bak TO intranet;
GRANT ALL ON TABLE users_bak TO alex;


--
-- Name: vpp2002; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE vpp2002 FROM PUBLIC;
REVOKE ALL ON TABLE vpp2002 FROM intranet;
GRANT ALL ON TABLE vpp2002 TO intranet;
GRANT ALL ON TABLE vpp2002 TO alex;


--
-- Name: vpp_pozice; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE vpp_pozice FROM PUBLIC;
REVOKE ALL ON TABLE vpp_pozice FROM intranet;
GRANT ALL ON TABLE vpp_pozice TO intranet;
GRANT ALL ON TABLE vpp_pozice TO alex;


--
-- Name: www; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE www FROM PUBLIC;
REVOKE ALL ON TABLE www FROM intranet;
GRANT ALL ON TABLE www TO intranet;
GRANT ALL ON TABLE www TO alex;


--
-- Name: years; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE years FROM PUBLIC;
REVOKE ALL ON TABLE years FROM intranet;
GRANT ALL ON TABLE years TO intranet;
GRANT ALL ON TABLE years TO alex;


--
-- Name: yesno; Type: ACL; Schema: public; Owner: intranet
--

REVOKE ALL ON TABLE yesno FROM PUBLIC;
REVOKE ALL ON TABLE yesno FROM intranet;
GRANT ALL ON TABLE yesno TO intranet;
GRANT ALL ON TABLE yesno TO alex;


SET search_path = regsys, pg_catalog;

--
-- Name: events; Type: ACL; Schema: regsys; Owner: rikard0
--

REVOKE ALL ON TABLE events FROM PUBLIC;
REVOKE ALL ON TABLE events FROM rikard0;
GRANT ALL ON TABLE events TO rikard0;
GRANT ALL ON TABLE events TO alex;


--
-- Name: actions; Type: ACL; Schema: regsys; Owner: rikard0
--

REVOKE ALL ON TABLE actions FROM PUBLIC;
REVOKE ALL ON TABLE actions FROM rikard0;
GRANT ALL ON TABLE actions TO rikard0;
GRANT ALL ON TABLE actions TO alex;


--
-- Name: regs_action; Type: ACL; Schema: regsys; Owner: rikard0
--

REVOKE ALL ON TABLE regs_action FROM PUBLIC;
REVOKE ALL ON TABLE regs_action FROM rikard0;
GRANT ALL ON TABLE regs_action TO rikard0;
GRANT ALL ON TABLE regs_action TO alex;


--
-- Name: regs_event; Type: ACL; Schema: regsys; Owner: rikard0
--

REVOKE ALL ON TABLE regs_event FROM PUBLIC;
REVOKE ALL ON TABLE regs_event FROM rikard0;
GRANT ALL ON TABLE regs_event TO rikard0;
GRANT ALL ON TABLE regs_event TO alex;


--
-- Name: terms; Type: ACL; Schema: regsys; Owner: rikard0
--

REVOKE ALL ON TABLE terms FROM PUBLIC;
REVOKE ALL ON TABLE terms FROM rikard0;
GRANT ALL ON TABLE terms TO rikard0;
GRANT ALL ON TABLE terms TO alex;


SET search_path = udb, pg_catalog;

--
-- Name: conference_types; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE conference_types FROM PUBLIC;
REVOKE ALL ON TABLE conference_types FROM udbmaster;
GRANT ALL ON TABLE conference_types TO udbmaster;
GRANT ALL ON TABLE conference_types TO alex;


--
-- Name: conferencies_mail; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE conferencies_mail FROM PUBLIC;
REVOKE ALL ON TABLE conferencies_mail FROM udbmaster;
GRANT ALL ON TABLE conferencies_mail TO udbmaster;
GRANT ALL ON TABLE conferencies_mail TO alex;


--
-- Name: conferencies_users; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE conferencies_users FROM PUBLIC;
REVOKE ALL ON TABLE conferencies_users FROM udbmaster;
GRANT ALL ON TABLE conferencies_users TO udbmaster;
GRANT ALL ON TABLE conferencies_users TO alex;


--
-- Name: history; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE history FROM PUBLIC;
REVOKE ALL ON TABLE history FROM udbmaster;
GRANT ALL ON TABLE history TO udbmaster;
GRANT ALL ON TABLE history TO alex;


--
-- Name: lcs; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE lcs FROM PUBLIC;
REVOKE ALL ON TABLE lcs FROM udbmaster;
GRANT ALL ON TABLE lcs TO udbmaster;
GRANT ALL ON TABLE lcs TO alex;


--
-- Name: rights; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE rights FROM PUBLIC;
REVOKE ALL ON TABLE rights FROM udbmaster;
GRANT ALL ON TABLE rights TO udbmaster;
GRANT ALL ON TABLE rights TO alex;


--
-- Name: services; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE services FROM PUBLIC;
REVOKE ALL ON TABLE services FROM udbmaster;
GRANT ALL ON TABLE services TO udbmaster;
GRANT ALL ON TABLE services TO alex;


--
-- Name: services_view; Type: ACL; Schema: udb; Owner: rikard0
--

REVOKE ALL ON TABLE services_view FROM PUBLIC;
REVOKE ALL ON TABLE services_view FROM rikard0;
GRANT ALL ON TABLE services_view TO rikard0;
GRANT SELECT ON TABLE services_view TO udbrights_r;


--
-- Name: slunicko_companies_view; Type: ACL; Schema: udb; Owner: rikard0
--

REVOKE ALL ON TABLE slunicko_companies_view FROM PUBLIC;
REVOKE ALL ON TABLE slunicko_companies_view FROM rikard0;
GRANT ALL ON TABLE slunicko_companies_view TO rikard0;
GRANT SELECT ON TABLE slunicko_companies_view TO udbrights_r;


--
-- Name: slunicko_view; Type: ACL; Schema: udb; Owner: rikard0
--

REVOKE ALL ON TABLE slunicko_view FROM PUBLIC;
REVOKE ALL ON TABLE slunicko_view FROM rikard0;
GRANT ALL ON TABLE slunicko_view TO rikard0;
GRANT SELECT ON TABLE slunicko_view TO udbrights_r;


--
-- Name: teams; Type: ACL; Schema: udb; Owner: udbmaster
--

REVOKE ALL ON TABLE teams FROM PUBLIC;
REVOKE ALL ON TABLE teams FROM udbmaster;
GRANT ALL ON TABLE teams TO udbmaster;
GRANT ALL ON TABLE teams TO alex;


--
-- PostgreSQL database dump complete
--

